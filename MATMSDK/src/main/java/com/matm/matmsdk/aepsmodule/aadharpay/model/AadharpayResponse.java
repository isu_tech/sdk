package com.matm.matmsdk.aepsmodule.aadharpay.model;
/**
 * AadharpayResponse represents a response object used in Aadharpay transactions.
 * It contains information about the transaction status, status code, transaction ID,
 * errors (if any), transaction amount, bank name, and mobile number.
 *
 * @author Smrutilipsa Patel
 */
public class AadharpayResponse {
    private String transactionStatus;
    private int status;
    private String txId;
    private String errors;
    private String transactionAmount;
    private String bankName;
    private String mobileNumber;
    /**
     * Default constructor for AadharpayResponse.
     */
    public AadharpayResponse() {
    }
    /**
     * Constructs an AadharpayResponse object with the specified parameters.
     *
     * @param transactionStatus    Status of the transaction (e.g., "Success", "Failure").
     * @param status               Status code indicating the result of the transaction.
     * @param txId                 Unique transaction ID associated with the transaction.
     * @param errors               Error message or details in case of a failed transaction.
     * @param transactionAmount   Amount involved in the transaction.
     * @param bankName             Bank name involved in the transaction.
     * @param mobileNumber         Mobile number associated with the transaction.
     */
        public AadharpayResponse(String transactionStatus, int status, String txId, String errors,String transactionAmount,
                                 String bankName,String mobileNumber) {
        this.transactionStatus = transactionStatus;
        this.status = status;
        this.txId = txId;
        this.errors = errors;
        this.transactionAmount = transactionAmount;
        this.bankName = bankName;
        this.mobileNumber = mobileNumber;
    }
    public String getTransactionStatus() {
        return transactionStatus;
    }
    public int getStatus() {
        return status;
    }
    public String getTxId() {
        return txId;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public void setTxId(String txId) {
        this.txId = txId;
    }
    public String getTransactionAmount() {
        return transactionAmount;
    }
    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }
    public String getBankName() {
        return bankName;
    }
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    public String getMobileNumber() {
        return mobileNumber;
    }
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}