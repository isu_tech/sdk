package com.matm.matmsdk.aepsmodule.unifiedaeps;
import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.Window;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.CancellationTokenSource;
import com.google.android.gms.tasks.Task;
import com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils;
import isumatm.androidsdk.equitas.R;
public class AepsBaseActivity extends AppCompatActivity implements LocationListener {
    private SharedPreferences.Editor mEditor;
    private int mPermissionDeniedCount = 0;
    private LocationManager mLocationManager;
    private LocationUpdateListener mLocationUpdateListener;
    private SharedPreferences mLatLongSharedPreferences;

    private static final int TIMEOUT_MILLISECONDS = 3000; // Timeout duration in milliseconds
    private boolean mIsLocationReceived = false; // Flag to track if location is received

    private boolean mIsLocationDialogShowing = false;




    /**
     * Launcher for requesting runtime permissions.
     */
    private final ActivityResultLauncher<String> mRequestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    handleGps();
                    mEditor.putInt(getString(R.string.denied_count), 0);
                } else {
                    mPermissionDeniedCount += 1;
                    mEditor.putInt(getString(R.string.denied_count), mPermissionDeniedCount);
                    mEditor.apply();
                    checkLocationPermission();
                }
            });

    /**
     * Launcher for turning on location services.
     */
    private final ActivityResultLauncher<Intent> mTurnOnLocationLauncher =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> handleGps());

    /**
     * Launcher for launching settings to manage permissions.
     */
    private final ActivityResultLauncher<Intent> mPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> checkLocationPermission());

    /**
     * Sets the location update listener.
     *
     * @param listener The location update listener.
     */
    public void setLocationUpdateListener(LocationUpdateListener listener) {
        this.mLocationUpdateListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aeps_base);
        SharedPreferences mSharedPreference;
        mSharedPreference = getSharedPreferences(getString(R.string.unified_aeps), MODE_PRIVATE);
        mLatLongSharedPreferences = getSharedPreferences(getString(R.string.location_preference), MODE_PRIVATE);
        mEditor = mSharedPreference.edit();
        mPermissionDeniedCount = mSharedPreference.getInt(getString(R.string.denied_count), 0);
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

    }
    /**
     * Checks for location permissions. If the ACCESS_FINE_LOCATION permission is granted,
     * calls the handleGps() method to proceed with location handling. If the permission is granted,
     * checks if the permission has been denied twice. If denied twice, shows a settings dialog to prompt the user
     * to grant permission manually. Otherwise, requests the ACCESS_FINE_LOCATION permission from the user.
     */
    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED) {
            handleGps();
        } else if (mPermissionDeniedCount == 2) {
            showSettingsDialog();
        } else {
            mRequestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }


    /**
     * Displays a dialog prompting the user to grant location permission. If the user chooses to grant permission,
     * opens the application settings where the user can manually grant the required permission.
     * If the user cancels the dialog, finishes the activity.
     */
    private void showSettingsDialog() {
        showAlert(this, getString(R.string.location_permission_required),
                getString(R.string.please_grant_location_permission),
                getString(R.string.settings),
                getString(R.string.cancel),
                (dialog, which) -> {
                    // Open app settings
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    mPermissionLauncher.launch(intent);
                }, (dialog, which) -> finish());
    }


    /**
     * Handles GPS functionality. Checks if GPS is enabled on the device.
     * If GPS is enabled, proceeds to retrieve latitude and longitude coordinates.
     * If GPS is not enabled, displays a dialog prompting the user to enable GPS.
     * If the user chooses to enable GPS, opens the device settings where the user can enable GPS.
     * If the user cancels the dialog, finishes the activity.
     */
    private void handleGps() {
        boolean isGpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGpsEnabled) {
            mLocationUpdateListener.onLocationTurnedOn();
            getLatitudeLongitude();
        } else {
            showAlert(this, getString(R.string.alert),
                    getString(R.string.gps_is_not_enabled_do_you_want_to_go_to_settings_menu),
                    getString(R.string.settings),
                    getString(R.string.cancel),
                    (dialog, which) -> {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mTurnOnLocationLauncher.launch(intent);
                        mIsLocationDialogShowing = false;
                    }, (dialog, which) -> {
                        mIsLocationDialogShowing = false;
                        finish();
                    }
            );
        }
    }

    /**
     * Retrieves the latitude and longitude coordinates.
     * If the necessary permissions are granted, it requests location updates from
     * the LocationManager. If no location is received within a specified timeout period,
     * it resorts to using the FusedLocationProviderClient to retrieve the location.
     */
    private void getLatitudeLongitude() {
        // Initialize the FusedLocationProviderClient
        com.google.android.gms.location.FusedLocationProviderClient fusedLocationClient =
                LocationServices.getFusedLocationProviderClient(this);

        // Check if the necessary location permissions are granted
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED) {
            // If permissions are not granted, request them
            checkLocationPermission();
        } else {
            // Request location updates from LocationManager
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    1000,
                    0f,
                    this
            );
            mLocationManager.requestLocationUpdates(
                    LocationManager.PASSIVE_PROVIDER,
                    1000,
                    0f,
                    this
            );
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    1000,
                    0f,
                    this
            );
            // Set up a handler to implement timeout
            new Handler().postDelayed(() -> {
                if (!mIsLocationReceived) {
                    // If location is not received within the timeout period,
                    // request location from FusedLocationProviderClient
                    fusedLocationClient.getCurrentLocation(
                            LocationRequest.PRIORITY_HIGH_ACCURACY,
                            new CancellationTokenSource().getToken()
                    ).addOnCompleteListener(this::handleLocationListenerTask);
                }
            }, TIMEOUT_MILLISECONDS);

        }
    }


    /**
     * Handles the completion task of fetching the location.
     * Updates the location fetching loader status to false.
     * If the task is successful, extracts the latitude and longitude coordinates
     * from the task result and notifies the location update listener.
     * If the task encounters an exception, notifies the location update listener
     * about the error and logs the exception message.
     *
     * @param task The task representing the location fetching operation.
     */
    private void handleLocationListenerTask(Task<Location> task) {
        try {
            if (task.isSuccessful()) {
                getLatLongAndSaveInSharedPreference(task.getResult());
            } else if (task.getException() != null) {
                Utils.showLog("", task.getException().getLocalizedMessage() != null ?
                        task.getException().getLocalizedMessage() : "");
            }
        } catch (NullPointerException e) {
            Utils.showLog("", e.getLocalizedMessage() != null ?
                    e.getLocalizedMessage() : "");
        }
    }

    /**
     * Retrieves the latitude and longitude from the provided Location object and saves them in shared preferences.
     *
     * @param location The Location object containing the latitude and longitude information.
     */
    private void getLatLongAndSaveInSharedPreference(Location location) {
        mIsLocationReceived = true;
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        mLocationUpdateListener.onLocationUpdate(latitude + "," + longitude);
        SharedPreferences.Editor editor = mLatLongSharedPreferences.edit();
        editor.putString(getString(R.string.latlong), latitude + "," + longitude);
        editor.apply();
    }

    public void showAlert(Activity showAlertContext,
                          String title, String msg,
                          String positiveText, String negativeText,
                          DialogInterface.OnClickListener positiveClickListener,
                          DialogInterface.OnClickListener negativeClickListener) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(showAlertContext);
            builder.setTitle(title != null ? title : getString(R.string.alert));
            builder.setMessage(msg != null ? msg : "");

            if (positiveText != null && !positiveText.isEmpty()) {
                builder.setPositiveButton(positiveText, (dialog, which) -> {
                    if (positiveClickListener != null) {
                        positiveClickListener.onClick(dialog, which);
                        dialog.dismiss();
                    }
                });
            }

            if (negativeText != null && !negativeText.isEmpty()) {
                builder.setNegativeButton(negativeText, (dialog, which) -> {
                    if (negativeClickListener != null) {
                        negativeClickListener.onClick(dialog, which);
                        dialog.dismiss();
                    }
                });
            }

            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAlert(Activity showAlertContext,
                          String title, String msg,
                          String positiveText,
                          DialogInterface.OnClickListener positiveClickListener) {
        showAlert(showAlertContext, title, msg, positiveText, null, positiveClickListener, null);
    }

    /**
     * Called when the location has changed.
     *
     * @param location The new location, as a Location object.
     * @see #getLatLongAndSaveInSharedPreference(Location)
     */
    @Override
    public void onLocationChanged(@NonNull Location location) {
        getLatLongAndSaveInSharedPreference(location);
    }

    /**
     * Called when the provider is disabled.
     *
     * @param provider The name of the location provider that is disabled.
     */
    @Override
    public void onProviderDisabled(@NonNull String provider) {
        LocationListener.super.onProviderDisabled(provider);
        if (!mIsLocationDialogShowing) {
            mIsLocationReceived = false;
            mIsLocationDialogShowing = true; // Set flag to true to indicate that the dialogue is showing
            handleGps();
        }
    }


    public interface LocationUpdateListener {
        void onLocationUpdate(String latLong);
        void onLocationTurnedOn();

    }
    @NonNull
    public String getData(String data) {
        return (data == null || data.isEmpty()) ? getString(R.string.n_a) : data;
    }
}