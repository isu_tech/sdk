package com.matm.matmsdk.aepsmodule.unifiedaeps;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.AEPS2HomeActivity;
import com.matm.matmsdk.aepsmodule.utils.GetPosConnectedPrinter;
import com.matm.matmsdk.aepsmodule.utils.Session;
import com.matm.matmsdk.vriddhi.AEMPrinter;
import com.matm.matmsdk.vriddhi.AEMScrybeDevice;
import com.matm.matmsdk.vriddhi.CardReader;
import com.matm.matmsdk.vriddhi.IAemCardScanner;
import com.matm.matmsdk.vriddhi.IAemScrybe;
import java.io.IOException;
import java.util.ArrayList;
import isumatm.androidsdk.equitas.R;
import wangpos.sdk4.libbasebinder.Printer;
public class UnifiedAepsMiniStatementActivity extends AppCompatActivity implements IAemScrybe, IAemCardScanner {
    public static final String TAG = UnifiedAepsMiniStatementActivity.class.getSimpleName();
    private RelativeLayout failureLayout;
    private LinearLayout successLayout;
    private Button okButton;
    private Button okSuccessButton;
    private Button printBtnSuccess;
    private TextView tvStatusDescTxt;
    private TextView tvFailtxnID;
    private TextView tvAadharNumber;
    private TextView tvDateTime;
    private TextView tvBankName;
    private TextView tvCardTransactionType;
    private TextView tvTransactionDetailsHeaderTxt;
    private TextView tvAadharNumTxt;
    private TextView tvAccountBalanceTxt;
    private TextView tvTransactionIdTxt;
    private TextView tvBankNameTxt;
    private Session session;
    private ProgressDialog pd;
    private RecyclerView statement_list;
    private String statusTxt;
    private UnifiedTxnStatusModel transactionStatusModel;
    private UnifiedStatementListAdapter statementList_adapter;
    private BluetoothAdapter bluetoothAdapter;
    private ArrayList<String> printerList;
    private AEMScrybeDevice m_AemScrybeDevice;
    private AEMPrinter m_AemPrinter = null;
    private String creditData;
    private String tempdata;
    private String replacedData;
    private String printerName;
    private CardReader m_cardReader = null;
    private CardReader.CARD_TRACK cardTrackType;
    private String responseString, response;
    private String[] responseArray = new String[1];
    private wangpos.sdk4.libbasebinder.Printer mPrinter;
    private ArrayList<UnifiedTxnStatusModel.MiniStatement> miniStatementsList;
    private TransactionType transactionType;
    private String txnType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SdkConstants.statementLayout == 0) {
            setContentView(R.layout.activity_statement_transaction);
        } else {
            setContentView(SdkConstants.statementLayout);
        }
        initializeViews();
        if(miniStatementsList == null || miniStatementsList.size()==0){
            printBtnSuccess.setVisibility(View.INVISIBLE);
        }
        try {
            pd.show();
            pd.setMessage("Loading..");
            statementList_adapter = new UnifiedStatementListAdapter(UnifiedAepsMiniStatementActivity.this, transactionStatusModel.getMinistatement(), pd);
            statement_list.setAdapter(statementList_adapter);
            pd.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        okButton.setOnClickListener(v -> onBackPressed());
        okSuccessButton.setOnClickListener(v -> {
            if(SdkConstants.onFinishListener != null){
                SdkConstants.onFinishListener.onSDKFinish(statusTxt,SdkConstants.paramA
                        , tvAccountBalanceTxt.getText().toString());
            }
            finish();
            Intent respIntent = new Intent();
            setResult(Activity.RESULT_OK, respIntent);
            finish();
        });
        printBtnSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deviceModel = android.os.Build.MODEL;
                if (deviceModel.equalsIgnoreCase("A910")) {
                } else if (deviceModel.equalsIgnoreCase("WPOS-3")) {
                    //start printing with wiseasy internal printer
                    new UnifiedAepsMiniStatementActivity.UnifiedPrintReceiptThread().start();
                } else {
                    registerForContextMenu(printBtnSuccess);
                    if (bluetoothAdapter == null) {
                        Toast.makeText(UnifiedAepsMiniStatementActivity.this, "Bluetooth NOT supported", Toast.LENGTH_SHORT).show();
                    } else {
                        if (bluetoothAdapter.isEnabled()) {
                            if (GetPosConnectedPrinter.aemPrinter == null) {
                                printerList = m_AemScrybeDevice.getPairedPrinters();
                                if (printerList.size() > 0) {
                                    openContextMenu(v);
                                } else {
                                    showAlert("No Paired Printers found");
                                }
                            } else {
                                m_AemPrinter = GetPosConnectedPrinter.aemPrinter;
                                callBluetoothFunction(tvTransactionIdTxt.getText().toString(), tvAadharNumTxt.getText().toString(), tvAccountBalanceTxt.getText().toString(), v);
                            }
                        } else {
                            GetPosConnectedPrinter.aemPrinter = null;
                            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(turnOn, 0);
                        }
                    }
                }
            }
        });
    }
    private void initializeViews() {
        session = new Session(UnifiedAepsMiniStatementActivity.this);
        pd = new ProgressDialog(UnifiedAepsMiniStatementActivity.this);
        statement_list = findViewById(R.id.statement_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(UnifiedAepsMiniStatementActivity.this);
        statement_list.setLayoutManager(linearLayoutManager);
        tvStatusDescTxt = findViewById(R.id.statusDescTxt);
        tvTransactionDetailsHeaderTxt = findViewById(R.id.transaction_details_header_txt);
        tvFailtxnID = findViewById(R.id.txnID);
        tvAadharNumber = findViewById(R.id.aadhar_number);
        tvDateTime = findViewById(R.id.date_time);
        tvBankName = findViewById(R.id.bank_name);
        tvCardTransactionType = findViewById(R.id.card_transaction_type);
        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        printBtnSuccess = findViewById(R.id.success_print_button);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        m_AemScrybeDevice = new AEMScrybeDevice(UnifiedAepsMiniStatementActivity.this);
        tvAadharNumTxt = findViewById(R.id.aadhar_num_txt);
        tvAccountBalanceTxt = findViewById(R.id.account_balance_txt);
        tvTransactionIdTxt = findViewById(R.id.transaction_id_txt);
        tvBankNameTxt = findViewById(R.id.bank_name_txt);
        transactionStatusModel = getIntent().getParcelableExtra(SdkConstants.TRANSACTION_STATUS_KEY);
        failureLayout.setVisibility(View.GONE);
        successLayout.setVisibility(View.VISIBLE);
        tvAadharNumTxt.setText(transactionStatusModel.getAadharCard());
        tvTransactionIdTxt.setText(transactionStatusModel.getTxnID());
        tvBankNameTxt.setText(transactionStatusModel.getBankName());
        tvAccountBalanceTxt.setText("Rs." + transactionStatusModel.getBalanceAmount());
        miniStatementsList = transactionStatusModel.getMinistatement();
        statusTxt = "Success";
    }

    //    WPOS-3 PRINT
    private class UnifiedPrintReceiptThread  extends Thread{
        @Override
        public void run() {
            mPrinter=new Printer(UnifiedAepsMiniStatementActivity.this);
            try {
                mPrinter.setPrintType(0);//Printer type 0 means it's an internal printer
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            checkPrinterStatus();
        }
    }
    private void checkPrinterStatus() {
        try {
            int[] status = new int[1];
            mPrinter.getPrinterStatus(status);
            Log.e(TAG,"Printer Status is "+status[0]);
            String msg="";
            switch (status[0]){
                case 0x00:
                    msg="Printer status OK";
                    Log.e(TAG, "check printer status: "+msg );
                    startPrinting();
                    break;
                case 0x01:
                    msg="Parameter error";
                    showLog(msg);
                    break;
                case 0x8A://----138 return
                    msg="Out of Paper";
                    showLog(msg);
                    break;
                case 0x8B:
                    msg="Overheat";
                    showLog(msg);
                    break;
                default:
                    msg="Printer Error";
                    showLog(msg);
                    break;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    private void showLog(String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(UnifiedAepsMiniStatementActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
        Log.e(TAG, "Printer status: "+msg );
    }
    /**
     * First initialize the printer and clear if any catch data is present
     *
     * After initialization of printer then start printing
     * */
    private void startPrinting() {
        int result = -1;
        try {
            result = mPrinter.printInit();
            Log.e(TAG, "startPrinting: Printer init result "+result );
            mPrinter.clearPrintDataCache();
            if (result==0){
                printReceipt();
            }else {
                Toast.makeText(this, "Printer initialization failed", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void printReceipt() {
        int result=-1;
        try {
            Log.e(TAG, "printReceipt: set density low 3" );
            mPrinter.setGrayLevel(3);
            result = mPrinter.printStringExt(SdkConstants.SHOP_NAME, 0,0f,2.0f, Printer.Font.SANS_SERIF, 25, Printer.Align.CENTER,true,false,true);
            result = mPrinter.printString("Success", 25, Printer.Align.CENTER, true, false);
            result = mPrinter.printString("Transaction Id :"+ tvTransactionIdTxt.getText(),20, Printer.Align.LEFT,false,false);
            result = mPrinter.printString("Balance Amount :"+ tvAccountBalanceTxt.getText(),20, Printer.Align.LEFT,false,false);
            result = mPrinter.printString("Aadhaar Number :"+ tvAadharNumTxt.getText(),20, Printer.Align.LEFT,false,false);
            result = mPrinter.printString("STATEMENT",23, Printer.Align.CENTER,true,false);
            if(miniStatementsList.size() == 0){
            }else {
                for (int i = 0; i< miniStatementsList.size(); i++){
                    UnifiedTxnStatusModel.MiniStatement mSList = miniStatementsList.get(i);
                    txnType=mSList.getDebitCredit();
                    transactionType=new TransactionType(DebitCredit.valueOf(txnType));
                    result = mPrinter.printString(mSList.getDate() + "                              "+transactionType.transactionType()+mSList.getAmount(), 20, Printer.Align.LEFT, false,false);
                    result = mPrinter.printString(mSList.getType(), 20, Printer.Align.LEFT, false, false);

                }
            }
            result = mPrinter.printStringExt("Thank You", 0,0f,2.0f, Printer.Font.SANS_SERIF, 20, Printer.Align.RIGHT,true,true,false);
            result = mPrinter.printStringExt(SdkConstants.BRAND_NAME, 0,0f,2.0f, Printer.Font.SANS_SERIF, 20, Printer.Align.RIGHT,true,true,false);
            result = mPrinter.printString(" ",25, Printer.Align.CENTER,false,false);
            result = mPrinter.printString(" ",25, Printer.Align.CENTER,false,false);
            Log.e(TAG, "printReceipt: print thank you result "+result );
            result = mPrinter.printPaper(30);
            Log.e(TAG, "printReceipt: print step result "+result );
            showPrinterStatus(result);
            result = mPrinter.printFinish();
            Log.e(TAG, "printReceipt: printer finish result "+result );
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void showPrinterStatus(int result) {
        String msg="";
        switch (result){
            case 0x00:
                msg="Print Finish";
                showLog(msg);
                break;
            case 0x01:
                msg="Parameter error";
                showLog(msg);
                break;
            case 0x8A://----138 return
                msg="Out of Paper";
                showLog(msg);
                break;
            case 0x8B:
                msg="Overheat";
                showLog(msg);
                break;
            default:
                msg="Printer Error";
                showLog(msg);
                break;
        }
    }
    @Override
    public void onBackPressed() {
        try {
            if (statusTxt.equalsIgnoreCase("FAILED")) {
                Intent intent = new Intent(UnifiedAepsMiniStatementActivity.this, AEPS2HomeActivity.class);
                intent.putExtra("FAILEDVALUE", "FAILEDDATA");
                startActivity(intent);
                finish();
            } else {
                if(SdkConstants.onFinishListener != null){
                    SdkConstants.onFinishListener.onSDKFinish(statusTxt,SdkConstants.paramA
                            , tvAccountBalanceTxt.getText().toString());
                }
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void onDiscoveryComplete(ArrayList<String> aemPrinterList) {
        printerList = aemPrinterList;
        for (int i = 0; i < aemPrinterList.size(); i++) {
            String Device_Name = aemPrinterList.get(i);
            String status = m_AemScrybeDevice.pairPrinter(Device_Name);
            Log.e("STATUS", status);
        }
    }
    public void showAlert(String alertMsg) {
        android.app.AlertDialog.Builder alertBox = new android.app.AlertDialog.Builder(UnifiedAepsMiniStatementActivity.this);
        alertBox.setMessage(alertMsg).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
            }
        });
        android.app.AlertDialog alert = alertBox.create();
        alert.show();
    }
    private void callBluetoothFunction(final String txnId, final String aadharNo, String accountBlance, View view) {
        try {
            m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
            m_AemPrinter.setFontType(AEMPrinter.FONT_002);
            m_AemPrinter.POS_FontThreeInchCENTER();
            Drawable d1 = getResources().getDrawable(R.drawable.digipay_logo_print);
            BitmapDrawable bitDw1 = ((BitmapDrawable) d1);
            Bitmap bmp1 = bitDw1.getBitmap();
            m_AemPrinter.printImage(bmp1);
            m_AemPrinter.POS_FontThreeInchCENTER();
            m_AemPrinter.POS__FontThreeInchDOUBLEHIEGHT();
            m_AemPrinter.print(SdkConstants.SHOP_NAME.trim());
            m_AemPrinter.print("\n");
            m_AemPrinter.POS_FontThreeInchCENTER();
            m_AemPrinter.POS__FontThreeInchDOUBLEHIEGHT();
            m_AemPrinter.print("SUCCESS");
            m_AemPrinter.print("\n");
            m_AemPrinter.print("\n");
            m_AemPrinter.print("Txn id: " + txnId);
            m_AemPrinter.print("\n");
            m_AemPrinter.print("Available Balance: ");
            m_AemPrinter.print(accountBlance);
            m_AemPrinter.print("\n");
            m_AemPrinter.print("Aadhar No: " + aadharNo);
            m_AemPrinter.print("\n");
            m_AemPrinter.print("\n");
            m_AemPrinter.POS_FontThreeInchCENTER();
            m_AemPrinter.print("Statement");
            m_AemPrinter.print("\n");
            m_AemPrinter.print("\n");
            if (miniStatementsList.size() == 0) {
            } else {
                for (int i = 0; i < miniStatementsList.size(); i++) {
                    UnifiedTxnStatusModel.MiniStatement mSList = miniStatementsList.get(i);
                    txnType=mSList.getDebitCredit();
                    transactionType=new TransactionType(DebitCredit.valueOf(txnType));
                    m_AemPrinter.print(mSList.getDate().trim() + "             " + transactionType.transactionType() + mSList.getAmount() + "\n" + mSList.getType().trim());
                    m_AemPrinter.print("\n");
                }
            }
            m_AemPrinter.print("Thank You");
            m_AemPrinter.POS_FontThreeInchRIGHT();
            m_AemPrinter.print("\n");
            m_AemPrinter.print(SdkConstants.BRAND_NAME.trim());
            m_AemPrinter.POS_FontThreeInchRIGHT();
            m_AemPrinter.print("\n");
            m_AemPrinter.POS_FontThreeInchCENTER();
            Drawable d = getResources().getDrawable(R.drawable.csc_logo_isu);
            BitmapDrawable bitDw = ((BitmapDrawable) d);
            Bitmap bmp = bitDw.getBitmap();
            m_AemPrinter.printImage(bmp);
            m_AemPrinter.print("\n");
            m_AemPrinter.print("\n");
            m_AemPrinter.print("\n");
            m_AemPrinter.POS_FontThreeInchCENTER();
            m_AemPrinter.print(" ");
            m_AemPrinter.print(" ");
            m_AemPrinter.print("\n");
        } catch (IOException e) {
            try {
                GetPosConnectedPrinter.aemPrinter = null;
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select Printer to connect");
        for (int i = 0; i < printerList.size(); i++) {
            menu.add(0, v.getId(), 0, printerList.get(i));
        }
    }
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        super.onContextItemSelected(item);
        printerName = item.getTitle().toString();
        try {
            m_AemScrybeDevice.connectToPrinter(printerName);
            m_cardReader = m_AemScrybeDevice.getCardReader(this);
            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            GetPosConnectedPrinter.aemPrinter = m_AemPrinter;
            Toast.makeText(UnifiedAepsMiniStatementActivity.this, "Connected with " + printerName, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            if (e.getMessage().contains("Service discovery failed")) {
                Toast.makeText(UnifiedAepsMiniStatementActivity.this, "Not Connected\n" + printerName + " is unreachable or off otherwise it is connected with other device", Toast.LENGTH_SHORT).show();
            } else if (e.getMessage().contains("Device or resource busy")) {
                Toast.makeText(UnifiedAepsMiniStatementActivity.this, "the device is already connected", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(UnifiedAepsMiniStatementActivity.this, "Unable to connect", Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }
    @Override
    public void onScanMSR(final String buffer, CardReader.CARD_TRACK cardTrack) {
        cardTrackType = cardTrack;
        creditData = buffer;
        this.runOnUiThread(new Runnable() {
            public void run() {
            }
        });
    }
    @Override
    public void onScanDLCard(final String buffer) {
        CardReader.DLCardData dlCardData = m_cardReader.decodeDLData(buffer);
        String name = "NAME:" + dlCardData.NAME + "\n";
        String SWD = "SWD Of: " + dlCardData.SWD_OF + "\n";
        String dob = "DOB: " + dlCardData.DOB + "\n";
        String dlNum = "DLNUM: " + dlCardData.DL_NUM + "\n";
        String issAuth = "ISS AUTH: " + dlCardData.ISS_AUTH + "\n";
        String doi = "DOI: " + dlCardData.DOI + "\n";
        String tp = "VALID TP: " + dlCardData.VALID_TP + "\n";
        String ntp = "VALID NTP: " + dlCardData.VALID_NTP + "\n";
        final String data = name + SWD + dob + dlNum + issAuth + doi + tp + ntp;
        runOnUiThread(new Runnable() {
            public void run() {
            }
        });
    }
    @Override
    public void onScanRCCard(final String buffer) {
        CardReader.RCCardData rcCardData = m_cardReader.decodeRCData(buffer);
        String regNum = "REG NUM: " + rcCardData.REG_NUM + "\n";
        String regName = "REG NAME: " + rcCardData.REG_NAME + "\n";
        String regUpto = "REG UPTO: " + rcCardData.REG_UPTO + "\n";
        final String data = regNum + regName + regUpto;
        runOnUiThread(new Runnable() {
            public void run() {
            }
        });
    }
    @Override
    public void onScanRFD(final String buffer) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(buffer);
        String temp = "";
        try {
            temp = stringBuffer.deleteCharAt(8).toString();
        } catch (Exception e) {
            // TODO: handle exception
        }
        final String data = temp;
        this.runOnUiThread(() -> {
            try {
                m_AemPrinter.print(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
    @Override
    public void onScanPacket(String buffer) {
        if (buffer.equals(getString(R.string.printerok))) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(buffer);
            String temp = "";
            try {
                temp = stringBuffer.toString();
            } catch (Exception e) {
                // TODO: handle exception
            }
            tempdata = temp;
            final String strData = tempdata.replace("|", "&");
            final String[][] formattedData = {strData.split("&", 3)};
            responseString = formattedData[0][2];
            responseArray[0] = responseString.replace("^", "");
            this.runOnUiThread(() -> {
                replacedData = tempdata.replace("|", "&");
                formattedData[0] = replacedData.split("&", 3);
                response = formattedData[0][2];
            });
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(buffer);
            String temp = "";
            try {
                temp = stringBuffer.toString();
            } catch (Exception e) {
                // TODO: handle exception
            }
            tempdata = temp;
            final String strData = tempdata.replace("|", "&");
            final String[][] formattedData = {strData.split("&", 3)};
            responseString = formattedData[0][2];
            responseArray[0] = responseString.replace("^", "");
            this.runOnUiThread(() -> {
                replacedData = tempdata.replace("|", "&");
                formattedData[0] = replacedData.split("&", 3);
                response = formattedData[0][2];
            });
        }
    }
}