package com.matm.matmsdk.aepsmodule.unifiedaeps;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.UnifiedAepsTransactionActivity.TAG;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.getCursorPos;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.makePrettyString;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.showLog;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.google.gson.Gson;
import com.matm.matmsdk.CustomThemes;
import com.matm.matmsdk.GpsTracker;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.alertShow.SingletonClass;
import com.matm.matmsdk.aepsmodule.bankspinner.BankNameListActivity;
import com.matm.matmsdk.aepsmodule.unifiedaeps.utils.AadhaarTransformation;
import com.matm.matmsdk.aepsmodule.unifiedaeps.utils.VIDTransformation;
import com.matm.matmsdk.aepsmodule.utils.Constants;
import com.matm.matmsdk.aepsmodule.utils.Session;
import com.matm.matmsdk.aepsmodule.utils.Util;
import com.matm.matmsdk.callbacks.OnDriverDataListener;
import com.moos.library.HorizontalProgressView;
import org.json.JSONException;
import org.json.JSONObject;
import isumatm.androidsdk.equitas.R;
/**
 * The `UnifiedAepsActivity` class represents the main activity for Unified AEPS functionality.
 * It extends `AepsBaseActivity` and implements various interfaces such as `UnifiedAepsContract.View`,
 * `OnDriverDataListener`, and `AepsBaseActivity.LocationUpdateListener`.
 * This activity handles user interactions related to Aadhar and virtual ID-based transactions,
 * including balance inquiry, cash withdrawal, and fingerprint-based operations.
 * It includes UI elements such as checkboxes, text views, edit text fields, image views, buttons,
 * and a progress bar. Additionally, it utilizes various components such as a GPS tracker for location updates,
 * a presenter (`UnifiedAepsPresenter`) for business logic, and a session manager (`Session`) for managing user sessions.
 * The activity also incorporates functionality for handling Aadhar and virtual ID validation,
 * @author Smrutilipsa Patel
 */
public class UnifiedAepsActivity extends AepsBaseActivity implements
        UnifiedAepsContract.View, OnDriverDataListener, AepsBaseActivity.LocationUpdateListener {
    private Boolean adharbool = true;
    private Boolean virtualbool = false;
    private AppCompatCheckBox terms;
    private Session session;
    private UnifiedAepsRequestModel unifiedAepsRequestModel;
    private String bankIINNumber = "";
    private ProgressDialog loadingView;
    private String flagNameRdService = "";
    private Class<?> mDriverActivity;
    private String balanaceInqueryAadharNo = "";
    private boolean flagFromDriver = false;
    private TextView tvVirtualidText;
    private TextView tvAadharText;
    private boolean mInside = false;
    private boolean mWannaDeleteHyphen = false;
    private boolean mKeyListenerSet = false;
    private String latLong = "";
    private TypedValue typedValue;
    private Resources.Theme theme;
    private EditText etAadharNumber;
    private EditText etAadharVirtualID;
    private TextView tvBalanceEnquiryExpandButton;
    private TextView tvCashWithdrawalButton;
    private TextView tvFingerprintStrengthDeposit;
    private TextView tvDepositNote;
    private EditText etMobileNumber;
    private EditText etBankspinner;
    private EditText etAmountEnter;
    private ImageView ivFingerprint;
    private ImageView ivVirtualID;
    private ImageView ivAadhaar;
    private HorizontalProgressView depositBar;
    private Button submitButton;
    private GpsTracker gpsTracker;
    private int gatewayPriority = 0;
    //notification 22 july
    private UnifiedAepsPresenter unifiedAepsPresenter;
    private String aadharNumberMain = "";
    // to save latlong
    private String lastLatlong;
    public static final String SHARED_PREFS = "LastLatlong";
    private String dataObj;
    private int counter = 30;
    private TextView tvTimer;
    /**
     * Called when the activity is first created. Initializes the activity, sets up UI elements,
     * and checks for necessary permissions and services.
     * @param savedInstanceState The saved instance state of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the content view based on the provided dashboard layout or the default layout
        if (SdkConstants.dashboardLayout != 0) {
            setContentView(SdkConstants.dashboardLayout);
        } else {
            setContentView(R.layout.activity_unified_aeps);
        }
        new CustomThemes(this);
        session = new Session(UnifiedAepsActivity.this); // Create a session manager instance
        fetchParentApplicationData(); // Fetch data from the parent application
        initializeViews(); // Initialize UI elements
        setLocationUpdateListener(this); // Set location update listener
        checkLocationPermission(); // Check and request location permission

        getRDServiceClass(); // Get the RDService class
        // Initialize typed value and theme for UI customization
        typedValue = new TypedValue();
        theme = this.getTheme();
        // Check and request fine location permission if not granted
        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{
                        android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getLocation(); // Get current location
        disableAutofill(); // Disable autofill for improved security
        // Check if the transaction type is "1"(cashWithdrawal)
       /* if (SdkConstants.transactionType.equalsIgnoreCase("1")) {
            // If true, make the timer visible and start the timer
            tvTimer.setVisibility(View.VISIBLE);
            statTimer();
        }*/
        // Check if the transaction type is a balance enquiry
        if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.balanceEnquiry)) {
            // Show balance enquiry expand button and hide cash withdrawal button and amount enter field
            tvBalanceEnquiryExpandButton.setVisibility(View.VISIBLE);
            tvCashWithdrawalButton.setVisibility(View.GONE);
            etAmountEnter.setVisibility(View.GONE);
        }  // Check if the transaction type is a mini statement
        else if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.ministatement)) {
            // Show balance enquiry expand button with updated text for mini statement,
            // and hide cash withdrawal button and amount enter field
            tvBalanceEnquiryExpandButton.setVisibility(View.VISIBLE);
            tvBalanceEnquiryExpandButton.setText(getResources().getString(R.string.mini_statement));
            tvCashWithdrawalButton.setVisibility(View.GONE);
            etAmountEnter.setVisibility(View.GONE);
        } // Check if the transaction type is a cash withdrawal
        else if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.cashWithdrawal)) {
            // Hide balance enquiry expand button, show cash withdrawal button,
            // and set the amount enter field with the transaction amount
            tvBalanceEnquiryExpandButton.setVisibility(View.GONE);
            tvCashWithdrawalButton.setVisibility(View.VISIBLE);
            etAmountEnter.setText(SdkConstants.transactionAmount);
        } // Check if the transaction type is a cash deposit
        else if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.cashDeposit)) {
            // Hide balance enquiry expand button, show cash withdrawal button with updated text for cash deposit,
            // and set the amount enter field with the transaction amount
            tvBalanceEnquiryExpandButton.setVisibility(View.GONE);
            tvCashWithdrawalButton.setVisibility(View.VISIBLE);
            tvCashWithdrawalButton.setText(R.string.cash_deposit);
            etAmountEnter.setText(SdkConstants.transactionAmount);
        }
        etAmountEnter.setEnabled(SdkConstants.editable);
        ivVirtualID.setBackgroundResource(R.drawable.ic_language);
        tvVirtualidText.setTextColor(getResources().getColor(R.color.grey));
        ivAadhaar.setBackgroundResource(R.drawable.ic_fingerprint_blue);
        etBankspinner.setOnClickListener(v -> {
            // Clear focus from the mobile number field
            etMobileNumber.clearFocus();
            // Create an intent to navigate to the BankNameListActivity
            Intent in = new Intent(UnifiedAepsActivity.this, BankNameListActivity.class);
            // Check the transaction type to determine the request code for startActivityForResult
            // Start the BankNameListActivity for balance enquiry
            if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.balanceEnquiry)) {
                startActivityForResult(in, SdkConstants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE);
            } // Start the BankNameListActivity for cash withdrawal or cash deposit
            else if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.cashWithdrawal)||
                    SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.cashDeposit)
            ) {
                startActivityForResult(in, SdkConstants.REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE);
            }// Start the BankNameListActivity for mini statement
            else if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.ministatement)) {
                startActivityForResult(in, SdkConstants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE);
            }
        });
        /**
         * Sets up a click listener for the fingerprint icon (`ivFingerprint`).
         * Initiates the fingerprint-based operation by launching the specified driver activity.
         */
        ivFingerprint.setOnClickListener(v -> {
            try {
                showLoader();
                ivFingerprint.setEnabled(false);
                ivFingerprint.setColorFilter(R.color.colorGrey);
                flagFromDriver = true;
                Intent launchIntent = new Intent(UnifiedAepsActivity.this, mDriverActivity);
                launchIntent.putExtra(getString(R.string.driverflag), flagNameRdService);
                launchIntent.putExtra(getString(R.string.freshnesfactor), session.getFreshnessFactor());
                launchIntent.putExtra(getString(R.string.aadharno), balanaceInqueryAadharNo);
                startActivity(launchIntent);
            } catch (Exception e) {
                e.printStackTrace();
                hideLoader();
            }
        });
        /**
         * Adds a `TextWatcher` to the Aadhar number `EditText` (`etAadharNumber`).
         * It sets up a `KeyListener` to handle backspace deletion of hyphens within the Aadhar number.
         */
        etAadharNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!mKeyListenerSet) {
                    etAadharNumber.setOnKeyListener((v, keyCode, event) -> {
                        try {
                            mWannaDeleteHyphen = (keyCode == KeyEvent.KEYCODE_DEL
                                    && etAadharNumber.getSelectionEnd() - etAadharNumber.getSelectionStart() <= 1
                                    && etAadharNumber.getSelectionStart() > 0
                                    && etAadharNumber.getText().toString().charAt(etAadharNumber.getSelectionEnd() - 1) == '-');
                        } catch (IndexOutOfBoundsException e) {
                            // never to happen because of checks
                        }
                        return false;
                    });
                    mKeyListenerSet = true;
                }
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mInside) // to avoid recursive calls
                    return;
                mInside = true;
                int currentPos = etAadharNumber.getSelectionStart();
                String string = etAadharNumber.getText().toString().toUpperCase();
                String newString = makePrettyString(string);
                if (count == 14) {
                    ivFingerprint.setEnabled(true);
                    ivFingerprint.setClickable(true);
                    theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                    int color = typedValue.data;
                    ivFingerprint.setColorFilter(color);
                }
                etAadharNumber.setText(newString);
                try {
                    etAadharNumber.setSelection(getCursorPos(string, newString, currentPos, mWannaDeleteHyphen));
                } catch (IndexOutOfBoundsException e) {
                    etAadharNumber.setSelection(etAadharNumber.length()); // last resort never to happen
                }
                mWannaDeleteHyphen = false;
                mInside = false;
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    etAadharNumber.setError(null);
                    String aadharNo = etAadharNumber.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                        balanaceInqueryAadharNo = aadharNo;
                        if (balanaceInqueryAadharNo.length() >= 12) {
                            if (Util.validateAadharNumber(aadharNo) == false) {
                                etAadharNumber.setError(getResources().getString(R.string.Validaadhaarerror));
                            } else {
                                ivFingerprint.setEnabled(true);
                                ivFingerprint.setClickable(true);
                                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                                int color = typedValue.data;
                                ivFingerprint.setColorFilter(color);
                                etAadharNumber.clearFocus();
                                etMobileNumber.requestFocus();
                            }
                        }
                    }
                }
            }
        });
        etAadharNumber.setTransformationMethod(new AadhaarTransformation());
        etAadharVirtualID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!mKeyListenerSet) {
                    etAadharVirtualID.setOnKeyListener((v, keyCode, event) -> {
                        try {
                            mWannaDeleteHyphen = (keyCode == KeyEvent.KEYCODE_DEL
                                    && etAadharVirtualID.getSelectionEnd() - etAadharVirtualID.getSelectionStart() <= 1
                                    && etAadharVirtualID.getSelectionStart() > 0
                                    && etAadharVirtualID.getText().toString().charAt(
                                            etAadharVirtualID.getSelectionEnd() - 1) == '-');
                        } catch (IndexOutOfBoundsException e) {
                            // never to happen because of checks
                        }
                        return false;
                    });
                    mKeyListenerSet = true;
                }
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mInside) // to avoid recursive calls
                    return;
                mInside = true;
                int currentPos = etAadharVirtualID.getSelectionStart();
                String string = etAadharVirtualID.getText().toString().toUpperCase();
                String newString = makePrettyString(string);
                if (count == 19) {
                    ivFingerprint.setEnabled(true);
                    ivFingerprint.setClickable(true);
                    theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                    int color = typedValue.data;
                    ivFingerprint.setColorFilter(color);
                }
                etAadharVirtualID.setText(newString);
                try {
                    etAadharVirtualID.setSelection(getCursorPos(string, newString, currentPos, mWannaDeleteHyphen));
                } catch (IndexOutOfBoundsException e) {
                    etAadharVirtualID.setSelection(etAadharVirtualID.length()); // last resort never to happen
                }
                mWannaDeleteHyphen = false;
                mInside = false;
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    etAadharVirtualID.setError(null);
                    String aadharNo = etAadharVirtualID.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                        balanaceInqueryAadharNo = aadharNo;
                        if (balanaceInqueryAadharNo.length() >= 16) {
                            if (Util.validateAadharVID(aadharNo) == false) {
                                etAadharVirtualID.setError(getResources().getString(R.string.valid_aadhar__uid_error));
                            } else {
                                ivFingerprint.setEnabled(true);
                                ivFingerprint.setClickable(true);
                                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                                int color = typedValue.data;
                                ivFingerprint.setColorFilter(color);
                                etAadharVirtualID.clearFocus();
                                etMobileNumber.requestFocus();
                            }
                        }
                    }
                }
            }
        });
        etAadharVirtualID.setTransformationMethod(new VIDTransformation());
        etBankspinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    etBankspinner.setError(getResources().getString(R.string.select_bank_error));
                    etMobileNumber.clearFocus();
                }
                if (s.length() > 0) {
                    etBankspinner.setError(null);
                }
            }
        });
        etAmountEnter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    etAmountEnter.setError(getResources().getString(R.string.amount_error));
                }
                if (s.length() > 0) {
                    etAmountEnter.setError(null);
                }
            }
        });
        etMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                String number = "";
                if (s.length() > 0 && s.length() < 9) {
                    etMobileNumber.setError(null);
                    number = s.toString();
                    if (number.startsWith("0")) {
                        etMobileNumber.setError(getResources().getString(R.string.mobilevaliderror));
                    }
                }
                if ((s.length() == 10) && !Util.isValidMobile(etMobileNumber.getText().toString().trim())) {
                    if (number.startsWith("0")) {
                        etMobileNumber.setError(getResources().getString(R.string.mobilevaliderror));
                    } else {
                        etMobileNumber.setError(getResources().getString(R.string.mobilevaliderror));
                    }
                }
            }
        });
        ivVirtualID.setOnClickListener(v -> {
            /* Will implement when VID will used*/
            etAadharNumber.setVisibility(View.GONE);
            etAadharVirtualID.setVisibility(View.VISIBLE);
            ivVirtualID.setEnabled(false);
            ivAadhaar.setEnabled(true);
            virtualbool = true;
            adharbool = false;
            ivVirtualID.setBackgroundResource(R.drawable.ic_language_blue);
            theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
            int color = typedValue.data;
            tvVirtualidText.setTextColor(color);
            ivAadhaar.setBackground(getResources().getDrawable(R.drawable.ic_fingerprint_grey));
            tvAadharText.setTextColor(getResources().getColor(R.color.grey));
        });
        ivAadhaar.setOnClickListener(v -> {
            etAadharNumber.setVisibility(View.VISIBLE);
            etAadharVirtualID.setVisibility(View.GONE);
            ivVirtualID.setEnabled(true);
            ivAadhaar.setEnabled(false);
            ivVirtualID.setBackgroundResource(R.drawable.ic_language);
            tvVirtualidText.setTextColor(getResources().getColor(R.color.grey));
            adharbool = true;
            virtualbool = false;
            ivAadhaar.setBackgroundResource(R.drawable.ic_fingerprint_blue);
            theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
            int color = typedValue.data;
            tvAadharText.setTextColor(color);
        });
        submitButton.setOnClickListener(v -> {
            String balanceaadharNo = "";
            String balanceaadharVid = "";
            balanceaadharNo = etAadharNumber.getText().toString();
            if (adharbool) {
                if (balanceaadharNo.contains("-")) {
                    balanceaadharNo = balanceaadharNo.replaceAll("-", "").trim();
                }
                SdkConstants.AADHAAR_NUMBER = etAadharNumber.getText().toString().trim();
                aadharNumberMain = SdkConstants.AADHAAR_NUMBER;
                if (aadharNumberMain== null || !Util.validateAadharNumber(balanaceInqueryAadharNo)) {
                    etAadharNumber.setError(getResources().getString(R.string.Validaadhaarerror));
                    return;
                }
            } else if (virtualbool) {
                balanceaadharVid = etAadharVirtualID.getText().toString().trim();
                if (balanceaadharVid.contains("-")) {
                    balanceaadharVid = balanceaadharVid.replaceAll("-", "").trim();
                }
                SdkConstants.AADHAAR_NUMBER = etAadharVirtualID.getText().toString().trim();
                aadharNumberMain = SdkConstants.AADHAAR_NUMBER;
                if (aadharNumberMain== null || !Util.validateAadharVID(balanceaadharVid)) {
                    etAadharNumber.setError(getResources().getString(R.string.Validaviderror));
                    return;
                }
            }
            if (!flagFromDriver) {
                Toast.makeText(UnifiedAepsActivity.this, getString(R.string.please_do_biometric_verification), Toast.LENGTH_LONG).show();
                return;
            } else {
                try {
                    JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
                    String scoreStr = respObj.getString(getString(R.string.pidata_qscore));
                    String[] scoreList = scoreStr.split(",");
                    scoreStr = scoreList[0];
                    if (Float.parseFloat(scoreStr) <= 40) {
                        showAlert(getString(R.string.bad_fingerprint_strength_please_try_again));
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (etMobileNumber.getText() == null || etMobileNumber.getText().toString().trim().matches("") ||
                    !Util.isValidMobile(etMobileNumber.getText().toString().trim())) {
                etMobileNumber.setError(getResources().getString(R.string.mobileerror));
                return;
            }
            SdkConstants.MOBILENUMBER = etMobileNumber.getText().toString().trim();
            String panaaadhaar = etMobileNumber.getText().toString().trim();

            if (panaaadhaar.contains(" ") && panaaadhaar.length() != 10) {
                etMobileNumber.setError(getResources().getString(R.string.mobileerror));
                return;
            }
            if (etBankspinner.getText() == null || etBankspinner.getText().toString().trim().matches("")) {
                etBankspinner.setError(getResources().getString(R.string.select_bank_error));
                return;
            }
            SdkConstants.BANK_NAME = etBankspinner.getText().toString().trim();
            String userType;
            if (!SdkConstants.applicationType.equalsIgnoreCase(getString(R.string.core))){
                userType = getString(R.string.apiuser);
            }else{
                userType = getString(R.string.androiduser);
            }
            if (SdkConstants.firstCheck) {
                    if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.balanceEnquiry)) {
                        showLoader();
                        try {
                            JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
                            String pidData = respObj.getString(getString(R.string.base64piddata));
                            unifiedAepsRequestModel = new UnifiedAepsRequestModel("",balanaceInqueryAadharNo,
                                    bankIINNumber, etMobileNumber.getText().toString().trim(), userType,
                                    etBankspinner.getText().toString(), pidData, lastLatlong, SdkConstants.paramA,
                                    SdkConstants.paramB, SdkConstants.paramC, SdkConstants.API_USER_NAME_VALUE,
                                    session.getUserName());
                            unifiedAepsPresenter = new UnifiedAepsPresenter(UnifiedAepsActivity.this);
                            unifiedAepsPresenter.performUnifiedResponse(UnifiedAepsActivity.this,
                                    session.getUserToken(), unifiedAepsRequestModel, tvBalanceEnquiryExpandButton.
                                            getText().toString(), gatewayPriority);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.ministatement)) {
                        showLoader();
                        try {
                            JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
                            String pidData = respObj.getString(getString(R.string.base64piddata));
                            unifiedAepsRequestModel = new UnifiedAepsRequestModel("", balanaceInqueryAadharNo,
                                    bankIINNumber, etMobileNumber.getText().toString().trim(), userType,
                                    etBankspinner.getText().toString(), pidData.trim(), lastLatlong,
                                    SdkConstants.paramA, SdkConstants.paramB, SdkConstants.paramC, SdkConstants.
                                    API_USER_NAME_VALUE, session.getUserName());
                            unifiedAepsPresenter = new UnifiedAepsPresenter(UnifiedAepsActivity.this);
                            unifiedAepsPresenter.performUnifiedResponse(UnifiedAepsActivity.this,
                                    session.getUserToken(), unifiedAepsRequestModel, tvBalanceEnquiryExpandButton.
                                            getText().toString(), gatewayPriority);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        showLoader();
                        if (etAmountEnter.getText() == null || etAmountEnter.getText().toString().trim().matches("")) {
                            etAmountEnter.setError(getResources().getString(R.string.amount_error));
                            return;
                        }
                        try {
                            JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
                            String pidData = respObj.getString(getString(R.string.base64piddata));
                            unifiedAepsRequestModel = new UnifiedAepsRequestModel(etAmountEnter.getText().toString().
                                    trim(), balanaceInqueryAadharNo,
                                    bankIINNumber, etMobileNumber.getText().toString().trim(), userType,
                                    etBankspinner.getText().toString(), pidData, lastLatlong, SdkConstants.paramA,
                                    SdkConstants.paramB, SdkConstants.paramC, SdkConstants.API_USER_NAME_VALUE,
                                    session.getUserName());
                            unifiedAepsPresenter = new UnifiedAepsPresenter(UnifiedAepsActivity.this);
                            unifiedAepsPresenter.performUnifiedResponse(UnifiedAepsActivity.this, session.
                                    getUserToken(), unifiedAepsRequestModel, tvCashWithdrawalButton.getText().
                                    toString(), gatewayPriority);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(UnifiedAepsActivity.this,
                            getString(R.string.accept_enable_the_terms_conditions_for_the_further_transaction),
                            Toast.LENGTH_SHORT).show();
                }
            });
        if ((getIntent().getStringExtra(getString(R.string.failedvalue)) != null) && getIntent().
                getStringExtra(getString(R.string.failedvalue)).equalsIgnoreCase(getString(R.string.faileddata))) {
            etAadharNumber.setText(makePrettyString(SdkConstants.AADHAAR_NUMBER));
            etBankspinner.setText(SdkConstants.BANK_NAME);
            etMobileNumber.setText(SdkConstants.MOBILENUMBER);
            bankIINNumber = SdkConstants.bankIIN;
            etAadharNumber.setEnabled(false);
            etAadharNumber.setTextColor(getResources().getColor(R.color.grey));
            etMobileNumber.setEnabled(false);
            etMobileNumber.clearFocus();
            etBankspinner.setEnabled(false);
            ivFingerprint.setEnabled(true);
            ivFingerprint.setClickable(true);
            theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
            int color = typedValue.data;
            ivFingerprint.setColorFilter(color);
            SdkConstants.RECEIVE_DRIVER_DATA = "";
            depositBar.setVisibility(View.GONE);
            tvDepositNote.setVisibility(View.GONE);
            submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
            gatewayPriority = gatewayPriority + 1;
        }
        if(SdkConstants.refeshUI){
                etAadharNumber.setText(SdkConstants.aadharNumberValue);
                etBankspinner.setText(SdkConstants.bankValue);
                etMobileNumber.setText(SdkConstants.mobileNumberValue);
            SdkConstants.RECEIVE_DRIVER_DATA = getIntent().getStringExtra(getString(R.string.recieverdata));
            fingerStrength();
            bankIINNumber = SdkConstants.bankIIN;
            ivFingerprint.setColorFilter(R.color.colorGrey);
            ivFingerprint.setEnabled(false);
            flagFromDriver = true;
            SdkConstants.refeshUI = false;
        }
    }
    /**
     * Setting Ui*/
    private void initializeViews() {
        ImageView back = findViewById(R.id.ivBack);
        back.setOnClickListener(v -> onBackPressed());
        tvFingerprintStrengthDeposit = findViewById(R.id.fingerprintStrengthDeposit);
        tvDepositNote = findViewById(R.id.depositNote);
        tvDepositNote.setVisibility(View.GONE);
        tvFingerprintStrengthDeposit.setVisibility(View.GONE);
        etAadharVirtualID = (EditText) findViewById(R.id.aadharVirtualID);
        ivVirtualID = findViewById(R.id.virtualID);
        ivAadhaar = findViewById(R.id.aadhaar);
        tvVirtualidText = findViewById(R.id.virtualidText);
        tvAadharText = findViewById(R.id.aadharText);
        etAadharNumber = findViewById(R.id.aadharNumber);
        etMobileNumber = findViewById(R.id.mobileNumber);
        etBankspinner = findViewById(R.id.bankspinner);
        etAmountEnter = findViewById(R.id.amountEnter);
        ivFingerprint = findViewById(R.id.fingerprint);
        ivFingerprint.setEnabled(false);
        ivFingerprint.setClickable(false);
        submitButton = findViewById(R.id.submitButton);
        depositBar = findViewById(R.id.depositBar);
        depositBar.setVisibility(View.GONE);
        tvDepositNote.setVisibility(View.GONE);
        tvFingerprintStrengthDeposit.setVisibility(View.GONE);
        tvFingerprintStrengthDeposit.setVisibility(View.GONE);
        terms = findViewById(R.id.terms);
        tvTimer = findViewById(R.id.tv_timer);
        tvCashWithdrawalButton = findViewById(R.id.cashWithdrawalButton);
        tvBalanceEnquiryExpandButton = findViewById(R.id.balanceEnquiryExpandButton);
    }
    /**
     * Fetches data from the parent application's intent and initializes relevant constants in `SdkConstants`.
     * This method should be called in the `onCreate` method of the activity to ensure that the required data
     * is available for the activity's functionality.
     */
    private void fetchParentApplicationData() {
        if ((getIntent() != null) && getIntent().hasExtra(getString(R.string.data))) {
            dataObj = getIntent().getStringExtra(getString(R.string.data));
            try {
                JSONObject object = new JSONObject(dataObj);
                if (object.has(getString(R.string.applicationtype))) {
                    SdkConstants.applicationType = object.getString(getString(R.string.applicationtype));
                }
                if (object.has(getString(R.string.token))) {
                    SdkConstants.tokenFromCoreApp = object.getString(getString(R.string.token));
                }
                if (object.has(getString(R.string.username))) {
                    SdkConstants.userNameFromCoreApp = object.getString(getString(R.string.username));
                }
                if (object.has(getString(R.string.api_user_name_value))) {
                    SdkConstants.API_USER_NAME_VALUE = object.getString(getString(R.string.api_user_name_value));
                }
                if (object.has(getString(R.string.driver_activity))) {
                    SdkConstants.DRIVER_ACTIVITY = object.getString(getString(R.string.driver_activity));
                }
                if (object.has(getString(R.string.manufacture_flag))) {
                    SdkConstants.MANUFACTURE_FLAG = object.getString(getString(R.string.manufacture_flag));
                }
                if (object.has(getString(R.string.internalfpname))) {
                    SdkConstants.internalFPName = object.getString(getString(R.string.internalfpname));
                }
                if (object.has(getString(R.string.transactiontype))) {
                    SdkConstants.transactionType = object.getString(getString(R.string.transactiontype));
                }
                if (object.has(getString(R.string.transactionamount))) {
                    SdkConstants.transactionAmount = object.getString(getString(R.string.transactionamount));
                }
                if (object.has(getString(R.string.parama))) {
                    SdkConstants.paramA = object.getString(getString(R.string.parama));
                }
                if (object.has(getString(R.string.paramb))) {
                    SdkConstants.paramB = object.getString(getString(R.string.paramb));
                }
                if (object.has(getString(R.string.paramc))) {
                    SdkConstants.paramC = object.getString(getString(R.string.paramc));
                }
                if (object.has(getString(R.string.brand_name))) {
                    SdkConstants.BRAND_NAME = object.getString(getString(R.string.brand_name));
                }
                if (object.has(getString(R.string.shop_name))) {
                    SdkConstants.SHOP_NAME = object.getString(getString(R.string.shop_name));
                }
                if (object.has(getString(R.string.applicationusername))) {
                    SdkConstants.applicationUserName = object.getString(getString(R.string.applicationusername));
                }
                if (object.has(getString(R.string.skipreceipt))) {
                    SdkConstants.skipReceipt = Boolean.valueOf(object.getString(getString(R.string.skipreceipt)));
                }
                if (object.has(getString(R.string.refeshui))) {
                    SdkConstants.refeshUI = Boolean.parseBoolean(object.getString(getString(R.string.refeshui)));
                }
                if (object.has(getString(R.string.bioauth))) {
                    SdkConstants.bioauth = Boolean.parseBoolean(object.getString(getString(R.string.bioauth)));
                }
                session.setUserToken(SdkConstants.tokenFromCoreApp);
                session.setUsername(SdkConstants.userNameFromCoreApp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * Retrieves the RDService class based on the specified class name provided in `SdkConstants.DRIVER_ACTIVITY`.
     * Sets the flag `flagNameRdService` with the value from `SdkConstants.MANUFACTURE_FLAG`.
     * The obtained class is stored in the variable `mDriverActivity`.
     */
    private void getRDServiceClass() {
        String accessClassName = SdkConstants.DRIVER_ACTIVITY;
        flagNameRdService = SdkConstants.MANUFACTURE_FLAG;
        try {
            Class<? extends Activity> targetActivity = Class.forName(accessClassName).asSubclass(Activity.class);
            mDriverActivity = targetActivity;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutofill() {
        getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }
    /**
     * Retrieves the current location using the `GpsTracker` utility.
     * If the location is successfully obtained, the latitude and longitude are stored in the `latLong` variable.
     * If not, a settings alert is displayed.
     * The obtained location is then saved using the `saveLocation` method.
     */
    public void getLocation() {
        Double latitude;
        Double longitude;
        gpsTracker = new GpsTracker(UnifiedAepsActivity.this);
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            latLong = latitude + "," + longitude;
        } else {
            gpsTracker.showSettingsAlert();
        }
        saveLocation();
    }
    /**
     * Saves the last obtained location (`latLong`) in the shared preferences.
     * The location is stored using the key `lastLatlong` in the shared preferences file named `SHARED_PREFS`.
     */
    public void saveLocation(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(!latLong.isEmpty()){
            lastLatlong = latLong;
        }
        editor.putString(lastLatlong, latLong);
        editor.apply();
    }
    // Validates latitude and longitude values to ensure they are not equal to 0.0.
    public boolean validLatLng (double lat, double lng) {
        return lat != 0.0 && lng != 0.0;
    }
    // Implementation of the LocationListener interface to handle location updates.
    /**
     * Overrides the default behavior of the back button press.
     * Clears certain constants and hides the deposit progress bar when the back button is pressed.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SdkConstants.mobileNumberValue = "";
        SdkConstants.bankValue = "";
        SdkConstants.aadharNumberValue = "";
        SdkConstants.RECEIVE_DRIVER_DATA = "";
        SdkConstants.OnBackpressedValue = true;
        depositBar.setVisibility(View.GONE);
        this.finish();
    }
    /**
     * Handles the result of activities launched with `startActivityForResult`.
     * This method is called when the launched activity returns a result.
     * @param requestCode The request code that was used to launch the activity.
     * @param resultCode The result code returned by the activity.
     * @param data An Intent containing the result data.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SdkConstants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE) {
            hideLoader();
            if (resultCode == RESULT_OK) {
                String bankName= data.getStringExtra(SdkConstants.BANK_NAME_KEY);
                String iinNo= data.getStringExtra(SdkConstants.IIN_KEY);
                etBankspinner.setText(bankName);
                bankIINNumber = iinNo;
                SdkConstants.BANK_NAME = bankName;
                SdkConstants.bankIIN = bankIINNumber;
                checkBalanceEnquiryValidation();
            }
            checkBalanceEnquiryValidation();
        } else if (requestCode == SdkConstants.REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE) {
            hideLoader();
            if (resultCode == RESULT_OK) {
                String bankName= data.getStringExtra(SdkConstants.BANK_NAME_KEY);
                String iinNo= data.getStringExtra(SdkConstants.IIN_KEY);
                etBankspinner.setText(bankName);
                bankIINNumber = iinNo;
                SdkConstants.BANK_NAME = bankName;
                SdkConstants.bankIIN = bankIINNumber;
                checkWithdrawalValidation();
            }
            checkWithdrawalValidation();
        } else if (requestCode == SdkConstants.REQUEST_CODE) {
            hideLoader();
            if (resultCode == RESULT_OK) {
                Intent respIntent = new Intent();
                respIntent.putExtra(SdkConstants.responseData, SdkConstants.transactionResponse);
                setResult(Activity.RESULT_OK, respIntent);
                finish();
            }
            checkWithdrawalValidation();
        } else if (requestCode == 1) {
            hideLoader();
        }
    }
    /**
     * Checks the unified response status and initiates the transition to the `UnifiedAepsTransactionActivity`.
     *
     * @param status The status of the unified transaction response.
     * @param message The message associated with the response status.
     * @param miniStatementResponseModel The model containing unified transaction status details.
     */
    @Override
    public void checkUnifiedResponseStatus(String status, String message, UnifiedTxnStatusModel miniStatementResponseModel) {
        Intent intent = new Intent(UnifiedAepsActivity.this, UnifiedAepsTransactionActivity.class);
        intent.putExtra(SdkConstants.TRANSACTION_STATUS_KEY, miniStatementResponseModel);
        intent.putExtra(getString(R.string.mobile_number), etMobileNumber.getText().toString().trim());
        hideLoader();
        startActivity(intent);
        finish();
    }
    /**
     * Checks the mini-statement unified response status and initiates the transition to either
     * `UnifiedAepsMiniStatementActivity` or `UnifiedAepsTransactionActivity` based on the status.
     *
     * @param status The status of the mini-statement unified transaction response.
     * @param message The message associated with the response status.
     * @param miniStatementResponseModel The model containing unified transaction status details.
     */
    @Override
    public void checkMSunifiedStatus(String status, String message, UnifiedTxnStatusModel miniStatementResponseModel) {
        if (status.equalsIgnoreCase(getString(R.string.success_caps))) {
            Gson gson = new Gson();
            session.setFreshnessFactor("");
            Intent intent = new Intent(UnifiedAepsActivity.this, UnifiedAepsMiniStatementActivity.class);
            intent.putExtra(SdkConstants.TRANSACTION_STATUS_KEY, miniStatementResponseModel);
            intent.putExtra(getString(R.string.mobile_number), etMobileNumber.getText().toString().trim());
            hideLoader();
            startActivity(intent);
        } else {
            session.setFreshnessFactor("");
            Intent intent = new Intent(UnifiedAepsActivity.this, UnifiedAepsTransactionActivity.class);
            intent.putExtra(SdkConstants.TRANSACTION_STATUS_KEY, miniStatementResponseModel);
            intent.putExtra(getString(R.string.mobile_number), etMobileNumber.getText().toString().trim());
            hideLoader();
            startActivity(intent);
        }
        finish();
    }
    // Displays a loading indicator to indicate ongoing processing.
    @Override
    public void showLoader() {
        try {
            if (loadingView == null) {
                loadingView = new ProgressDialog(UnifiedAepsActivity.this);
                loadingView.setCancelable(false);
                loadingView.setMessage(getString(R.string.please_wait));
            }
            loadingView.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // Hides the loading indicator.
    @Override
    public void hideLoader() {
        try {
            if (loadingView != null) {
                loadingView.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Checks the validation conditions for balance enquiry and enables/disables the submit button accordingly.
     */
    private void checkBalanceEnquiryValidation() {
        if (etMobileNumber.getText() != null && !etMobileNumber.getText().toString().trim().matches("")
                && Util.isValidMobile(etMobileNumber.getText().toString().trim()) && etBankspinner.getText() != null
                && !etBankspinner.getText().toString().trim().matches("")) {
            boolean status = false;
            // Check validation based on Aadhar or Virtual ID
            if (adharbool) {
                String aadharNo = etAadharNumber.getText().toString();
                if (aadharNo.contains("-")) {
                    aadharNo = aadharNo.replaceAll("-", "").trim();
                    status = Util.validateAadharNumber(aadharNo);
                }
            } else if (virtualbool) {
                String aadharVid = etAadharVirtualID.getText().toString();
                if (aadharVid.contains("-")) {
                    aadharVid = aadharVid.replaceAll("-", "").trim();
                    status = Util.validateAadharVID(aadharVid);
                }
            }
        } else {
            // Enable/disable the submit button based on validation status
            submitButton.setEnabled(false);
            submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
        }
    }
    // Checks the validation conditions for cash withdrawal and enables/disables the submit button accordingly.
    private void checkWithdrawalValidation() {
        if (etMobileNumber.getText() != null
                && !etMobileNumber.getText().toString().trim().matches("")
                && Util.isValidMobile(etMobileNumber.getText().toString().trim())
                && etMobileNumber.getText().toString().length() == 10
                && etBankspinner.getText() != null
                && !etBankspinner.getText().toString().trim().matches("")
                && etAmountEnter.getText() != null
                && !etAmountEnter.getText().toString().trim().matches("")) {
            boolean status = false;
            // Check validation based on Aadhar or Virtual ID
            if (adharbool) {
                String aadharNo = etAadharNumber.getText().toString();
                if (aadharNo.contains("-")) {
                    aadharNo = aadharNo.replaceAll("-", "").trim();
                    status = Util.validateAadharNumber(aadharNo);
                }
            } else if (virtualbool) {
                String aadharVid = etAadharVirtualID.getText().toString();
                if (aadharVid.contains("-")) {
                    aadharVid = aadharVid.replaceAll("-", "").trim();
                    status = Util.validateAadharVID(aadharVid);
                }
            }
        } else {
            submitButton.setEnabled(false);
            submitButton.setBackground(getResources().getDrawable(R.drawable.button_submit));
        }
    }
    /**
     * Called when the activity is resumed.
     * Hides the keyboard and performs specific actions if the flag `flagFromDriver` is true.
     */
    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
        if (flagFromDriver) {
            hideLoader();
            if (SdkConstants.RECEIVE_DRIVER_DATA.isEmpty() || SdkConstants.RECEIVE_DRIVER_DATA.equalsIgnoreCase("")) {
                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                int color = typedValue.data;
                ivFingerprint.setColorFilter(color);
                ivFingerprint.setEnabled(true);
                ivFingerprint.setClickable(true);
                submitButton.setBackgroundResource(R.drawable.button_submit);
                submitButton.setEnabled(false);
            } else if (balanaceInqueryAadharNo.equalsIgnoreCase("") || balanaceInqueryAadharNo.isEmpty()) {
                etAadharNumber.setError(getString(R.string.enter_aadhar_no));
                fingerStrength();
            } else if (etMobileNumber.getText().toString().isEmpty() || etMobileNumber.getText().toString().equalsIgnoreCase("")) {
                etMobileNumber.setError(getString(R.string.enter_mobile_no));
                fingerStrength();
            } else if (etBankspinner.getText().toString().isEmpty() || etBankspinner.getText().toString().trim().equalsIgnoreCase("")) {
                etBankspinner.setError(getString(R.string.choose_your_bank));
                fingerStrength();
            } else {
                fingerStrength();
                ivFingerprint.setColorFilter(R.color.colorGrey);
                ivFingerprint.setEnabled(false);
            }
        }
    }
    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    //Handled Finger Strength
    public void fingerStrength() {
        try {
            JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
            String scoreStr = respObj.getString(getString(R.string.pidata_qscore));
            String[] scoreList = scoreStr.split(",");
            scoreStr = scoreList[0];
            if (scoreStr.equalsIgnoreCase("")) {
                hideLoader();
                showAlert(getString(R.string.invalid_fingerprint_data));
            } else {
                submitButton.setEnabled(true);
                submitButton.setBackgroundResource(R.drawable.button_submit_blue);
                if (Float.parseFloat(scoreStr) <= 40) {
                    depositBar.setVisibility(View.VISIBLE);
                    depositBar.setProgress(Float.parseFloat(scoreStr));
                    depositBar.setProgressTextMoved(true);
                    depositBar.setEndColor(getResources().getColor(R.color.red));
                    depositBar.setStartColor(getResources().getColor(R.color.red));
                    tvDepositNote.setVisibility(View.VISIBLE);
                    tvFingerprintStrengthDeposit.setVisibility(View.VISIBLE);
                }else {
                    depositBar.setVisibility(View.VISIBLE);
                    depositBar.setProgress(Float.parseFloat(scoreStr));
                    depositBar.setProgressTextMoved(true);
                    depositBar.setEndColor(getResources().getColor(R.color.green));
                    depositBar.setStartColor(getResources().getColor(R.color.green));
                    tvDepositNote.setVisibility(View.VISIBLE);
                    tvFingerprintStrengthDeposit.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            showAlert(getString(R.string.invalid_fingerprint_data));
        }
    }
    /**
     * Displays an alert dialog with the provided message.
     * @param msg The message to be displayed in the alert.
     */
    public void showAlert(String msg) {
        SingletonClass.getInstance().showAlert(msg, UnifiedAepsActivity.this);
    }
    @Override
    public void onFingerClick(String aadharNo, String mobileNumber, String bankName, String driverFlag,
                              String freashnessFactor, OnDriverDataListener listener) {
    }
    @Override
    public void onPidUpdate(String data) {
    }
    /**
     * Called when a location update is received.
     * @param latLong The updated latitude and longitude string.
     */
    @Override
    public void onLocationUpdate(String latLong) {
        lastLatlong = latLong;
        showLog(TAG, lastLatlong);
    }

    @Override
    public void onLocationTurnedOn() {
        // Not Required
    }

    public void showSettingsAlert(){
        SingletonClass.getInstance().showSettingsAlert(UnifiedAepsActivity.this);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.BANK_LIST = "";
    }
    /**
     * Starts a timer with a duration of 180 seconds (3 minutes).
     * for Cash Withdrawal transaction
     */
    private void statTimer() {
        counter = 0;
        new CountDownTimer(180000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                // Update the TextView with the remaining time
                long minutes = millisUntilFinished / 60000;
                long seconds = (millisUntilFinished % 60000) / 1000;
                tvTimer.setText(String.format("%02d:%02d", minutes, seconds));
                // Check if a minute has passed
                tvTimer.setTextColor(Color.RED);
            }
            @Override
            public void onFinish() {
                // Handle the timer finish event (if needed)
                tvTimer.setText("00:00");
                showAlertTimer(getString(R.string.timeout),UnifiedAepsActivity.this);
            }
        }.start();
    }
    /**
     * Displays an alert dialog with the provided message and handles the positive button click.
     *
     * @param msg The message to be displayed in the alert.
     * @param showAlert_context The context to show the alert dialog.
     */
    public void showAlertTimer(String msg, Context showAlert_context) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(showAlert_context);
            builder.setTitle(getString(R.string.alert));
            builder.setMessage(msg);
            builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                dialog.dismiss();
                onBackPressed();

            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}