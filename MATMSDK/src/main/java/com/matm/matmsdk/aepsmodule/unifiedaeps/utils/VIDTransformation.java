package com.matm.matmsdk.aepsmodule.unifiedaeps.utils;

import android.text.method.PasswordTransformationMethod;
import android.view.View;

import androidx.annotation.NonNull;


public  class VIDTransformation extends PasswordTransformationMethod {
    @Override
    public CharSequence getTransformation(CharSequence source, View view) {
        return new PasswordCharSequenceVID(source);
    }

    private static class PasswordCharSequenceVID implements CharSequence {
        private final CharSequence mSource;

        public PasswordCharSequenceVID(CharSequence source) {
            mSource = source;
        }

        public char charAt(int index) {
            char c = mSource.charAt(index);
            if (Character.isDigit(c) && index <= 13 && mSource.length() > 14)
                return 'X';
            else
                return mSource.charAt(index);
        }

        public int length() {
            return mSource.length();
        }

        @NonNull
        public CharSequence subSequence(int start, int end) {
            return mSource.subSequence(start, end);
        }
    }
}
