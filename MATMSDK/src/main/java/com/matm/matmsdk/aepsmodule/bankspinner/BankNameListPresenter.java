package com.matm.matmsdk.aepsmodule.bankspinner;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.utils.Constants;
import com.matm.matmsdk.aepsmodule.utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class BankNameListPresenter implements BankNameContract.UserActionsListener {
    private BankNameContract.View banknameView;

    public BankNameListPresenter(BankNameContract.View banknameView) {
        this.banknameView = banknameView;
    }

    /**
     * Getting the bank List as per the transaction Type */
    @Override
    public void loadBankNamesList(Context context) {
        // Initialize a variable to store the operation performed based on the transaction type
        String operationPerformed="";
        // Check the transaction type to determine the operation performed and set the corresponding value
        if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.balanceEnquiry)) {
            operationPerformed="uaeps_be";
        } else if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.ministatement)) {
            operationPerformed="uaeps_ms";
        } else if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.cashWithdrawal)) {
            operationPerformed="uaeps_cw";
        } else if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.cashDeposit)) {
            operationPerformed="uaeps_cd";
        } else if (SdkConstants.transactionType.equalsIgnoreCase(SdkConstants.adhaarPay)) {
            operationPerformed="aadhaarpay";
        }
        BankRequest bankRequest= new BankRequest();
        bankRequest.setOperationPerformed(operationPerformed);
        banknameView.showLoader();
        if (Constants.BANK_LIST.equalsIgnoreCase("")) {
            AndroidNetworking.post("https://centralize-banklist-v2.iserveu.tech/aeps-iin-bank/display-bank-details")
                    .setPriority(Priority.HIGH)
                    .addBodyParameter(bankRequest)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Constants.BANK_LIST = response.toString();
                                Gson gson = new Gson();
                                BankResponse bankResponse = gson.fromJson(response.toString(), BankResponse.class);
                                banknameView.hideLoader();
                                banknameView.bankNameListReady(bankResponse.getBankList());
                                banknameView.showBankNames();
                            } catch (Exception e) {
                                e.printStackTrace();
                                banknameView.hideLoader();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            banknameView.hideLoader();
                        }
                    });
        } else {
            try {
                Gson gson = new Gson();
                BankResponse bankResponse = gson.fromJson(Constants.BANK_LIST, BankResponse.class);
                banknameView.hideLoader();
                banknameView.bankNameListReady(bankResponse.getBankList());
                banknameView.showBankNames();
            } catch (Exception e) {
                e.printStackTrace();
                banknameView.hideLoader();
            }
        }
    }

}

