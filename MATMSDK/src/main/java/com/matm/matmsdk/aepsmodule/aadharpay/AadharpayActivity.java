package com.matm.matmsdk.aepsmodule.aadharpay;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.UnifiedAepsTransactionActivity.TAG;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.getCursorPos;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.makePrettyString;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.showLog;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.matm.matmsdk.CustomThemes;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.aadharpay.model.AadharpayRequest;
import com.matm.matmsdk.aepsmodule.aadharpay.model.AadharpayResponse;
import com.matm.matmsdk.aepsmodule.alertShow.SingletonClass;
import com.matm.matmsdk.aepsmodule.bankspinner.BankNameListActivity;
import com.matm.matmsdk.aepsmodule.unifiedaeps.AepsBaseActivity;
import com.matm.matmsdk.aepsmodule.unifiedaeps.utils.AadhaarTransformation;
import com.matm.matmsdk.aepsmodule.utils.Constants;
import com.matm.matmsdk.aepsmodule.utils.Session;
import com.matm.matmsdk.aepsmodule.utils.Util;
import com.matm.matmsdk.callbacks.OnDriverDataListener;
import com.matm.matmsdk.notification.NotificationHelper;
import com.moos.library.HorizontalProgressView;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Objects;
import isumatm.androidsdk.equitas.R;
public class AadharpayActivity extends AepsBaseActivity implements OnDriverDataListener,
        AepsBaseActivity.LocationUpdateListener {
    private EditText etAadharNumber;
    private EditText etMobileNumber;
    private EditText etBankSpinner;
    private EditText etEnteredAmount;
    private EditText etVirtualID;
    private ImageView ivAadhaar;
    private ImageView ivVirtualID;
    private ImageView ivFingerprint_BIG;
    private TextView tvUnderAdhaarIV;
    private TextView tvUnderVartualID;
    private TextView depositNote;
    private TextView fingerprintStrengthDeposit;
    private Button btnsubmit;
    private String aadharNumberMain = "";
    private String bankName_ = "";
    private String bankIINNumber = "";
    private ProgressDialog loadingView;
    private AppCompatCheckBox terms;
    private String fmDeviceId = "Startek Eng-Inc.";
    private String fmDeviceId2 = "Startek Eng-Inc.\u0000";
    private String fmDeviceId3 = "Startek Eng. Inc.";
    private String fmDeviceId4 = "Startek";
    private Boolean flagFromDriver = false;
    private Boolean adharbool = true;
    private Boolean virtualbool = false;
    private boolean mWannaDeleteHyphen = false;
    private boolean mKeyListenerSet = false;
    private boolean mInside = false;
    private TypedValue typedValue;
    private Resources.Theme theme;
    private Class driverActivity;
    private String flagNameRdService = "";
    private Session session;
    private AadharpayRequest aadharpayRequest;
    private AadharpayResponse aadharpayResponse = new AadharpayResponse();
    private NotificationHelper notificationHelper;
    private HorizontalProgressView depositBar;
    private String mLastLatlong;
    SharedPreferences sp;
    public static final String MATM_PREF = "matmPref";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);// to apply custom themes
        setContentView(R.layout.activity_aeps3_home);
        loadingView = new ProgressDialog(AadharpayActivity.this);
        etAadharNumber = findViewById(R.id.et_aadharNumber);
        etMobileNumber = findViewById(R.id.et_mobileNumber);
        etBankSpinner = findViewById(R.id.et_bankSpinner);
        etEnteredAmount = findViewById(R.id.et_enteredAmount);
        ivAadhaar = findViewById(R.id.iv_aadhaar);
        ivVirtualID = findViewById(R.id.iv_virtualID);
        ivFingerprint_BIG = findViewById(R.id.iv_fingerprint);
        btnsubmit = findViewById(R.id.btn_submitButton);
        etVirtualID = findViewById(R.id.et_aadharVirtualID);
        tvUnderAdhaarIV = findViewById(R.id.tv_aadharText);
        tvUnderVartualID = findViewById(R.id.tv_virtualidText);
        depositBar = findViewById(R.id.depositBar);
        depositBar.setVisibility(View.GONE);
        depositNote = findViewById(R.id.depositNote);
        fingerprintStrengthDeposit = findViewById(R.id.tv_fingerprintStrengthDeposit);
        terms = findViewById(R.id.terms);
        ivFingerprint_BIG.setEnabled(false);
        typedValue = new TypedValue();
        theme = this.getTheme();
        session = new Session(AadharpayActivity.this);
        notificationHelper = new NotificationHelper(this);
        setLocationUpdateListener(this);
        checkLocationPermission();
        /** to enable the amount field, SdkConstants.editable = true.
         *  by default value is false
         *  **/
        etEnteredAmount.setEnabled(SdkConstants.editable);
        etEnteredAmount.setText(SdkConstants.transactionAmount);
        sp = getSharedPreferences(MATM_PREF, Context.MODE_PRIVATE);
        /**onFailed scenario data should not be reset*/
        if (getIntent().getStringExtra(getString(R.string.transactionstatus_)) != null &&
                getIntent().getStringExtra(getString(R.string.transactionstatus_)).equals(getString(R.string.failed))) {
            etAadharNumber.setText(makePrettyString(SdkConstants.AADHAAR_NUMBER));
            etBankSpinner.setText(SdkConstants.BANK_NAME);
            etMobileNumber.setText(SdkConstants.MOBILENUMBER);
            bankIINNumber = SdkConstants.IIN_NUMBER;
            bankName_ = SdkConstants.BANK_NAME;
            aadharNumberMain = SdkConstants.AADHAAR_NUMBER;
            ivFingerprint_BIG.setEnabled(true);
            theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
            int color = typedValue.data;
            ivFingerprint_BIG.setColorFilter(color);
        }
        if (SdkConstants.refeshUI) {
            etAadharNumber.setText(SdkConstants.aadharNumberValue);
            etBankSpinner.setText(SdkConstants.bankValue);
            etMobileNumber.setText(SdkConstants.mobileNumberValue);
            SdkConstants.RECEIVE_DRIVER_DATA = getIntent().getStringExtra(getString(R.string.recieverdata));
            fingerStrength();
            bankIINNumber = SdkConstants.bankIIN;
            ivFingerprint_BIG.setColorFilter(R.color.colorGrey);
            ivFingerprint_BIG.setEnabled(false);
            flagFromDriver = true;
            SdkConstants.refeshUI = false;
        }
        getRDServiceClass();// to get the driver Activity from SDK Constants
        getPublicIp();// To get the public IP
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        /**  SdkConstants.isSL is required to call the api so if the sdk is used by our core app then it should be false,
         * or else if the sdk is used by SDK client then it should be true*/
        if (SdkConstants.applicationType.equalsIgnoreCase("CORE")) {
            session.setUserToken(SdkConstants.tokenFromCoreApp);
            session.setUsername(SdkConstants.userNameFromCoreApp);
            SdkConstants.isSL = false; //As this is a double layer application
        } else {
            SdkConstants.isSL = true;//As this is a single layer application
            getUserAuthToken(); // To generate token for sdk users
        }
        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ivVirtualID.setBackgroundResource(R.drawable.ic_language);
        tvUnderVartualID.setTextColor(getResources().getColor(R.color.grey));
        ivAadhaar.setBackgroundResource(R.drawable.ic_fingerprint_blue);
        /**EditText aadhaarNumber validation**/
        etAadharNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!mKeyListenerSet) {
                    etAadharNumber.setOnKeyListener((v, keyCode, event) -> {
                        try {
                            mWannaDeleteHyphen = (keyCode == KeyEvent.KEYCODE_DEL
                                    && etAadharNumber.getSelectionEnd() - etAadharNumber.getSelectionStart() <= 1
                                    && etAadharNumber.getSelectionStart() > 0
                                    && etAadharNumber.getText().toString().charAt(etAadharNumber.getSelectionEnd() - 1) == '-');
                        } catch (IndexOutOfBoundsException e) {
                            // never to happen because of checks
                        }
                        return false;
                    });
                    mKeyListenerSet = true;
                }
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mInside) // to avoid recursive calls
                    return;
                mInside = true;
                int currentPos = etAadharNumber.getSelectionStart();
                String string = etAadharNumber.getText().toString().toUpperCase();
                String newString = makePrettyString(string);
                if (count == 14) {
                    ivFingerprint_BIG.setEnabled(true);
                    theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                    int color = typedValue.data;
                    ivFingerprint_BIG.setColorFilter(color);
                }
                etAadharNumber.setText(newString);
                try {
                    etAadharNumber.setSelection(getCursorPos(string, newString, currentPos, mWannaDeleteHyphen));
                } catch (IndexOutOfBoundsException e) {
                    etAadharNumber.setSelection(etAadharNumber.length());
                }
                mWannaDeleteHyphen = false;
                mInside = false;
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    etAadharNumber.setError(null);
                    String aadharNo = etAadharNumber.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                        aadharNumberMain = aadharNo;
                        if (aadharNumberMain.length() >= 12) {
                            if (!Util.validateAadharNumber(aadharNumberMain)) {
                                etAadharNumber.setError(getResources().getString(R.string.Validaadhaarerror));
                            }else{
                                ivFingerprint_BIG.setEnabled(true);
                                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                                int color = typedValue.data;
                                ivFingerprint_BIG.setColorFilter(color);
                                etAadharNumber.clearFocus();
                                etMobileNumber.requestFocus();
                            }
                        }
                    }
                }
            }
        });
        etAadharNumber.setTransformationMethod(new AadhaarTransformation());
        /**EditText MobileNumber validation*/
        etMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                String number = "";
                if (s.length() > 0 && s.length() < 9) {
                    etMobileNumber.setError(null);
                    number = s.toString();
                    if (number.startsWith("0")) {
                        etMobileNumber.setError(getResources().getString(R.string.mobilevaliderror));
                    }
                }
                if (s.length() == 10) {
                    if (!Util.isValidMobile(etMobileNumber.getText().toString().trim())) {
                        if (number.startsWith("0")) {
                            etMobileNumber.setError(getResources().getString(R.string.mobilevaliderror));
                        } else {
                            etMobileNumber.setError(getResources().getString(R.string.mobilevaliderror));
                        }
                    }else {
                        etMobileNumber.clearFocus();
                    }
                }
            }
        });
        /**BankSPinner apicall*/
        etBankSpinner.setOnClickListener(v -> {
            etMobileNumber.clearFocus();
            Intent in = new Intent(AadharpayActivity.this, BankNameListActivity.class);
            startActivityForResult(in, SdkConstants.REQUEST_FOR_ACTIVITY_AADHAARPAY_CODE);
        });
        /**EditText amount validation*/
        etEnteredAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    etEnteredAmount.setError(getResources().getString(R.string.amount_error));
                }
                if (s.length() > 0) {
                    etEnteredAmount.setError(null);
                }
            }
        });
/** This event is a default.
 * when someone initiate the transaction via AadhaarNumber */
        ivAadhaar.setOnClickListener(v -> {
            etAadharNumber.setVisibility(View.VISIBLE);
            etVirtualID.setVisibility(View.GONE);
            ivVirtualID.setEnabled(true);
            ivAadhaar.setEnabled(false);
            ivVirtualID.setBackgroundResource(R.drawable.ic_language);
            tvUnderVartualID.setTextColor(getResources().getColor(R.color.grey));
            adharbool = true;
            virtualbool = false;
            ivAadhaar.setBackgroundResource(R.drawable.ic_fingerprint_blue);
            theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
            int color = typedValue.data;
            tvUnderAdhaarIV.setTextColor(color);
        });
        /** This event is for if someone wants to do transaction via VID instate of AadhaarNumber*/
        ivVirtualID.setOnClickListener(v -> {
            etAadharNumber.setVisibility(View.GONE);
            etVirtualID.setVisibility(View.VISIBLE);
            ivVirtualID.setEnabled(false);
            ivAadhaar.setEnabled(true);
            virtualbool = true;
            adharbool = false;
            ivVirtualID.setBackgroundResource(R.drawable.ic_language_blue);
            theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
            int color = typedValue.data;
            tvUnderVartualID.setTextColor(color);
            ivAadhaar.setBackground(getResources().getDrawable(R.drawable.ic_fingerprint_grey));
            tvUnderAdhaarIV.setTextColor(getResources().getColor(R.color.grey));
        });
/** e This event is for capturing the user fingerprint */
        ivFingerprint_BIG.setOnClickListener(v -> {
            try {
                showLoader();
                hideKeyboard();
                ivFingerprint_BIG.setEnabled(false);
                ivFingerprint_BIG.setColorFilter(R.color.colorGrey);
                flagFromDriver = true;
                if (SdkConstants.MANUFACTURE_FLAG.equalsIgnoreCase(fmDeviceId) ||
                        SdkConstants.MANUFACTURE_FLAG.contains(fmDeviceId4) ||
                        SdkConstants.MANUFACTURE_FLAG.equalsIgnoreCase(fmDeviceId2) ||
                        SdkConstants.MANUFACTURE_FLAG.equalsIgnoreCase(fmDeviceId3)) {
                    SdkConstants.aadharNumberValue = etAadharNumber.getText().toString();
                    SdkConstants.mobileNumberValue = etMobileNumber.getText().toString();
                    SdkConstants.bankValue = etBankSpinner.getText().toString();
                    bankIINNumber = SdkConstants.bankIIN;
                    SdkConstants.bankIIN = bankIINNumber;
                    if (etMobileNumber.getText().toString().isEmpty() || etBankSpinner.getText().toString().isEmpty()) {
                        hideLoader();
                        Toast.makeText(AadharpayActivity.this, getString(R.string.fill_up_details), Toast.LENGTH_SHORT).show();
                    } else {
                        ivFingerprint_BIG.setEnabled(false);
                        etAadharNumber.setEnabled(false);
                        etAadharNumber.setTextColor(getResources().getColor(R.color.grey));
                        etMobileNumber.setEnabled(false);
                        etMobileNumber.clearFocus();
                        etBankSpinner.setEnabled(false);
                        SdkConstants.OnBackpressedValue = false;
                        ivFingerprint_BIG.setColorFilter(R.color.colorGrey);
                        if (SdkConstants.onDriverDataListener != null) {
                            SdkConstants.onDriverDataListener.onFingerClick(aadharNumberMain,
                                    etMobileNumber.getText().toString(),
                                    etBankSpinner.getText().toString(),
                                    flagNameRdService
                                    , session.getFreshnessFactor(), AadharpayActivity.this);
                            hideLoader();
                            finish();
                        }
                    }
                } else {
                    Intent launchIntent = new Intent(AadharpayActivity.this, driverActivity);
                    launchIntent.putExtra(getString(R.string.driverflag), flagNameRdService);
                    launchIntent.putExtra(getString(R.string.freshnesfactor), session.getFreshnessFactor());
                    launchIntent.putExtra(getString(R.string.aadharno), aadharNumberMain);
                    startActivity(launchIntent);
                }
            } catch (Exception e) {
                hideLoader();
                e.printStackTrace();
            }
        });
        /** there is a terms and condition check in sdk (for now this is hidden) */
        terms.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SdkConstants.firstCheck = true;
            showTermsDetails(AadharpayActivity.this);
        });
        /** After clicking the submit button the api call and the transacion initiated here. with all the input validaions */
        btnsubmit.setOnClickListener(v -> {
            String AadhaarNumber = aadharNumberMain;
            String _mobileNumber = "";
            if (adharbool) {
                if (AadhaarNumber != null || !AadhaarNumber.matches("")) {
                    if (AadhaarNumber.length() == 12) {
                        if (Util.validateAadharNumber(AadhaarNumber)) {
                            if (!etMobileNumber.getText().toString().isEmpty() &&
                                    !etMobileNumber.getText().toString().trim().matches("") &&
                                    Util.isValidMobile(etMobileNumber.getText().toString().trim())) {
                                _mobileNumber = etMobileNumber.getText().toString().trim();
                                if (!etBankSpinner.getText().toString().isEmpty() &&
                                        !etBankSpinner.getText().toString().trim().matches("")) {
                                    JSONObject respObj = null;
                                    if (flagFromDriver) {
                                       try {
                                            showLoader();
                                            respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
                                            String aadhaarsha = Util.getSha256Hash(AadhaarNumber);
                                            String base64pidData = respObj.getString(getString(R.string.base64piddata));
                                            aadharpayRequest = new AadharpayRequest(AadhaarNumber, bankIINNumber,
                                                    getString(R.string.android), mLastLatlong, getPublicIp(),
                                                    etEnteredAmount.getText().toString().trim(),
                                                    _mobileNumber, SdkConstants.loginID, SdkConstants.paramA,
                                                    SdkConstants.paramB, SdkConstants.paramC, SdkConstants.isSL,
                                                    bankName_.trim(), base64pidData, aadhaarsha);
                                            SdkConstants.AADHAAR_NUMBER = AadhaarNumber;
                                            SdkConstants.IIN_NUMBER = bankIINNumber;
                                            SdkConstants.MOBILENUMBER = etMobileNumber.getText().toString().trim();
                                            SdkConstants.BANK_NAME = etBankSpinner.getText().toString();
                                           CallAadhaarPayApi();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Toast.makeText(AadharpayActivity.this,
                                                getString(R.string.please_do_biometric_verification), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    etBankSpinner.setError(getResources().getString(R.string.select_bank_error));
                                }
                            } else {
                                etMobileNumber.setError(getResources().getString(R.string.mobileerror));
                            }
                        } else {
                            etAadharNumber.setError(getResources().getString(R.string.valid_aadhar_error));
                        }
                    } else {
                        etAadharNumber.setError(getResources().getString(R.string.valid_aadhar_error));
                    }
                } else {
                    etAadharNumber.setError(getResources().getString(R.string.valid_aadhar_error));
                }
            } else if (virtualbool) {
                String vartual_Id = etVirtualID.getText().toString();
                if (vartual_Id.contains("-")) {
                    vartual_Id = vartual_Id.replaceAll("-", "").trim();
                }
                if (vartual_Id == null || vartual_Id.matches("")) {
                    etVirtualID.setError(getResources().getString(R.string.valid_vid_error));
                    return;
                }
                if (!Util.validateAadharNumber(vartual_Id)) {
                    etVirtualID.setError(getResources().getString(R.string.valid_aadhar_error));
                }
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
        /**This flag will be true when the finger print got successfully captured from driver activity.*/
        if (flagFromDriver) {
            hideLoader();
            if (SdkConstants.RECEIVE_DRIVER_DATA.isEmpty()) {
                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                int color = typedValue.data;
                ivFingerprint_BIG.setColorFilter(color);
                ivFingerprint_BIG.setEnabled(true);
                btnsubmit.setBackgroundResource(R.drawable.button_submit);
                btnsubmit.setEnabled(false);
            } else if (aadharNumberMain.equalsIgnoreCase("") || aadharNumberMain.isEmpty()) {
                etAadharNumber.setError(getString(R.string.enter_aadhar_no));
                fingerStrength();
            } else if (etMobileNumber.getText().toString().isEmpty() ||
                    etMobileNumber.getText().toString().equalsIgnoreCase("")) {
                etMobileNumber.setError(getString(R.string.enter_mobile_no));
                fingerStrength();
            } else if (etBankSpinner.getText().toString().isEmpty() ||
                    etBankSpinner.getText().toString().trim().equalsIgnoreCase("")) {
                etBankSpinner.setError(getString(R.string.choose_your_bank));
                fingerStrength();
            } else {
                fingerStrength();
                ivFingerprint_BIG.setColorFilter(R.color.colorGrey);
                ivFingerprint_BIG.setEnabled(false);
            }
        }
    }
    /**
     * This method will invoked after successfully capturing the
     * finGerprint data to show the fingerprint strength on finger deposit bAR
     */
    public void fingerStrength() {
        try {
            JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
            String scoreStr = respObj.getString(getString(R.string.pidata_qscore));

            if (scoreStr.contains(",")) {
                showAlert(getString(R.string.invalid_fingerprint_data));
            } else {
                btnsubmit.setEnabled(true);
                btnsubmit.setBackgroundResource(R.drawable.button_submit_blue);
                if (Float.parseFloat(scoreStr) <= 30) {
                    depositBar.setVisibility(View.VISIBLE);
                    depositBar.setProgress(Float.parseFloat(scoreStr));
                    depositBar.setProgressTextMoved(true);
                    depositBar.setEndColor(getResources().getColor(R.color.red));
                    depositBar.setStartColor(getResources().getColor(R.color.red));
                    depositNote.setVisibility(View.VISIBLE);
                    fingerprintStrengthDeposit.setVisibility(View.VISIBLE);
                } else if (Float.parseFloat(scoreStr) >= 30 && Float.parseFloat(scoreStr) <= 60) {
                    depositBar.setVisibility(View.VISIBLE);
                    depositBar.setProgress(Float.parseFloat(scoreStr));
                    depositBar.setProgressTextMoved(true);
                    depositBar.setEndColor(getResources().getColor(R.color.yellow));
                    depositBar.setStartColor(getResources().getColor(R.color.yellow));
                    depositNote.setVisibility(View.VISIBLE);
                    fingerprintStrengthDeposit.setVisibility(View.VISIBLE);
                } else {
                    depositBar.setVisibility(View.VISIBLE);
                    depositBar.setProgress(Float.parseFloat(scoreStr));
                    depositBar.setProgressTextMoved(true);
                    depositBar.setEndColor(getResources().getColor(R.color.green));
                    depositBar.setStartColor(getResources().getColor(R.color.green));
                    depositNote.setVisibility(View.VISIBLE);
                    fingerprintStrengthDeposit.setVisibility(View.VISIBLE);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            showAlert(getString(R.string.invalid_fingerprint_data));
        }
    }
    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    private void getUserAuthToken() {
        String url = SdkConstants.BASE_URL + "/api/getAuthenticateData";
        JSONObject obj = new JSONObject();
        try {
            obj.put(getString(R.string.encrypteddata), SdkConstants.encryptedData);
            obj.put(getString(R.string.retailerusername), SdkConstants.loginID);
            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString(getString(R.string.status_));

                                if (status.equalsIgnoreCase(getString(R.string.success_small))) {
                                    String userName = obj.getString(getString(R.string.username_small));
                                    String userToken = obj.getString(getString(R.string.usertoken));
                                    session.setUsername(userName);
                                    session.setUserToken(userToken);
                                    hideLoader();
                                } else {
                                    showAlert(status);
                                    hideLoader();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                showAlert(getString(R.string.invalid_encrypted_data));
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            showAlert(anError.getErrorDetail());
                        }
                    });
        } catch (Exception e) {
            hideLoader();
            e.printStackTrace();
        }
    }
    private void getRDServiceClass() {
        String accessClassName = SdkConstants.DRIVER_ACTIVITY;
        flagNameRdService = SdkConstants.MANUFACTURE_FLAG;
        try {
            driverActivity = Class.forName(accessClassName).asSubclass(Activity.class);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    private String getPublicIp() {
        String publicIP_ = "";
        try {
            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            publicIP_ = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
            if (publicIP_.equalsIgnoreCase("0.0.0.0")) {
                publicIP_ = getLocalIpAddress();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return publicIP_;
    }
    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return Formatter.formatIpAddress(inetAddress.hashCode());
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    /**
     * This function is for Aadhaarpay api call and initiate the transactions
     */
    private void CallAadhaarPayApi() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonString = mapper.writeValueAsString(aadharpayRequest); // to convert the Model class to JSONObject
            JSONObject jsonObject = new JSONObject(jsonString);// to convert the Model class to JSONObject
            System.out.println(jsonString);
            System.out.println(session.getUserToken());
            AndroidNetworking.post(Constants.LIVE_URL + getString(R.string.aadhaarpay))
                    .addHeaders(getString(R.string.authorization), session.getUserToken())
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoader();
                            try {
                                aadharpayResponse = new AadharpayResponse(
                                        response.getString(getString(R.string.transactionstatus_small)),
                                        response.getInt(getString(R.string.status_)),
                                        response.getString(getString(R.string.txid)),
                                        response.getString(getString(R.string.errors)),
                                        etEnteredAmount.getText().toString().trim(),
                                        bankName_.trim(), etMobileNumber.getText().toString().trim());
                                Gson gson = new Gson();
                                String myJson = gson.toJson(aadharpayResponse); // Converting Model class to JSON to intent to receipt activity
                                Intent intent = new Intent(AadharpayActivity.this, AadhaarpayReceiptActivity.class);
                                intent.putExtra(getString(R.string.myjson), myJson);// Intending the Aadhaarpay response to receipt Activity
                                startActivity(intent);
                                finish();
                            } catch (JSONException e) {
                                showAlert(getString(R.string.something_went_wrong_please_try_again));
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            if (anError.getErrorCode() == 400) {
                                try {
                                    JSONObject jsonObject1 = new JSONObject(anError.getErrorBody());
                                    String transactionStatus = jsonObject1.getString(getString(R.string.transactionstatus_));
                                    int status = jsonObject1.getInt(getString(R.string.status_));
                                    String txId = jsonObject1.getString(getString(R.string.txid));
                                    String errors = jsonObject1.getString(getString(R.string.errors));
                                    aadharpayResponse = new AadharpayResponse(transactionStatus, status,
                                            txId, errors, etEnteredAmount.getText().toString().trim(),
                                            bankName_.trim(), etMobileNumber.getText().toString().trim());
                                    Gson gson = new Gson();
                                    String myJson = gson.toJson(aadharpayResponse); // Converting Model class to JSON to intent to receipt activity
                                    Intent intent2 = new Intent(AadharpayActivity.this,
                                            AadhaarpayReceiptActivity.class);
                                    intent2.putExtra(getString(R.string.myjson), myJson);// Intending the Aadhaarpay response to receipt Activity
                                    startActivity(intent2);
                                    finish();
                                } catch (JSONException e) {
                                    showAlert(getString(R.string.something_went_wrong_please_try_again));
                                    e.printStackTrace();
                                }
                            } else {
                                showAlert(getString(R.string.something_went_wrong_please_try_again));
                            }
                        }
                    });
        } catch (JSONException | JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    public void showLoader() {
        loadingView.setMessage(getString(R.string.please_wait));
        loadingView.setCancelable(false);
        loadingView.show();
    }
    public void hideLoader() {
        try {
            if (!isFinishing() && loadingView != null) {
                loadingView.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void showAlert(String msg) {
        SingletonClass.getInstance().showAlert(msg, AadharpayActivity.this);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SdkConstants.REQUEST_FOR_ACTIVITY_AADHAARPAY_CODE) {
            hideLoader();
            if (resultCode == RESULT_OK) {
                String bankName = data.getStringExtra(SdkConstants.BANK_NAME_KEY);
                String iinNo = data.getStringExtra(SdkConstants.IIN_KEY);
                etBankSpinner.setText(bankName);
                bankIINNumber = iinNo;
                bankName_ = bankName;
                checkAadhaarPayValidation();
            }
            checkAadhaarPayValidation();
        }
        else if (requestCode == 1) {
            hideLoader();
        }
    }
    private void checkAadhaarPayValidation() {
        if (etMobileNumber.getText() != null
                && !etMobileNumber.getText().toString().trim().matches("")
                && Util.isValidMobile(etMobileNumber.getText().toString().trim())
                && etMobileNumber.getText().toString().length() == 10
                && etBankSpinner.getText() != null
                && !etBankSpinner.getText().toString().trim().matches("")
                && etEnteredAmount.getText() != null
                && !etEnteredAmount.getText().toString().trim().matches("")) {
            boolean status = false;
            if (!adharbool && virtualbool) {
                String aadharVid = etVirtualID.getText().toString();
                if (aadharVid.contains("-")) {
                    aadharVid = aadharVid.replaceAll("-", "").trim();
                    status = Util.validateAadharVID(aadharVid);
                }
            }
        } else {
            btnsubmit.setEnabled(false);
            btnsubmit.setBackground(getResources().getDrawable(R.drawable.button_submit));
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SdkConstants.mobileNumberValue = "";
        SdkConstants.bankValue = "";
        SdkConstants.aadharNumberValue = "";
        SdkConstants.RECEIVE_DRIVER_DATA = "";
        SdkConstants.OnBackpressedValue = true;
        depositBar.setVisibility(View.GONE);
        depositNote.setVisibility(View.GONE);
        this.finish();
    }
    /**
     * Term and conditions  by @Subhashree
     */
    public void showTermsDetails(Activity activity) {
        try {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.activity_terms_conditions);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            TextView firstText = dialog.findViewById(R.id.firststText);
            TextView secondText = dialog.findViewById(R.id.secondstText);
            SwitchCompat switchCompat = dialog.findViewById(R.id.swOnOff);
            switchCompat.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    firstText.setText(getResources().getString(R.string.hinditm1));
                    secondText.setText(getResources().getString(R.string.hinditm2));
                } else {
                    firstText.setText(getResources().getString(R.string.term1));
                    secondText.setText(getResources().getString(R.string.term2));
                }
            });
            Button dialogBtnClose = dialog.findViewById(R.id.close_Btn);
            dialogBtnClose.setOnClickListener(v -> dialog.cancel());
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(AadharpayActivity.this, getString(R.string.error) + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onFingerClick(String aadharNo, String mobileNumber, String bankName, String driverFlag, String freashnessFactor, OnDriverDataListener listener) {
    }
    @Override
    public void onPidUpdate(String pidData) {
    }
    @Override
    public void onLocationUpdate(String latLong) {
        mLastLatlong = latLong;
        showLog(TAG, mLastLatlong);
    }

    public void showSettingsAlert(){
        SingletonClass.getInstance().showSettingsAlert(AadharpayActivity.this);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.BANK_LIST = "";
    }

    @Override
    public void onLocationTurnedOn() {
        // Not Required
    }
}
