package com.matm.matmsdk.aepsmodule.unifiedaeps.utils;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.matm.matmsdk.Utils.SdkConstants;

public class Utils {
    private Utils() {
    }
    public static String makePrettyString(String string) {
        String number = string.replace("-", "");
        boolean isEndHyphen = string.endsWith("-") && (number.length() % 4 == 0);
        return number.replaceAll("(.{4}(?!$))", "$1-") + (isEndHyphen ? "-" : "");
    }
    final static String MARKER = "|"; // filtered in layout not to be in the string
    public static int getCursorPos(String oldString, String newString, int oldPos, boolean isDeleteHyphen) {
        int cursorPos = newString.length();
        if (oldPos != oldString.length()) {
            String stringWithMarker = oldString.substring(0, oldPos) + MARKER + oldString.substring(oldPos);

            cursorPos = (makePrettyString(stringWithMarker)).indexOf(MARKER);
            if (isDeleteHyphen)
                cursorPos -= 1;
        }
        return cursorPos;
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showLog(String tag, String message) {
        boolean isShowLog= SdkConstants.isShowLog();
        if (isShowLog) {
            Log.d(tag, message);
        }
    }


}
