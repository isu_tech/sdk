package com.matm.matmsdk.aepsmodule.unifiedaeps;
import android.content.Context;
public class UnifiedAepsContract {
    public interface View {
        void checkUnifiedResponseStatus(String status, String message, UnifiedTxnStatusModel miniStatementResponseModel);
        void checkMSunifiedStatus(String status,String message,UnifiedTxnStatusModel miniStatementResponseModel);
        void showLoader();
        void hideLoader();
    }
    interface UserActionsListener {
        void performUnifiedResponse(Context context, String token, UnifiedAepsRequestModel unifiedAepsRequestModel,
                                    String transaction_type,int gatewayPriority);
    }
}

