package com.matm.matmsdk.aepsmodule.unifiedaeps;

import static com.matm.matmsdk.aepsmodule.unifiedaeps.UnifiedAepsTransactionActivity.TAG;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.getCursorPos;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.makePrettyString;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.showLog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.unifiedaeps.utils.AadhaarTransformation;
import com.matm.matmsdk.aepsmodule.unifiedaeps.utils.VIDTransformation;
import com.matm.matmsdk.aepsmodule.utils.Session;
import com.matm.matmsdk.aepsmodule.utils.Util;
import com.moos.library.HorizontalProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import isumatm.androidsdk.equitas.R;

/**
 * The UnifiedBioAuthActivity class represents an activity for bio-authentication in the AEPS system.
 * It extends AepsBaseActivity and implements the LocationUpdateListener interface.
 */
public class UnifiedBioAuthActivity extends AepsBaseActivity implements AepsBaseActivity.LocationUpdateListener {
    private ImageView ivTwo_fact_fingerprint;
    private ImageView ivNew_two_fact_fingerprint;
    private HorizontalProgressView depositBar;
    private HorizontalProgressView new_depositBar;
    private Button two_fact_submitButton;
    private Button new_two_fact_submitButton;
    private ProgressDialog loadingView;
    private Session session;
    private String lat_long;
    private CheckBox textUserTitle;
    private CheckBox newtextUserTitle;
    private TypedValue typedValue;
    private Resources.Theme theme;
    private TextView tvUserName;
    private EditText etBalanceAadharNumber;
    private EditText etBalanceAadharVID;
    private EditText etConfirmbalanceAadharNumber;
    private EditText etNewbalanceAadharNumber;
    private LinearLayout bio_ll;
    private LinearLayout bio_new;
    private String flagNameRdService = "";
    private Class<?> mDriverActivity;
    private String balanaceInqueryAadharNo = "";
    private String confirmAadharNumber = "";
    private Boolean flagFromDriver = false;
    private boolean mInside = false;
    private boolean mWannaDeleteHyphen = false;
    private boolean mKeyListenerSet = false;
    private String dataObj;
    private boolean mIsBioAuthApiCalled = false;

    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState If the activity is being re-initialized after previously being shut down,
     *                           this Bundle contains the data it most recently supplied in onSaveInstanceState.
     */
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_auth);
        typedValue = new TypedValue(); // Initialize typedValue and theme
        theme = this.getTheme();
        session = new Session(UnifiedBioAuthActivity.this); // Initialize session and views
        initializeViews(); // Initialize UI elements
        setLocationUpdateListener(this); // Set the location update listener
        checkLocationPermission(); // Check location permission
        fetchParentApplicationData(); // Fetch parent application data
        getRDServiceClass();  // Get the RD service class
        etBalanceAadharNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!mKeyListenerSet) {
                    etBalanceAadharNumber.setOnKeyListener((v, keyCode, event) -> {
                        try {
                            mWannaDeleteHyphen = (keyCode == KeyEvent.KEYCODE_DEL
                                    && etBalanceAadharNumber.getSelectionEnd() - etBalanceAadharNumber.getSelectionStart() <= 1
                                    && etBalanceAadharNumber.getSelectionStart() > 0
                                    && etBalanceAadharNumber.getText().toString().charAt(etBalanceAadharNumber.getSelectionEnd() - 1) == '-');
                        } catch (IndexOutOfBoundsException e) {
                            // never to happen because of checks
                        }
                        return false;
                    });
                    mKeyListenerSet = true;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mInside) // to avoid recursive calls
                    return;
                mInside = true;
                int currentPos = etBalanceAadharNumber.getSelectionStart();
                String string = etBalanceAadharNumber.getText().toString().toUpperCase();
                String newString = makePrettyString(string);
                if (count == 14 && Util.validateAadharNumber(confirmAadharNumber)) {
                    ivTwo_fact_fingerprint.setEnabled(true);
                    ivTwo_fact_fingerprint.setClickable(true);
                    theme.resolveAttribute(androidx.appcompat.R.attr.colorPrimary, typedValue, true);
                    int color = typedValue.data;
                    ivTwo_fact_fingerprint.setColorFilter(color);
                } else {
                    ivTwo_fact_fingerprint.setEnabled(false);
                    ivTwo_fact_fingerprint.setColorFilter(null);
                }
                etBalanceAadharNumber.setText(newString);
                try {
                    etBalanceAadharNumber.setSelection(getCursorPos(string, newString, currentPos, mWannaDeleteHyphen));
                } catch (IndexOutOfBoundsException e) {
                    etBalanceAadharNumber.setSelection(etBalanceAadharNumber.length()); // last resort never to happen
                }
                mWannaDeleteHyphen = false;
                mInside = false;
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    etBalanceAadharNumber.setError(null);
                    String aadharNo = etBalanceAadharNumber.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                        balanaceInqueryAadharNo = aadharNo;
                        if (balanaceInqueryAadharNo.length() >= 12) {
                            if (!Util.validateAadharNumber(aadharNo)) {
                                etBalanceAadharNumber.setError(getResources().getString(R.string.Validaadhaarerror));
                            } else if (Util.validateAadharNumber(aadharNo) && Util.validateAadharNumber(confirmAadharNumber)) {
                                ivTwo_fact_fingerprint.setEnabled(true);
                                ivTwo_fact_fingerprint.setClickable(true);
                                etBalanceAadharNumber.clearFocus();
                                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                                int color = typedValue.data;
                                ivTwo_fact_fingerprint.setColorFilter(color);
                            }
                        }
                    }
                }
            }
        });
        etConfirmbalanceAadharNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!mKeyListenerSet) {
                    etConfirmbalanceAadharNumber.setOnKeyListener((v, keyCode, event) -> {
                        try {
                            mWannaDeleteHyphen = (keyCode == KeyEvent.KEYCODE_DEL
                                    && etConfirmbalanceAadharNumber.getSelectionEnd() - etConfirmbalanceAadharNumber.getSelectionStart() <= 1
                                    && etConfirmbalanceAadharNumber.getSelectionStart() > 0
                                    && etConfirmbalanceAadharNumber.getText().toString().charAt(etConfirmbalanceAadharNumber.getSelectionEnd() - 1) == '-');
                        } catch (IndexOutOfBoundsException e) {
                            // never to happen because of checks
                        }
                        return false;
                    });
                    mKeyListenerSet = true;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mInside) // to avoid recursive calls
                    return;
                mInside = true;
                int currentPos = etConfirmbalanceAadharNumber.getSelectionStart();
                String string = etConfirmbalanceAadharNumber.getText().toString().toUpperCase();
                String newString = makePrettyString(string);
                if (count == 14 && Util.validateAadharNumber(balanaceInqueryAadharNo)) {
                    ivTwo_fact_fingerprint.setEnabled(true);
                    ivTwo_fact_fingerprint.setClickable(true);
                } else {
                    ivTwo_fact_fingerprint.setEnabled(false);
                    ivTwo_fact_fingerprint.setColorFilter(null);
                }
                etConfirmbalanceAadharNumber.setText(newString);
                try {
                    etConfirmbalanceAadharNumber.setSelection(getCursorPos(string, newString, currentPos,
                            mWannaDeleteHyphen));
                } catch (IndexOutOfBoundsException e) {
                    etConfirmbalanceAadharNumber.setSelection(etConfirmbalanceAadharNumber.length());
                    // last resort never to happen
                }
                mWannaDeleteHyphen = false;
                mInside = false;
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    etConfirmbalanceAadharNumber.setError(null);
                    String aadharNo = etConfirmbalanceAadharNumber.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                        confirmAadharNumber = aadharNo;
                        if (confirmAadharNumber.length() >= 12) {
                            if (!Util.validateAadharNumber(aadharNo)) {
                                etConfirmbalanceAadharNumber.setError(getResources().getString(R.string.Validaadhaarerror));
                            } else if (Util.validateAadharNumber(aadharNo) && Util.validateAadharNumber(balanaceInqueryAadharNo)) {
                                ivTwo_fact_fingerprint.setEnabled(true);
                                ivTwo_fact_fingerprint.setClickable(true);
                                etBalanceAadharNumber.clearFocus();
                                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                                int color = typedValue.data;
                                ivTwo_fact_fingerprint.setColorFilter(color);
                                etConfirmbalanceAadharNumber.clearFocus();
                            }
                        }
                    }
                }
            }
        });
        etBalanceAadharNumber.setTransformationMethod(new AadhaarTransformation());
        etConfirmbalanceAadharNumber.setTransformationMethod(new AadhaarTransformation());
        etBalanceAadharVID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!mKeyListenerSet) {
                    etBalanceAadharVID.setOnKeyListener((v, keyCode, event) -> {
                        try {
                            mWannaDeleteHyphen = (keyCode == KeyEvent.KEYCODE_DEL
                                    && etBalanceAadharVID.getSelectionEnd() - etBalanceAadharVID.getSelectionStart() <= 1
                                    && etBalanceAadharVID.getSelectionStart() > 0
                                    && etBalanceAadharVID.getText().toString().charAt(etBalanceAadharVID.getSelectionEnd() - 1) == '-');
                        } catch (IndexOutOfBoundsException e) {
                            // never to happen because of checks
                        }
                        return false;
                    });
                    mKeyListenerSet = true;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mInside) // to avoid recursive calls
                    return;
                mInside = true;
                int currentPos = etBalanceAadharVID.getSelectionStart();
                String string = etBalanceAadharVID.getText().toString().toUpperCase();
                String newString = makePrettyString(string);
                if (count == 19) {
                    ivTwo_fact_fingerprint.setEnabled(true);
                    ivTwo_fact_fingerprint.setClickable(true);
                }
                etBalanceAadharVID.setText(newString);
                try {
                    etBalanceAadharVID.setSelection(getCursorPos(string, newString, currentPos, mWannaDeleteHyphen));
                } catch (IndexOutOfBoundsException e) {
                    etBalanceAadharVID.setSelection(etBalanceAadharVID.length()); // last resort never to happen
                }
                mWannaDeleteHyphen = false;
                mInside = false;
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    etBalanceAadharVID.setError(null);
                    String aadharNo = etBalanceAadharVID.getText().toString();
                    if (aadharNo.contains("-")) {
                        aadharNo = aadharNo.replaceAll("-", "").trim();
                        balanaceInqueryAadharNo = aadharNo;
                        if (balanaceInqueryAadharNo.length() >= 16) {
                            if (!Util.validateAadharVID(aadharNo)) {
                                etBalanceAadharVID.setError(getResources().getString(R.string.valid_aadhar__uid_error));
                            } else {
                                ivTwo_fact_fingerprint.setEnabled(true);
                                ivTwo_fact_fingerprint.setClickable(true);
                                etBalanceAadharVID.clearFocus();
                            }
                        }
                    }
                }
            }
        });
        etBalanceAadharVID.setTransformationMethod(new VIDTransformation());
        ivTwo_fact_fingerprint.setOnClickListener(v -> {
            if (balanaceInqueryAadharNo.length() > 0 && Util.validateAadharNumber(balanaceInqueryAadharNo) && (confirmAadharNumber.length() > 0 && Util.validateAadharNumber(confirmAadharNumber))) {
                if (balanaceInqueryAadharNo.equalsIgnoreCase(confirmAadharNumber)) {
                    showLoader();
                    flagFromDriver = true;
                    ivTwo_fact_fingerprint.setEnabled(false);
                    ivTwo_fact_fingerprint.setColorFilter(null);
                    Intent launchIntent = new Intent(UnifiedBioAuthActivity.this, mDriverActivity);
                    launchIntent.putExtra(getString(R.string.driverflag), flagNameRdService);
                    launchIntent.putExtra(getString(R.string.freshnesfactor), session.getFreshnessFactor());
                    launchIntent.putExtra(getString(R.string.aadharno), balanaceInqueryAadharNo);
                    startActivity(launchIntent);
                } else {
                    Toast.makeText(UnifiedBioAuthActivity.this, getString(R.string.aadhar_number_didn_t_match),
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(UnifiedBioAuthActivity.this, R.string.Validaadhaarerror, Toast.LENGTH_SHORT).show();
            }
        });
        ivNew_two_fact_fingerprint.setOnClickListener(v -> {
            if (etNewbalanceAadharNumber.length() > 0) {
                showLoader();
                flagFromDriver = true;
                ivNew_two_fact_fingerprint.setEnabled(false);
                ivNew_two_fact_fingerprint.setColorFilter(null);
                Intent launchIntent = new Intent(UnifiedBioAuthActivity.this, mDriverActivity);
                launchIntent.putExtra(getString(R.string.driverflag), flagNameRdService);
                launchIntent.putExtra(getString(R.string.freshnesfactor), session.getFreshnessFactor());
                launchIntent.putExtra(getString(R.string.aadharno), balanaceInqueryAadharNo);
                startActivity(launchIntent);
            } else {
                Toast.makeText(UnifiedBioAuthActivity.this, R.string.Validaadhaarerror, Toast.LENGTH_SHORT).show();
            }
        });
        two_fact_submitButton.setOnClickListener(v -> {
            if (etBalanceAadharNumber.getText().toString().equals("")) {
                Toast.makeText(UnifiedBioAuthActivity.this, R.string.aadhaarnumber, Toast.LENGTH_SHORT).show();
            } else {
                if (!Util.validateAadharNumber(balanaceInqueryAadharNo)) {
                    Toast.makeText(UnifiedBioAuthActivity.this, R.string.Validaadhaarerror,
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (!flagFromDriver) {
                        Toast.makeText(UnifiedBioAuthActivity.this,
                                getString(R.string.please_do_biometric_verification),
                                Toast.LENGTH_LONG).show();
                    } else {
                        if (!textUserTitle.isChecked()) {
                            Toast.makeText(UnifiedBioAuthActivity.this,
                                    getString(R.string.please_accept_the_aadhaar_consent), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        try {
                            JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
                            String scoreStr = respObj.getString(getString(R.string.pidata_qscore));
                            String[] scoreList = scoreStr.split(",");
                            scoreStr = scoreList[0];
                            String pidData = respObj.getString(getString(R.string.base64piddata));
                            if (Float.parseFloat(scoreStr) <= 40) {
                                Toast.makeText(UnifiedBioAuthActivity.this,
                                        getString(R.string.bad_fingerprint_strength_please_try_again),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                try {
                                    JSONObject obj = new JSONObject();
                                    obj.put(getString(R.string.latlong), lat_long);
                                    obj.put(getString(R.string.aadharno), balanaceInqueryAadharNo);
                                    obj.put(getString(R.string.piddata), pidData.replace("\n", ""));
                                    submitBioAuthh(obj);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        new_two_fact_submitButton.setOnClickListener(v -> {
            if (etNewbalanceAadharNumber.getText().toString().equals("")) {
                Toast.makeText(UnifiedBioAuthActivity.this, R.string.aadhaarnumber, Toast.LENGTH_SHORT).show();
            } else {
                if (!flagFromDriver) {
                    Toast.makeText(UnifiedBioAuthActivity.this,
                            getString(R.string.please_do_biometric_verification), Toast.LENGTH_LONG).show();
                } else {
                    if (!newtextUserTitle.isChecked()) {
                        Toast.makeText(UnifiedBioAuthActivity.this,
                                getString(R.string.please_accept_the_aadhaar_consent), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    try {
                        JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
                        String scoreStr = respObj.getString(getString(R.string.pidata_qscore));
                        String[] scoreList = scoreStr.split(",");
                        scoreStr = scoreList[0];
                        String pidData = respObj.getString(getString(R.string.base64piddata));
                        if (Float.parseFloat(scoreStr) <= 40) {
                            Toast.makeText(UnifiedBioAuthActivity.this,
                                    getString(R.string.bad_fingerprint_strength_please_try_again),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                JSONObject obj = new JSONObject();
                                obj.put(getString(R.string.latlong), lat_long);
                                obj.put(getString(R.string.aadharno), "");
                                obj.put(getString(R.string.piddata), pidData.replace("\n", ""));
                                submitBioAuthh(obj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onLocationTurnedOn() {
        if (!mIsBioAuthApiCalled) {
            mIsBioAuthApiCalled = true;
            checkBioAuth();
        }
    }

    private void fetchParentApplicationData() {
        if ((getIntent() != null) && getIntent().hasExtra(getString(R.string.data))) {
            dataObj = getIntent().getStringExtra(getString(R.string.data));
            try {
                JSONObject object = new JSONObject(dataObj);
                if (object.has(getString(R.string.applicationtype))) {
                    SdkConstants.applicationType = object.getString(getString(R.string.applicationtype));
                }
                if (object.has(getString(R.string.token))) {
                    SdkConstants.tokenFromCoreApp = object.getString(getString(R.string.token));
                }
                if (object.has(getString(R.string.username))) {
                    SdkConstants.userNameFromCoreApp = object.getString(getString(R.string.username));
                }
                if (object.has(getString(R.string.api_user_name_value))) {
                    SdkConstants.API_USER_NAME_VALUE = object.getString(getString(R.string.api_user_name_value));
                }
                if (object.has(getString(R.string.merchant_id))) {
                    SdkConstants.MERCHANT_ID = object.getString(getString(R.string.merchant_id));
                }
                if (object.has(getString(R.string.driver_activity))) {
                    SdkConstants.DRIVER_ACTIVITY = object.getString(getString(R.string.driver_activity));
                }
                if (object.has(getString(R.string.manufacture_flag))) {
                    SdkConstants.MANUFACTURE_FLAG = object.getString(getString(R.string.manufacture_flag));
                }
                if (object.has(getString(R.string.internalfpname))) {
                    SdkConstants.internalFPName = object.getString(getString(R.string.internalfpname));
                }
                if (object.has(getString(R.string.transactiontype))) {
                    SdkConstants.transactionType = object.getString(getString(R.string.transactiontype));
                }
                if (object.has(getString(R.string.transactionamount))) {
                    SdkConstants.transactionAmount = object.getString(getString(R.string.transactionamount));
                }
                if (object.has(getString(R.string.parama))) {
                    SdkConstants.paramA = object.getString(getString(R.string.parama));
                }
                if (object.has(getString(R.string.paramb))) {
                    SdkConstants.paramB = object.getString(getString(R.string.paramb));
                }
                if (object.has(getString(R.string.paramc))) {
                    SdkConstants.paramC = object.getString(getString(R.string.paramc));
                }
                if (object.has(getString(R.string.brand_name))) {
                    SdkConstants.BRAND_NAME = object.getString(getString(R.string.brand_name));
                }
                if (object.has(getString(R.string.shop_name))) {
                    SdkConstants.SHOP_NAME = object.getString(getString(R.string.shop_name));
                }
                if (object.has(getString(R.string.applicationusername))) {
                    SdkConstants.applicationUserName = object.getString(getString(R.string.applicationusername));
                }
                if (object.has(getString(R.string.skipreceipt))) {
                    SdkConstants.skipReceipt = Boolean.valueOf(object.getString(getString(R.string.skipreceipt)));
                }
                if (object.has(getString(R.string.refeshui))) {
                    SdkConstants.refeshUI = Boolean.valueOf(object.getString(getString(R.string.refeshui)));
                }
                if (object.has(getString(R.string.bioauth))) {
                    SdkConstants.bioauth = Boolean.valueOf(object.getString(getString(R.string.bioauth)));
                }
                session.setUserToken(SdkConstants.tokenFromCoreApp);
                session.setUsername(SdkConstants.userNameFromCoreApp);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void initializeViews() {
        bio_ll = findViewById(R.id.bio_ll);
        bio_new = findViewById(R.id.bio_new);
        ivTwo_fact_fingerprint = findViewById(R.id.two_fact_fingerprint);
        ivNew_two_fact_fingerprint = findViewById(R.id.new_two_fact_fingerprint);
        ivTwo_fact_fingerprint.setEnabled(true);
        ivNew_two_fact_fingerprint.setEnabled(true);
        depositBar = findViewById(R.id.depositBar);
        new_depositBar = findViewById(R.id.new_depositBar);
        depositBar.setVisibility(View.GONE);
        new_depositBar.setVisibility(View.GONE);
        textUserTitle = findViewById(R.id.textUserTitle);
        newtextUserTitle = findViewById(R.id.newtextUserTitle);
        two_fact_submitButton = findViewById(R.id.two_fact_submitButton);
        new_two_fact_submitButton = findViewById(R.id.new_two_fact_submitButton);
        etBalanceAadharNumber = findViewById(R.id.balanceAadharNumber);
        etNewbalanceAadharNumber = findViewById(R.id.newbalanceAadharNumber);
        etConfirmbalanceAadharNumber = findViewById(R.id.confirmAadharNumber);
        etBalanceAadharVID = findViewById(R.id.balanceAadharVID);
        tvUserName = findViewById(R.id.userName);
        tvUserName.setText(SdkConstants.applicationUserName);
        ivTwo_fact_fingerprint.setEnabled(false);
        ivTwo_fact_fingerprint.setClickable(false);
    }

    private void getRDServiceClass() {
        String accessClassName = SdkConstants.DRIVER_ACTIVITY;
        flagNameRdService = SdkConstants.MANUFACTURE_FLAG;
        try {
            Class<? extends Activity> targetActivity = Class.forName(accessClassName).asSubclass(Activity.class);
            mDriverActivity = targetActivity;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void submitBioAuthh(JSONObject obj1) {
        showLoader();
        AndroidNetworking.post("https://unifiedaepsbeta.iserveu.tech/bioAuth")
                .setPriority(Priority.HIGH)
                .addHeaders(getString(R.string.authorization), SdkConstants.tokenFromCoreApp)
                .addJSONObjectBody(obj1)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString(getString(R.string.status_));
                            String message = obj.getString(getString(R.string.statusdesc));
                            if (status.equalsIgnoreCase("1")) {
                                Toast.makeText(UnifiedBioAuthActivity.this, message, Toast.LENGTH_SHORT).show();
                                sendAEPS2Intent(true);
                            } else {
                                if (message.equalsIgnoreCase(getString(R.string.biometric_data_did_not_match))) {
                                    retryBioAuthAlert(message);
                                } else {
                                    showAlert(message);
                                }
                            }
                        } catch (Exception e) {
                            showAlert(getString(R.string.something_went_wrong_please_try_again));
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                        showAlert(getString(R.string.something_went_wrong_please_try_again));
                    }
                });
    }

    private void checkBioAuth() {
        showLoader();
        AndroidNetworking.get("https://unifiedaepsbeta.iserveu.tech/checkBioAuth")
                .addHeaders("Authorization", SdkConstants.tokenFromCoreApp)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String statusDesc = obj.getString(getString(R.string.statusdesc));
                            String isUidPresent = obj.optString(getString(R.string.isuidpresent));
                            String uidRefId = obj.optString(getString(R.string.uidrefid));
                            if (statusDesc.equalsIgnoreCase(getString(R.string.true_)) &&
                                    isUidPresent.equalsIgnoreCase(getString(R.string.true_))) {
                                sendAEPS2Intent(true);
                            } else {
                                if (statusDesc.equalsIgnoreCase(getString(R.string.false_)) &&
                                        isUidPresent.equalsIgnoreCase(getString(R.string.true_))) {
                                    bio_new.setVisibility(View.VISIBLE);
                                    theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                                    int color = typedValue.data;
                                    ivNew_two_fact_fingerprint.setColorFilter(color);
                                    ivNew_two_fact_fingerprint.setEnabled(true);
                                    ivNew_two_fact_fingerprint.setClickable(true);
                                    newtextUserTitle.setText(getString(R.string.hi_) + SdkConstants.userNameFromCoreApp +
                                            getString(R.string.complete_verification_with_your_registered_aadhar_number) + uidRefId);
                                    etNewbalanceAadharNumber.setText(uidRefId);
                                    etNewbalanceAadharNumber.setFocusable(false);
                                    etNewbalanceAadharNumber.setEnabled(false);
                                } else {
                                    textUserTitle.setText("I, " + SdkConstants.MERCHANT_ID +
                                            getString(R.string.understand_and_agree_that_the_above_aadhaar_is_registered_with_merchant_id) +
                                            SdkConstants.userNameFromCoreApp +
                                            getString(R.string.and_shall_be_used_for_biometric_authentication_everyday_before_processing_aadhaar_based_transactions_and_that_it_is_not_subject_to_cancellation_or_amendments));
                                    bio_ll.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showAlert(getString(R.string.something_went_wrong_please_try_again));
                        } catch (Exception e) {
                            showAlert(getString(R.string.something_went_wrong_please_try_again));
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                        showAlert(getString(R.string.something_went_wrong_please_try_again));
                    }
                });
    }

    public void showLoader() {
        if (loadingView == null) {
            loadingView = new ProgressDialog(UnifiedBioAuthActivity.this);
            loadingView.setCancelable(false);
            loadingView.setMessage(getString(R.string.please_wait));
        }
        loadingView.show();
    }

    public void hideLoader() {
        try {
            if (loadingView != null) {
                loadingView.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            hideLoader();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
        if (flagFromDriver) {
            hideLoader();
            if (SdkConstants.RECEIVE_DRIVER_DATA.isEmpty() || SdkConstants.RECEIVE_DRIVER_DATA.equalsIgnoreCase("")) {
                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                int color = typedValue.data;
                ivTwo_fact_fingerprint.setColorFilter(color);
                ivTwo_fact_fingerprint.setEnabled(true);
                ivTwo_fact_fingerprint.setClickable(true);
                ivNew_two_fact_fingerprint.setColorFilter(color);
                ivNew_two_fact_fingerprint.setEnabled(true);
                ivNew_two_fact_fingerprint.setClickable(true);
            } else if (balanaceInqueryAadharNo.equalsIgnoreCase("") || balanaceInqueryAadharNo.isEmpty()) {
                etBalanceAadharNumber.setError(getString(R.string.enter_aadhar_no));
                ivTwo_fact_fingerprint.setColorFilter(R.color.colorGrey);
                ivTwo_fact_fingerprint.setEnabled(false);
                fingerStrength();
            } else {
                fingerStrength();
                ivTwo_fact_fingerprint.setColorFilter(null);
                ivTwo_fact_fingerprint.setEnabled(false);
                ivNew_two_fact_fingerprint.setColorFilter(null);
                ivNew_two_fact_fingerprint.setEnabled(false);
                two_fact_submitButton.setEnabled(true);
            }
        }
    }

    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void sendAEPS2Intent(boolean bioauth) {
        SdkConstants.bioauth = bioauth;
        Intent intent = new Intent(UnifiedBioAuthActivity.this, UnifiedAepsActivity.class);
        intent.putExtra(getString(R.string.data), dataObj);
        startActivity(intent);
        finish();
    }

    public void showAlert(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(UnifiedBioAuthActivity.this);
            builder.setTitle(getString(R.string.alert));
            builder.setMessage(msg);
            builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                dialog.dismiss();
                finish();
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void retryBioAuthAlert(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(UnifiedBioAuthActivity.this);
            builder.setTitle(getString(R.string.alert));
            builder.setMessage(msg);
            builder.setPositiveButton(R.string.retry, (dialog, which) -> {
                dialog.dismiss();
                depositBar.setVisibility(View.GONE);
                flagFromDriver = false;
                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                int color = typedValue.data;
                ivTwo_fact_fingerprint.setColorFilter(color);
                ivTwo_fact_fingerprint.setEnabled(true);
                ivTwo_fact_fingerprint.setClickable(true);
                new_depositBar.setVisibility(View.GONE);
                ivNew_two_fact_fingerprint.setColorFilter(color);
                ivNew_two_fact_fingerprint.setEnabled(true);
                ivNew_two_fact_fingerprint.setClickable(true);
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fingerStrength() {
        try {
            JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
            String scoreStr = respObj.getString(getString(R.string.pidata_qscore));
            String[] scoreList = scoreStr.split(",");
            scoreStr = scoreList[0];
            String deviceType = "";
            if (respObj.has(getString(R.string.rdsid))) {
                deviceType = respObj.getString(getString(R.string.rdsid));
            }
            if (deviceType.contains(getString(R.string.scpl))) {
                depositBar.setVisibility(View.VISIBLE);
                depositBar.setProgress(Float.parseFloat(scoreStr));
                depositBar.setProgressTextMoved(true);
                depositBar.setStartColor(getResources().getColor(R.color.green));
                depositBar.setEndColor(getResources().getColor(R.color.green));
                /*new */
                new_depositBar.setVisibility(View.VISIBLE);
                new_depositBar.setProgress(Float.parseFloat(scoreStr));
                new_depositBar.setProgressTextMoved(true);
                new_depositBar.setStartColor(getResources().getColor(R.color.green));
                new_depositBar.setEndColor(getResources().getColor(R.color.green));
            } else {
                if (Float.parseFloat(scoreStr) <= 40) {
                    depositBar.setVisibility(View.VISIBLE);
                    depositBar.setProgress(Float.parseFloat(scoreStr));
                    depositBar.setProgressTextMoved(true);
                    depositBar.setEndColor(getResources().getColor(R.color.red));
                    depositBar.setStartColor(getResources().getColor(R.color.red));
                    /*new */
                    new_depositBar.setVisibility(View.VISIBLE);
                    new_depositBar.setProgress(Float.parseFloat(scoreStr));
                    new_depositBar.setProgressTextMoved(true);
                    new_depositBar.setEndColor(getResources().getColor(R.color.red));
                    new_depositBar.setStartColor(getResources().getColor(R.color.red));
                } else {
                    depositBar.setVisibility(View.VISIBLE);
                    depositBar.setProgress(Float.parseFloat(scoreStr));
                    depositBar.setProgressTextMoved(true);
                    depositBar.setEndColor(getResources().getColor(R.color.green));
                    depositBar.setStartColor(getResources().getColor(R.color.green));
                    /*new*/
                    new_depositBar.setVisibility(View.VISIBLE);
                    new_depositBar.setProgress(Float.parseFloat(scoreStr));
                    new_depositBar.setProgressTextMoved(true);
                    new_depositBar.setEndColor(getResources().getColor(R.color.green));
                    new_depositBar.setStartColor(getResources().getColor(R.color.green));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationUpdate(String latLong) {
        lat_long = latLong;
        showLog(TAG, lat_long);
    }

}
