package com.matm.matmsdk.aepsmodule.unifiedaeps;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.UnifiedAepsTransactionActivity.TAG;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.showLog;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.Utils.SdkConstants;
import com.moos.library.HorizontalProgressView;
import org.json.JSONException;
import org.json.JSONObject;
import isumatm.androidsdk.equitas.R;
public class CWBioauthActivity extends AepsBaseActivity implements AepsBaseActivity.LocationUpdateListener {
    private TypedValue typedValue;
    private Resources.Theme theme;
    private String dataObj;
    private EditText aadharNumber;
    private CheckBox newtextUserTitle;
    private ImageView new_two_fact_fingerprint;
    private HorizontalProgressView new_depositBar;
    private Button new_two_fact_submitButton;
    private ProgressDialog loadingView;
    private Boolean flagFromDriver = false;
    private String lat_long;
    private String flagNameRdService = "";
    private Class driverActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cwbioauth);
        typedValue = new TypedValue();
        theme = this.getTheme();
        checkBioAuth();
        initializeViews();
        setLocationUpdateListener(this);
        checkLocationPermission();
        getRDServiceClass();
        newtextUserTitle.setText(getString(R.string.hi_) + SdkConstants.userNameFromCoreApp +
                getString(R.string.complete_verification_with_your_registered_aadhar_number) + SdkConstants.aadharNumFromBioauth);
        new_two_fact_fingerprint.setOnClickListener(v -> {
            if (aadharNumber.length() > 0) {
                showLoader();
                flagFromDriver = true;
                new_two_fact_fingerprint.setEnabled(false);
                new_two_fact_fingerprint.setColorFilter(null);
                Intent launchIntent = new Intent(CWBioauthActivity.this, driverActivity);
                launchIntent.putExtra(getString(R.string.driverflag), flagNameRdService);
                launchIntent.putExtra(getString(R.string.aadharno), aadharNumber.getText().toString());
                startActivityForResult(launchIntent, 1);
            } else {
                Toast.makeText(CWBioauthActivity.this, R.string.Validaadhaarerror, Toast.LENGTH_SHORT).show();
            }
        });
        new_two_fact_submitButton.setOnClickListener(v -> {
            if (aadharNumber.getText().toString().equals("")) {
                Toast.makeText(CWBioauthActivity.this, R.string.aadhaarnumber, Toast.LENGTH_SHORT).show();
            } else {
                if (!flagFromDriver) {
                    Toast.makeText(CWBioauthActivity.this, getString(R.string.please_do_biometric_verification), Toast.LENGTH_LONG).show();
                    return;
                } else {
                    if (!newtextUserTitle.isChecked()) {
                        Toast.makeText(CWBioauthActivity.this, getString(R.string.please_accept_the_aadhaar_consent), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    try {
                        JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
                        String scoreStr = respObj.getString(getString(R.string.pidata_qscore));
                        String[] scoreList = scoreStr.split(",");
                        scoreStr = scoreList[0];
                        String pidData = respObj.getString(getString(R.string.base64piddata));
                        if (Float.parseFloat(scoreStr) <= 40) {
                            Toast.makeText(CWBioauthActivity.this, getString(R.string.bad_fingerprint_strength_please_try_again),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                JSONObject obj = new JSONObject();
                                obj.put(getString(R.string.latlong), lat_long);
                                obj.put(getString(R.string.aadharno), "");
                                obj.put(getString(R.string.piddata), pidData.replace("\n", ""));
                                submitBioAuthh(obj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    private void initializeViews() {
        new_two_fact_fingerprint = findViewById(R.id.new_two_fact_fingerprint);
        new_two_fact_fingerprint.setEnabled(true);
        new_depositBar = findViewById(R.id.new_depositBar);
        new_depositBar.setVisibility(View.GONE);
        newtextUserTitle = findViewById(R.id.newtextUserTitle);
        new_two_fact_submitButton = findViewById(R.id.new_two_fact_submitButton);
        aadharNumber = findViewById(R.id.aadharNumber);
        aadharNumber.setText(SdkConstants.aadharNumFromBioauth);
    }
    private void getRDServiceClass() {
        String accessClassName = SdkConstants.DRIVER_ACTIVITY;
        flagNameRdService = SdkConstants.MANUFACTURE_FLAG;
        try {
            Class<? extends Activity> targetActivity = Class.forName(accessClassName).asSubclass(Activity.class);
            driverActivity = targetActivity;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
        if (flagFromDriver) {
            hideLoader();
            if (SdkConstants.RECEIVE_DRIVER_DATA.isEmpty() || SdkConstants.RECEIVE_DRIVER_DATA.equalsIgnoreCase("")) {
                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                int color = typedValue.data;
                new_two_fact_fingerprint.setColorFilter(color);
                new_two_fact_fingerprint.setEnabled(true);
                new_two_fact_fingerprint.setClickable(true);
            } else if (SdkConstants.aadharNumFromBioauth.equalsIgnoreCase("") ||
                    SdkConstants.aadharNumFromBioauth.isEmpty()) {
                aadharNumber.setError(getString(R.string.enter_aadhar_no));
                new_two_fact_fingerprint.setColorFilter(R.color.colorGrey);
                fingerStrength();
            } else {
                fingerStrength();
                new_two_fact_fingerprint.setColorFilter(null);
                new_two_fact_fingerprint.setEnabled(false);
            }
        }
    }
    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            hideLoader();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    private void checkBioAuth() {
        try {
            AndroidNetworking.get("https://unifiedaepsbeta.iserveu.tech/checkBioAuth")
                    .addHeaders(getString(R.string.authorization), SdkConstants.tokenFromCoreApp)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String statusDesc = obj.getString(getString(R.string.statusdesc));
                                String isUidPresent = obj.optString(getString(R.string.isuidpresent));
                                String uidRefId = obj.optString(getString(R.string.uidrefid));
                                SdkConstants.aadharNumFromBioauth = uidRefId;
                                aadharNumber.setText(uidRefId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                showAlert(e.getMessage().toString());
                            } catch (Exception e) {
                                showAlert(e.getMessage().toString());
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            showAlert(getString(R.string.something_went_wrong_please_try_again));
                        }
                    });
        } catch (Exception e) {
            showError(e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    private void submitBioAuthh(JSONObject obj1) {
        try {
            showLoader();
            AndroidNetworking.post("https://unifiedaepsbeta.iserveu.tech/bioAuth")
                    .setPriority(Priority.HIGH)
                    .addHeaders(getString(R.string.authorization), SdkConstants.tokenFromCoreApp)
                    .addJSONObjectBody(obj1)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoader();
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString(getString(R.string.status_));
                                String message = obj.getString(getString(R.string.statusdesc));
                                if (status.equalsIgnoreCase("1")) {
                                    Toast.makeText(CWBioauthActivity.this, message, Toast.LENGTH_SHORT).show();
                                    sendAEPS2Intent(true);
                                } else {
                                    if (message.equalsIgnoreCase(getString(R.string.biometric_data_did_not_match))) {
                                        retryBioAuthAlert(message);
                                    } else {
                                        showAlert(message);
                                    }
                                }
                            } catch (JSONException e) {
                                showAlert(e.getMessage().toString());
                                e.printStackTrace();
                            } catch (Exception e) {
                                showAlert(e.getMessage().toString());
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            showAlert(getString(R.string.something_went_wrong_please_try_again));
                        }
                    });
        } catch (Exception e) {
            showError(e.getMessage());
        }
    }
    public void sendAEPS2Intent(boolean bioauth) {
        SdkConstants.bioauth = bioauth;
        Intent intent = new Intent(CWBioauthActivity.this, UnifiedAepsActivity.class);
        intent.putExtra(getString(R.string.data), dataObj);
        startActivityForResult(intent, SdkConstants.REQUEST_CODE);
        finish();
    }
    public void showError(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(CWBioauthActivity.this);
            builder.setTitle(getString(R.string.error));
            builder.setMessage(msg);
            builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                dialog.dismiss();
                finish();
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void showAlert(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(CWBioauthActivity.this);
            builder.setTitle(getString(R.string.alert));
            builder.setMessage(msg);
            builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                dialog.dismiss();
                finish();
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void retryBioAuthAlert(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(CWBioauthActivity.this);
            builder.setTitle(getString(R.string.alert));
            builder.setMessage(msg);
            builder.setPositiveButton(getString(R.string.retry), (dialog, which) -> {
                dialog.dismiss();
                new_depositBar.setVisibility(View.GONE);
                flagFromDriver = false;
                theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
                int color = typedValue.data;
                new_depositBar.setVisibility(View.GONE);
                new_two_fact_fingerprint.setColorFilter(color);
                new_two_fact_fingerprint.setEnabled(true);
                new_two_fact_fingerprint.setClickable(true);
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void fingerStrength() {
        try {
            JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
            String scoreStr = respObj.getString(getString(R.string.pidata_qscore));
            String[] scoreList = scoreStr.split(",");
            scoreStr = scoreList[0];
            String deviceType = "";
            if (respObj.has(getString(R.string.rdsid))) {
                deviceType = respObj.getString(getString(R.string.rdsid));
            }
            if (deviceType.contains(getString(R.string.scpl))) {
                new_depositBar.setVisibility(View.VISIBLE);
                new_depositBar.setProgress(Float.parseFloat(scoreStr));
                new_depositBar.setProgressTextMoved(true);
                new_depositBar.setStartColor(getResources().getColor(R.color.green));
                new_depositBar.setEndColor(getResources().getColor(R.color.green));
            } else {
                if (Float.parseFloat(scoreStr) <= 40) {
                    new_depositBar.setVisibility(View.VISIBLE);
                    new_depositBar.setProgress(Float.parseFloat(scoreStr));
                    new_depositBar.setProgressTextMoved(true);
                    new_depositBar.setEndColor(getResources().getColor(R.color.red));
                    new_depositBar.setStartColor(getResources().getColor(R.color.red));
                } else {
                    new_depositBar.setVisibility(View.VISIBLE);
                    new_depositBar.setProgress(Float.parseFloat(scoreStr));
                    new_depositBar.setProgressTextMoved(true);
                    new_depositBar.setEndColor(getResources().getColor(R.color.green));
                    new_depositBar.setStartColor(getResources().getColor(R.color.green));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void showLoader() {
        if (loadingView == null) {
            loadingView = new ProgressDialog(CWBioauthActivity.this);
            loadingView.setCancelable(false);
            loadingView.setMessage(getString(R.string.please_wait));
        }
        loadingView.show();
    }
    public void hideLoader() {
        try {
            if (loadingView != null) {
                loadingView.dismiss();
            }
        } catch (Exception e) {
        }
    }
    @Override
    public void onLocationUpdate(String latLong) {
        lat_long = latLong;
        showLog(TAG, lat_long);
    }
    @Override
    public void onLocationTurnedOn() {
        // Not Required
    }
}
