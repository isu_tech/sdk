package com.matm.matmsdk.aepsmodule.bankspinner;

import com.google.gson.annotations.SerializedName;

public class BankDetails {

    @SerializedName("ifsc_code")
    private String ifscCode;

    @SerializedName("bank_code")
    private String bankCode;

    @SerializedName("bank_name")
    private String bankName;

    @SerializedName("iin")
    private String iin;

    public String getIfscCode(){
        return ifscCode;
    }

    public String getBankCode(){
        return bankCode;
    }

    public String getBankName(){
        return bankName;
    }

    public String getIin(){
        return iin;
    }
}

