package com.matm.matmsdk.aepsmodule.aadharpay;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.UnifiedAepsTransactionActivity.TAG;
import static com.matm.matmsdk.aepsmodule.unifiedaeps.utils.Utils.showLog;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.unifiedaeps.AepsBaseActivity;
import com.matm.matmsdk.aepsmodule.utils.Session;
import com.moos.library.HorizontalProgressView;
import org.json.JSONException;
import org.json.JSONObject;
import isumatm.androidsdk.equitas.R;
@SuppressLint("SetTextI18n")
public class AadhaarPayBioAuthActivity extends AepsBaseActivity implements AepsBaseActivity.LocationUpdateListener {
    private Class<?> driverActivity;
    private String balanaceInquiryAadharNo = "";
    private boolean flagFromDriver = false;
    private TextView textUserTitle;
    private TextView newTextUserTitle;
    private HorizontalProgressView depositBar;
    private HorizontalProgressView new_depositBar;
    Button two_fact_submitButton;
    Button new_two_fact_submitButton;
    ProgressDialog loadingView;
    Session session;
    private String lat_long;
    ImageView two_fact_fingerprint;
    ImageView new_two_fact_fingerprint;
    String flagNameRdService = "";
    EditText aadharNumberET;
    TypedValue typedValue;
    Resources.Theme theme;
    LinearLayout aadhaarRegisterLayout;
    LinearLayout bioAuthLayout;
    String isUidPresent = "";
    String baseUrl="https://unifiedaepsbeta.iserveu.tech/";   //Production
    String uidRefId="";
    private boolean mIsBioAuthApiCalled = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aadhaar_pay_bio_auth);
        init();
        setLocationUpdateListener(this);
        checkLocationPermission();
        getRDServiceClass();
        onClickListener();
    }

    @Override
    public void onLocationTurnedOn() {
        if (!mIsBioAuthApiCalled) {
            mIsBioAuthApiCalled = true;
            checkAadhaarRegistrationStatus();
        }
    }
    private void checkAadhaarRegistrationStatus() {
        showLoader();
        AndroidNetworking.post(baseUrl + "fino/aadharRegistrationCheck")
                .setPriority(Priority.HIGH)
                .addHeaders(getString(R.string.authorization), SdkConstants.tokenFromCoreApp)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            int status = obj.getInt(getString(R.string.status_));
                            String statusDesc = obj.getString(getString(R.string.statusdesc));
                            if (status == 0) {
                                uidRefId= obj.optString(getString(R.string.uidrefid));
                                aadhaarRegisterLayout.setVisibility(View.GONE);
                                checkBioAuthStatus();
                            } else {
                                aadhaarRegisterLayout.setVisibility(View.VISIBLE);
                                isUidPresent = obj.optString(getString(R.string.isuidpresent));
                                resetFingerPrintAndStrength();
                                if (isUidPresent.equalsIgnoreCase(getString(R.string.true_))) {
                                    String uidRefId = obj.optString(getString(R.string.uidrefid));
                                    textUserTitle.setText(getString(R.string.hi_)+ SdkConstants.userNameFromCoreApp +
                                            getString(R.string.complete_verification_with_your_registered_aadhar_number) + uidRefId);
                                    aadharNumberET.setVisibility(View.VISIBLE);
                                    aadharNumberET.setText(uidRefId);
                                    aadharNumberET.setEnabled(false);
                                } else {
                                    textUserTitle.setVisibility(View.GONE);
                                }
                            }
                        } catch (Exception e) {
                            showAlert(getString(R.string.something_went_wrong_please_try_again));
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                        showAlert(getString(R.string.something_went_wrong_please_try_again));
                    }
                });
    }
    public void showAlert(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(AadhaarPayBioAuthActivity.this);
            builder.setTitle(getString(R.string.alert));
            builder.setMessage(msg);
            builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                dialog.dismiss();
                finish();
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void checkBioAuthStatus() {
        showLoader();
        AndroidNetworking.post(baseUrl + "fino/bioAuthCheck")
                .setPriority(Priority.HIGH)
                .addHeaders(getString(R.string.authorization), SdkConstants.tokenFromCoreApp)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            int status = obj.getInt(getString(R.string.status_));
                            String statusDesc = obj.optString(getString(R.string.statusdesc));
                            if (status == 0) {
                                goToAadhaarPayActivity();
                            } else {
                                bioAuthLayout.setVisibility(View.VISIBLE);
                                resetFingerPrintAndStrength();
                                newTextUserTitle.setText(getString(R.string.hi_) + SdkConstants.userNameFromCoreApp +
                                        getString(R.string.complete_verification_with_your_registered_aadhar_number) + uidRefId);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            showAlert(getString(R.string.something_went_wrong_please_try_again));
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                        showAlert(getString(R.string.something_went_wrong_please_try_again));
                    }
                });
    }
    private void goToAadhaarPayActivity() {
        Intent intent = new Intent(AadhaarPayBioAuthActivity.this, AadharpayActivity.class);
        startActivity(intent);
        finish();
    }
    private void onClickListener() {
        two_fact_fingerprint.setOnClickListener(v -> {
            try {
                showLoader();
                flagFromDriver = true;
                Intent launchIntent = new Intent(AadhaarPayBioAuthActivity.this, driverActivity);
                launchIntent.putExtra(getString(R.string.driverflag), flagNameRdService);
                launchIntent.putExtra(getString(R.string.freshnesfactor), session.getFreshnessFactor());
                launchIntent.putExtra(getString(R.string.aadharno), balanaceInquiryAadharNo);
                startActivityForResult(launchIntent, 1);
            } catch (Exception e) {
                hideLoader();
                Toast.makeText(AadhaarPayBioAuthActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        new_two_fact_fingerprint.setOnClickListener(v -> {
            try {
                showLoader();
                flagFromDriver = true;
                Intent launchIntent = new Intent(AadhaarPayBioAuthActivity.this, driverActivity);
                launchIntent.putExtra(getString(R.string.driverflag), flagNameRdService);
                launchIntent.putExtra(getString(R.string.freshnesfactor), session.getFreshnessFactor());
                launchIntent.putExtra(getString(R.string.aadharno), balanaceInquiryAadharNo);
                startActivity(launchIntent);
            } catch (Exception e) {
                hideLoader();
                Toast.makeText(AadhaarPayBioAuthActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        two_fact_submitButton.setOnClickListener(view -> {
            if (!flagFromDriver) {
                Toast.makeText(AadhaarPayBioAuthActivity.this, getString(R.string.please_do_biometric_verification),
                        Toast.LENGTH_LONG).show();
                return;
            }
            try {
                JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
                String scoreStr = respObj.getString(getString(R.string.pidata_qscore));
                String[] scoreList = scoreStr.split(",");
                scoreStr = scoreList[0];
                String pidData = respObj.getString(getString(R.string.base64piddata));
                if (Float.parseFloat(scoreStr) <= 40) {
                    Toast.makeText(AadhaarPayBioAuthActivity.this, getString(R.string.bad_fingerprint_strength_please_try_again),
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                JSONObject obj = new JSONObject();
                obj.put(getString(R.string.latlong), lat_long);
                if (!isUidPresent.equalsIgnoreCase(getString(R.string.true_))) {
                    obj.put(getString(R.string.aadharno), balanaceInquiryAadharNo);
                }
                obj.put(getString(R.string.piddata), pidData.replace("\n", ""));
                submitAadhaarRegistration(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        new_two_fact_submitButton.setOnClickListener(view -> {
            if (!flagFromDriver) {
                Toast.makeText(AadhaarPayBioAuthActivity.this, getString(R.string.please_do_biometric_verification),
                        Toast.LENGTH_LONG).show();
                return;
            }
            try {
                JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
                String scoreStr = respObj.getString(getString(R.string.pidata_qscore));
                String[] scoreList = scoreStr.split(",");
                scoreStr = scoreList[0];
                String pidData = respObj.getString(getString(R.string.base64piddata));

                if (Float.parseFloat(scoreStr) <= 40) {
                    Toast.makeText(AadhaarPayBioAuthActivity.this,
                            getString(R.string.bad_fingerprint_strength_please_try_again), Toast.LENGTH_SHORT).show();
                    return;
                }
                JSONObject obj = new JSONObject();
                obj.put(getString(R.string.latlong), lat_long);
                obj.put(getString(R.string.piddata), pidData.replace("\n", ""));
                submitBioAuth(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }
    private void submitBioAuth(JSONObject obj) {
        showLoader();
        AndroidNetworking.post(baseUrl + "fino/bioAuth")
                .setPriority(Priority.HIGH)
                .addHeaders(getString(R.string.authorization), SdkConstants.tokenFromCoreApp)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            int status = obj.getInt(getString(R.string.status_));
                            String statusDesc = obj.optString(getString(R.string.statusdesc));
                            Toast.makeText(AadhaarPayBioAuthActivity.this, statusDesc, Toast.LENGTH_SHORT).show();
                            if (status == 1) {
                                goToAadhaarPayActivity();
                            } else {
                                if (statusDesc.equalsIgnoreCase(getString(R.string.biometric_data_did_not_match))) {
                                    retryBioAuthAlert(statusDesc);
                                } else {
                                    showAlert(statusDesc);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                    }
                });
    }
    private void submitAadhaarRegistration(JSONObject obj) {
        showLoader();
        AndroidNetworking.post(baseUrl + "fino/aadharRegistration")
                .setPriority(Priority.HIGH)
                .addHeaders(getString(R.string.authorization), SdkConstants.tokenFromCoreApp)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            int status = obj.getInt(getString(R.string.status_));
                            String statusDesc = obj.optString(getString(R.string.statusdesc));
                            Toast.makeText(AadhaarPayBioAuthActivity.this, statusDesc, Toast.LENGTH_SHORT).show();
                            if (status == 1) {
                                aadhaarRegisterLayout.setVisibility(View.GONE);
                                checkBioAuthStatus();
                            }else {
                                showAlert(getString(R.string.something_went_wrong_please_try_again));
                            }
                        } catch (Exception e) {
                            showAlert(getString(R.string.something_went_wrong_please_try_again));
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                        showAlert(getString(R.string.something_went_wrong_please_try_again));
                    }
                });
    }
    private void init() {
        typedValue = new TypedValue();
        theme = this.getTheme();
        aadhaarRegisterLayout = findViewById(R.id.aadharRegLayout);
        bioAuthLayout = findViewById(R.id.bioAuthLayout);
        session = new Session(AadhaarPayBioAuthActivity.this);
        two_fact_fingerprint = findViewById(R.id.two_fact_fingerprint);
        new_two_fact_fingerprint = findViewById(R.id.new_two_fact_fingerprint);
        two_fact_fingerprint.setEnabled(true);
        new_two_fact_fingerprint.setEnabled(true);
        depositBar = findViewById(R.id.depositBar);
        new_depositBar = findViewById(R.id.new_depositBar);
        depositBar.setVisibility(View.GONE);
        new_depositBar.setVisibility(View.GONE);
        textUserTitle = findViewById(R.id.textUserTitle);
        newTextUserTitle = findViewById(R.id.newtextUserTitle);
        two_fact_submitButton = findViewById(R.id.two_fact_submitButton);
        new_two_fact_submitButton = findViewById(R.id.new_two_fact_submitButton);
        aadharNumberET = findViewById(R.id.balanceAadharNumber);
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();

        if (flagFromDriver) {
            hideLoader();
            if (SdkConstants.RECEIVE_DRIVER_DATA.isEmpty()) {
                flagFromDriver=false;
                two_fact_fingerprint.setEnabled(true);
                new_two_fact_fingerprint.setEnabled(true);
            }else {
                fingerStrength();
                new_two_fact_fingerprint.setEnabled(false);
                two_fact_fingerprint.setEnabled(false);
                two_fact_submitButton.setEnabled(true);
                new_two_fact_submitButton.setEnabled(true);
                two_fact_fingerprint.setColorFilter(null);
                new_two_fact_fingerprint.setColorFilter(null);
            }
        }
    }
    private void getRDServiceClass() {
        String accessClassName = SdkConstants.DRIVER_ACTIVITY;
        flagNameRdService = SdkConstants.MANUFACTURE_FLAG;
        try {
            driverActivity = Class.forName(accessClassName).asSubclass(Activity.class);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    public void fingerStrength() {
        try {
            JSONObject respObj = new JSONObject(SdkConstants.RECEIVE_DRIVER_DATA);
            String scoreStr = respObj.getString(getString(R.string.pidata_qscore));
            String[] scoreList = scoreStr.split(",");
            scoreStr = scoreList[0];
            String deviceType = "";
            if (respObj.has(getString(R.string.rdsid))) {
                deviceType = respObj.getString(getString(R.string.rdsid));
            }
            if (deviceType.contains(getString(R.string.scpl))) {
                depositBar.setVisibility(View.VISIBLE);
                depositBar.setProgress(Float.parseFloat(scoreStr));
                depositBar.setProgressTextMoved(true);
                depositBar.setStartColor(getResources().getColor(R.color.green));
                depositBar.setEndColor(getResources().getColor(R.color.green));
                /*new */
                new_depositBar.setVisibility(View.VISIBLE);
                new_depositBar.setProgress(Float.parseFloat(scoreStr));
                new_depositBar.setProgressTextMoved(true);
                new_depositBar.setStartColor(getResources().getColor(R.color.green));
                new_depositBar.setEndColor(getResources().getColor(R.color.green));
            } else {
                if (Float.parseFloat(scoreStr) <= 40) {
                    depositBar.setVisibility(View.VISIBLE);
                    depositBar.setProgress(Float.parseFloat(scoreStr));
                    depositBar.setProgressTextMoved(true);
                    depositBar.setEndColor(getResources().getColor(R.color.red));
                    depositBar.setStartColor(getResources().getColor(R.color.red));
                    /*new */
                    new_depositBar.setVisibility(View.VISIBLE);
                    new_depositBar.setProgress(Float.parseFloat(scoreStr));
                    new_depositBar.setProgressTextMoved(true);
                    new_depositBar.setEndColor(getResources().getColor(R.color.red));
                    new_depositBar.setStartColor(getResources().getColor(R.color.red));
                } else {
                    depositBar.setVisibility(View.VISIBLE);
                    depositBar.setProgress(Float.parseFloat(scoreStr));
                    depositBar.setProgressTextMoved(true);
                    depositBar.setEndColor(getResources().getColor(R.color.green));
                    depositBar.setStartColor(getResources().getColor(R.color.green));
                    /*new*/
                    new_depositBar.setVisibility(View.VISIBLE);
                    new_depositBar.setProgress(Float.parseFloat(scoreStr));
                    new_depositBar.setProgressTextMoved(true);
                    new_depositBar.setEndColor(getResources().getColor(R.color.green));
                    new_depositBar.setStartColor(getResources().getColor(R.color.green));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void showLoader() {
        if (loadingView == null) {
            loadingView = new ProgressDialog(AadhaarPayBioAuthActivity.this);
            loadingView.setCancelable(false);
            loadingView.setMessage(getString(R.string.please_wait));
        }
        loadingView.show();
    }
    public void hideLoader() {
        try {
            if (loadingView != null) {
                loadingView.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            hideLoader();
        }
    }
    public void retryBioAuthAlert(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(AadhaarPayBioAuthActivity.this);
            builder.setTitle(getString(R.string.alert));
            builder.setMessage(msg);
            builder.setPositiveButton(getString(R.string.retry), (dialog, which) -> {
                dialog.dismiss();
                resetFingerPrintAndStrength();
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void resetFingerPrintAndStrength(){
        depositBar.setVisibility(View.GONE);
        flagFromDriver = false;
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        int color = typedValue.data;
        two_fact_fingerprint.setColorFilter(color);
        two_fact_fingerprint.setEnabled(true);
        two_fact_fingerprint.setClickable(true);
        new_depositBar.setVisibility(View.GONE);
        new_two_fact_fingerprint.setColorFilter(color);
        new_two_fact_fingerprint.setEnabled(true);
        new_two_fact_fingerprint.setClickable(true);
    }
    @Override
    public void onLocationUpdate(String latLong) {
        lat_long = latLong;
        showLog(TAG, lat_long);
    }
}
