package com.matm.matmsdk.aepsmodule.bankspinner;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BankResponse{

    @SerializedName("data")
    private List<BankDetails> bankList;

    @SerializedName("status")
    private int status;

    public BankResponse() {
    }

    public List<BankDetails> getBankList(){
        return bankList;
    }

    public int getStatus(){
        return status;
    }
}

