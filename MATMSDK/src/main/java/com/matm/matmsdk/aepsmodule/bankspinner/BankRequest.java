package com.matm.matmsdk.aepsmodule.bankspinner;


import com.google.gson.annotations.SerializedName;

public class BankRequest {
    @SerializedName("operation_performed")
    private String operationPerformed;

    public void setOperationPerformed(String operationPerformed) {
        this.operationPerformed = operationPerformed;
    }
}

