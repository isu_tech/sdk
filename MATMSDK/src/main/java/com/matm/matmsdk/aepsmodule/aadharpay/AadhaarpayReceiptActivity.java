package com.matm.matmsdk.aepsmodule.aadharpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.RemoteException;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.matm.matmsdk.CustomThemes;
import com.matm.matmsdk.pdfFileUtils;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.aadharpay.model.AadharpayResponse;
import com.matm.matmsdk.aepsmodule.aadharpay.model.FCMNotificationData;
import com.matm.matmsdk.aepsmodule.utils.Constants;
import com.matm.matmsdk.aepsmodule.utils.GetPosConnectedPrinter;
import com.matm.matmsdk.aepsmodule.utils.Util;
import com.matm.matmsdk.notification.service.MyFirebaseMessagingService;
import com.matm.matmsdk.permission.PermissionsActivity;
import com.matm.matmsdk.permission.PermissionsChecker;
import com.matm.matmsdk.transaction_report.print.GetConnectToPrinter;
import com.matm.matmsdk.vriddhi.AEMPrinter;
import com.matm.matmsdk.vriddhi.AEMScrybeDevice;
import com.matm.matmsdk.vriddhi.CardReader;
import com.matm.matmsdk.vriddhi.IAemCardScanner;
import com.matm.matmsdk.vriddhi.IAemScrybe;
import com.pax.dal.entity.EFontTypeAscii;
import com.pax.dal.entity.EFontTypeExtCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import isumatm.androidsdk.equitas.R;
import wangpos.sdk4.libbasebinder.Printer;

import static com.matm.matmsdk.permission.PermissionsActivity.PERMISSION_REQUEST_CODE;
import static com.matm.matmsdk.permission.PermissionsChecker.REQUIRED_PERMISSION;

public class AadhaarpayReceiptActivity extends AppCompatActivity implements IAemCardScanner, IAemScrybe {
    public static final String TAG = AadhaarpayReceiptActivity.class.getSimpleName();
    private static final int pageHeight = 1120;
    private static final int pagewidth = 792;
    private ImageView ivStatusIcon;
    private ImageView ivPending;
    private ImageView sendButton;
    private TextView tvTransactionAmount;
    private TextView tvBankName;
    private TextView tvDateTime;
    private TextView tvTxnID;
    private TextView tvViewDetails;
    private TextView tvProgressViewMessage;
    private TextView tvTransactionStatusDecs;
    private Button btnPrint;
    private Button btnDownload;
    private Button btnClose;
    private Button btnUpdateStatus;
    private Button btnOk;
    private LinearLayout ll_progressBar_container;
    private RelativeLayout rl_mainLayout;
    private ProgressBar pb_bigProgressBar;
    private CountDownTimer countDownTimer;
    private boolean onceDataLoaded = false;
    private String aadhaarNumber_dialog = "N/A";
    private String rrNumber_dialog = "N/A";
    private String transactionType_dialog = "N/A";
    private String balanceAmt_dialog = "N/A";
    private String _statusMain = "";
    private String finalTxnID = "N/A";
    private LinearLayout mobileEditLayout;
    private LinearLayout mobileTextLayout;
    private EditText editTextMobile;
    private CheckBox mobileCheckBox;
    private ProgressDialog progressDialog;
    private AadharpayResponse aadharpayResponse;
    private FCMNotificationData fcmNotificationData;
    private PermissionsChecker checker;
    private BluetoothAdapter bluetoothAdapter;
    private String txnIdForPdf;
    private String filePath = "";
    //For Vriddhi
    AEMScrybeDevice m_AemScrybeDevice;
    AEMPrinter m_AemPrinter = null;
    CardReader m_cardReader = null;
    CardReader.CARD_TRACK cardTrackType;
    String creditData, tempdata, replacedData, data;
    ArrayList<String> printerList;
    String responseString, response;
    String[] responseArray = new String[1];
    char[] printerStatus = new char[]{0x1B, 0x7E, 0x42, 0x50, 0x7C, 0x47, 0x45, 0x54, 0x7C, 0x50, 0x52, 0x4E, 0x5F, 0x53, 0x54, 0x5E};
    String printerName;
    private wangpos.sdk4.libbasebinder.Printer mPrinter;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this); // This is for Custom theme
        setContentView(R.layout.activity_aadhaarpay_receipt);
        initilizationOfviews(); // This is for view initialization
        MyFirebaseMessagingService myFirebaseMessagingService = new MyFirebaseMessagingService();
        checker = new PermissionsChecker(AadhaarpayReceiptActivity.this); // To allow all the permission for print and download
        Date date = Calendar.getInstance().getTime();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        m_AemScrybeDevice = new AEMScrybeDevice(AadhaarpayReceiptActivity.this);
        printerList = new ArrayList<String>();
        creditData = new String();
        // Display a date in day, month, year format
        DateFormat formatter = new SimpleDateFormat(getString(R.string.dd_mm_yyyy_hh_mm_ss));
        String currentDateandTime = formatter.format(date);
        System.out.println(getString(R.string.today_) + currentDateandTime);
        /** collect the data from previous activity*/
        Gson gson = new Gson();
        if (getIntent() != null && getIntent().hasExtra(getString(R.string.myjson))) {
            if (getIntent().getStringExtra(getString(R.string.myjson)) != null) {
                try {
                    aadharpayResponse = gson.fromJson(getIntent().getStringExtra(getString(R.string.myjson)),
                            AadharpayResponse.class); // Convert the JSON to Model
                    int status = 1;
                    String txnID = getString(R.string.n_a);
                    if (aadharpayResponse != null) {
                        status = aadharpayResponse.getStatus();
                        txnID = aadharpayResponse.getTxId();
                    } else {
                        ll_progressBar_container.setVisibility(View.GONE);
                        rl_mainLayout.setVisibility(View.VISIBLE);
                        setDataToView(getString(R.string.failed), getString(R.string.n_a), getString(R.string.n_a),
                                getString(R.string.this_transaction_got_failed_please_check_the_report), getString(R.string.n_a));
                    }
                    /*If the status is 0 then it is searching for response in Notification*/
                    if (status == 0) {
                        showLoaderForMaxOneMinutes();
                    } else {
                        ll_progressBar_container.setVisibility(View.GONE);
                        rl_mainLayout.setVisibility(View.VISIBLE);
                        setDataToView(getString(R.string.failed), aadharpayResponse.getTxId(),
                                aadharpayResponse.getTransactionAmount(), aadharpayResponse.getTransactionStatus(),
                                aadharpayResponse.getBankName());
                    }
                    finalTxnID = txnID;
                } catch (Exception e) {
                    e.printStackTrace();
                    ll_progressBar_container.setVisibility(View.GONE);
                    rl_mainLayout.setVisibility(View.VISIBLE);
                    setDataToView(getString(R.string.failed), getString(R.string.n_a), getString(R.string.n_a),
                            getString(R.string.this_transaction_got_failed_please_check_the_report), getString(R.string.n_a));
                }

            } else {
                ll_progressBar_container.setVisibility(View.GONE);
                rl_mainLayout.setVisibility(View.VISIBLE);
                setDataToView(getString(R.string.failed), getString(R.string.n_a), getString(R.string.n_a),
                        getString(R.string.this_transaction_got_failed_please_check_the_report), getString(R.string.n_a));
            }
        } else {
            ll_progressBar_container.setVisibility(View.GONE);
            rl_mainLayout.setVisibility(View.VISIBLE);
            setDataToView(getString(R.string.failed), "N/A", "N/A", "This transaction got failed, Please check the report..", "N/A");
        }
        /** this event is for fetch the transaction via api{If the notification is not received }*/
        btnUpdateStatus.setOnClickListener(v -> callFetchdataAPI(finalTxnID));
        /** this will */
        btnClose.setOnClickListener(v -> {
            finish();
        });
        btnOk.setOnClickListener(v -> finish());
        tvViewDetails.setOnClickListener(v -> showTransactionDetails(AadhaarpayReceiptActivity.this,
                aadhaarNumber_dialog, rrNumber_dialog, transactionType_dialog, balanceAmt_dialog));
        btnDownload.setOnClickListener(v -> {
            if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
                PermissionsActivity.startActivityForResult(AadhaarpayReceiptActivity.this,
                        PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
            } else {
                Date date1 = new Date();
                long timeMilli = date1.getTime();
                System.out.println(getString(R.string.time_in_milliseconds_using_date_class) + timeMilli);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    createPdf(pdfFileUtils.commonDocumentDirPath(getString(R.string.pdf)) + timeMilli +
                            getString(R.string.order_receipt_pdf));
                }else {
                    createPdf(pdfFileUtils.getAppPath(getApplicationContext()) + timeMilli + getString(R.string.order_receipt_pdf));
                }
            }
        });
        btnPrint.setOnClickListener(v -> {
            String deviceModel = Build.MODEL;
            if(deviceModel.equalsIgnoreCase(getString(R.string.a910))) {
                getPrintData(tvTxnID.getText().toString(), aadhaarNumber_dialog, tvDateTime.getText().toString(),
                        tvBankName.getText().toString(),
                        rrNumber_dialog, transactionType_dialog );
            }else if(deviceModel.equalsIgnoreCase(getString(R.string.wpos_3))){
                //start printing with wiseasy internal printer
                new UnifiedPrintReceiptThread().start();
            }  else {
                registerForContextMenu(btnPrint);
                if (bluetoothAdapter == null) {
                    Toast.makeText(AadhaarpayReceiptActivity.this, getString(R.string.bluetooth_not_supported),
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (bluetoothAdapter.isEnabled()) {
                        if (GetPosConnectedPrinter.aemPrinter == null) {
                            printerList = m_AemScrybeDevice.getPairedPrinters();
                            if (!printerList.isEmpty()) {
                                openContextMenu(v);
                            } else {
                                showAlert(getString(R.string.no_paired_printers_found));
                            }
                        } else {
                            m_AemPrinter = GetPosConnectedPrinter.aemPrinter;
                            if (balanceAmt_dialog.equals("") || balanceAmt_dialog.equalsIgnoreCase(getString(R.string.n_a))) {
                                balanceAmt_dialog = getString(R.string.n_a);
                            }else{
                                balanceAmt_dialog = getString(R.string.rs) + balanceAmt_dialog;
                            }
                            callBluetoothFunction(tvTxnID.getText().toString(), tvDateTime.getText().toString(),
                                    tvBankName.getText().toString(), rrNumber_dialog,
                                    tvTransactionAmount.getText().toString(), balanceAmt_dialog,
                                    transactionType_dialog, aadhaarNumber_dialog, _statusMain, v);
                        }
                    } else {
                        GetPosConnectedPrinter.aemPrinter = null;
                        Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivity(turnOn);
                    }
                }
            }
        });
        mobileCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mobileEditLayout.setVisibility(View.VISIBLE);
            } else {
                mobileEditLayout.setVisibility(View.GONE);
            }
        });
        editTextMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 10) {
                    editTextMobile.setError(getResources().getString(R.string.mobileerror));
                }
                if (s.length() > 0) {
                    editTextMobile.setError(null);
                    String x = s.toString();
                    if (x.startsWith("0") || !Util.isValidMobile(editTextMobile.getText().toString().trim())) {
                        editTextMobile.setError(getResources().getString(R.string.mobilevaliderror));
                    }
                }
            }
        });
        sendButton.setOnClickListener(v -> {
            if (editTextMobile.getText() == null || editTextMobile.getText().toString().trim().matches("") ||
                    !Util.isValidMobile(editTextMobile.getText().toString().trim())) {
                editTextMobile.setError(getResources().getString(R.string.mobileerror));
            } else {
                showLoader();
                mobileNumberSMS();
            }
        });
    }
    @Override
    protected void onRestart() {
        super.onRestart();
    }
    private void callBluetoothFunction(String txnId, String date, String bankName, String rrNumber,
                                       String transactionAmount, String balanceAmount, String transactionType,
                                       String aadhaarNumber, String txnStatus,View view) {
        try {
            m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
            m_AemPrinter.setFontType(AEMPrinter.FONT_002);
            m_AemPrinter.POS_FontThreeInchCENTER();
            m_AemPrinter.print(SdkConstants.SHOP_NAME);
            m_AemPrinter.print("\n");
            m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            m_AemPrinter.print(getString(R.string.transaction_report));
            m_AemPrinter.POS_FontThreeInchCENTER();
            m_AemPrinter.print(txnStatus);
            m_AemPrinter.print("\n\n");
            m_AemPrinter.print(txnId);
            m_AemPrinter.print("\n");
            m_AemPrinter.print(getString(R.string.aadhaar_number) + aadhaarNumber);
            m_AemPrinter.print("\n");
            m_AemPrinter.print(getString(R.string.date_time) + date);
            m_AemPrinter.print("\n");
            m_AemPrinter.print(getString(R.string.bank_name) + bankName);
            m_AemPrinter.print("\n");
            m_AemPrinter.print(getString(R.string.rrn_no) + rrNumber);
            m_AemPrinter.print("\n");
            m_AemPrinter.print(getString(R.string.balance_amount) + balanceAmount);
            m_AemPrinter.print("\n");
            m_AemPrinter.print( transactionAmount);
            m_AemPrinter.print("\n");
            m_AemPrinter.print(getString(R.string.transaction_type) + transactionType);
            m_AemPrinter.print("\n\n");
            m_AemPrinter.setFontType(AEMPrinter.FONT_002);
            m_AemPrinter.POS_FontThreeInchRIGHT();
            m_AemPrinter.print(getString(R.string.thank_you));
            m_AemPrinter.POS_FontThreeInchRIGHT();
            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_RIGHT);
            m_AemPrinter.print(SdkConstants.BRAND_NAME);
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            data = printerStatus();
            m_AemPrinter.print(data);
            m_AemPrinter.print("\n");
        } catch (IOException e) {
            try{
                getConnection(view);
            }catch (Exception exception){
                exception.printStackTrace();
            }
        }
    }
    private void createPdf(String pathFile) {
        filePath = pathFile;
        createPdfGenericMethod(this, pathFile);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2296) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    createPdfGenericMethod(AadhaarpayReceiptActivity.this, filePath);
                } else {
                    Toast.makeText(this, R.string.allow_permission_for_storage_access, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    private void createPdfGenericMethod(Context context, String pathFile) {
        PdfDocument pdfDocument = new PdfDocument();
        Paint imagePaint = new Paint();
        Paint faildStaus = new Paint();
        Paint keys = new Paint();
        Paint value = new Paint();
        Paint amount = new Paint();
        Paint centerData = new Paint();
        Paint rightData = new Paint();
        Paint successStatus = new Paint();
        Paint txnDetails = new Paint();
        PdfDocument.PageInfo mypageInfo = new PdfDocument.PageInfo.Builder(pagewidth, pageHeight, 1).create();
        PdfDocument.Page myPage = pdfDocument.startPage(mypageInfo);
        //it is used for design pdf
        Canvas canvas = myPage.getCanvas();
        /* below line is used for adding typeface for
         our text which we will be adding in our PDF file.*/
        faildStaus.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        keys.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        value.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
        amount.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        successStatus.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        txnDetails.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        /* below line is used for setting text size
         which we will be displaying in our PDF file.*/
        faildStaus.setTextSize(40);
        faildStaus.setColor(Color.RED);
        faildStaus.setTextAlign(Paint.Align.CENTER);
        keys.setTextSize(26);
        keys.setTextAlign(Paint.Align.LEFT);
        successStatus.setTextSize(40);
        successStatus.setColor(Color.GREEN);
        successStatus.setTextAlign(Paint.Align.CENTER);
        centerData.setTextSize(36);
        centerData.setTextAlign(Paint.Align.CENTER);
        rightData.setTextSize(26);
        rightData.setTextAlign(Paint.Align.RIGHT);
        // below line is used for text color inside our PDF file.
        //it is used for text draw on PDF
        canvas.drawText(SdkConstants.SHOP_NAME, 400, 100, centerData);
        canvas.drawText(getString(R.string.receipt), 400, 150, centerData);
        if (_statusMain.equalsIgnoreCase("FAILED")) {
            canvas.drawText(_statusMain, 400, 200, faildStaus);
        } else {
            canvas.drawText(_statusMain, 400, 200, successStatus);
        }
        canvas.drawText(getString(R.string.date_time) + tvDateTime.getText().toString().trim(), 50, 280, keys);
        canvas.drawText(getString(R.string.operation_performed) + "AadhaarPay", 50, 330, keys);
        canvas.drawText(getString(R.string.transactiondetails), 400, 400, centerData);
        canvas.drawText(getString(R.string.transaction_id) + txnIdForPdf, 50, 480, keys);
        canvas.drawText(getString(R.string.aadhaar_number) + aadhaarNumber_dialog, 50, 530, keys);
        canvas.drawText(getString(R.string.bank_name) + tvBankName.getText().toString().trim(), 50, 580, keys);
        canvas.drawText(getString(R.string.rrn) + rrNumber_dialog, 50, 630, keys);
        if (balanceAmt_dialog.equals("") || balanceAmt_dialog.equalsIgnoreCase(getString(R.string.n_a))) {
            balanceAmt_dialog = getString(R.string.n_a);
        } else {
            balanceAmt_dialog = getString(R.string.rs) + balanceAmt_dialog;
        }
        canvas.drawText(getString(R.string.balance_amount) + balanceAmt_dialog, 50, 680, keys);
        canvas.drawText(getString(R.string.transaction_amount_rs) + SdkConstants.transactionAmount, 50, 730, keys);
        canvas.drawText(getString(R.string.transaction_type) + getString(R.string.aadharpay), 50, 780, keys);
        canvas.drawText(getString(R.string.thank_you), 700, 880, rightData);
        canvas.drawText(SdkConstants.BRAND_NAME, 700, 930, rightData);
        pdfDocument.finishPage(myPage);
        // below line is used to set the name of PDF file and its path.
        String save;
        int num = 0;
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) +
                getString(R.string.walletcashout_report_pdf));
        while (folder.exists()) {
            save = getString(R.string.transaction_report) + (num++) + getString(R.string._pdf);
            folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), save);
        }
        try {
            pdfDocument.writeTo(new FileOutputStream(folder));
            Toast.makeText(context, getString(R.string.pdf_saved_in_the_internal_storage), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        pdfDocument.close();
        Uri path = FileProvider.getUriForFile(context,
                context.getApplicationContext().getPackageName() + getString(R.string.provider), folder);
        Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
        pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pdfOpenintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        pdfOpenintent.setDataAndType(path, getString(R.string.application_pdf));
        try {
            context.startActivity(pdfOpenintent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void showTransactionDetails(Activity activity, String aadhaarNumber, String rrNumber,
                                       String transactionType, String balanceAmt) {
        try {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.transaction_aeps_details_layout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            TextView aadhar_number = (TextView) dialog.findViewById(R.id.aadhar_number);
            TextView rref_num = (TextView) dialog.findViewById(R.id.rref_num);
            TextView card_transaction_amount = (TextView) dialog.findViewById(R.id.card_transaction_amount);
            TextView card_transaction_type = (TextView) dialog.findViewById(R.id.card_transaction_type);

            if (aadhaarNumber.equals("") || aadhaarNumber == null) {
                aadhar_number.setText(getString(R.string.n_a));
            } else {
                aadhar_number.setText(aadhaarNumber);
            }
            if (rrNumber.equals("") || rrNumber == null) {
                rref_num.setText(getString(R.string.n_a));
            } else {
                rref_num.setText(rrNumber);
            }
            if (balanceAmt.equals("") || balanceAmt.equalsIgnoreCase(getString(R.string.n_a))) {
                card_transaction_amount.setText(getString(R.string.n_a));
            } else {
                card_transaction_amount.setText(balanceAmt);
            }
            card_transaction_type.setText(transactionType);
            Button dialogBtn_close = (Button) dialog.findViewById(R.id.close_Btn);
            dialogBtn_close.setOnClickListener(v -> dialog.cancel());
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void initilizationOfviews() {
        ivStatusIcon = findViewById(R.id.status_icon);
        tvTransactionAmount = findViewById(R.id.card_amount);
        tvTransactionStatusDecs = findViewById(R.id.balanceText);
        tvBankName = findViewById(R.id.bank_name);
        tvDateTime = findViewById(R.id.date_time);
        tvTxnID = findViewById(R.id.txnID);
        tvViewDetails = findViewById(R.id.txndetails);
        tvProgressViewMessage = findViewById(R.id.pb_aeps3_tv);
        btnPrint = findViewById(R.id.printBtn);
        btnDownload = findViewById(R.id.downloadBtn);
        btnClose = findViewById(R.id.closeBtn);
        btnUpdateStatus = findViewById(R.id.btn_updateStatus);
        btnOk = findViewById(R.id.btn_ok);
        ll_progressBar_container = findViewById(R.id.progress_container);
        rl_mainLayout = findViewById(R.id.rl_main);
        pb_bigProgressBar = findViewById(R.id.big_pb);
        ivPending = findViewById(R.id.iv_aeps3_pending);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        /*for sms feature*/
        mobileCheckBox = findViewById(R.id.mobileCheckBox);
        mobileEditLayout = findViewById(R.id.mobileEditLayout);
        mobileTextLayout = findViewById(R.id.mobileTextLayout);
        editTextMobile = findViewById(R.id.editTextMobile);
        sendButton = findViewById(R.id.sendButton);
    }
    private void callFetchdataAPI(String transactionID) {
        rl_mainLayout.setVisibility(View.GONE);
        ll_progressBar_container.setVisibility(View.VISIBLE);
        btnUpdateStatus.setVisibility(View.GONE);
        pb_bigProgressBar.setVisibility(View.VISIBLE);
        ivPending.setVisibility(View.GONE);
        tvProgressViewMessage.setText(getString(R.string.processing));
        String url = Constants.FETCH_TRANSACTION_DETAILS + "/" + transactionID;

        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (!response.toString().equals("") || response.toString() != null) {
                            TransitionManager.beginDelayedTransition(rl_mainLayout);
                            rl_mainLayout.setVisibility(View.VISIBLE);
                            ll_progressBar_container.setVisibility(View.GONE);
                            btnOk.setVisibility(View.GONE);
                            try {
                                aadhaarNumber_dialog = response.getString(getString(R.string.origin_identifier));
                                rrNumber_dialog = response.getString(getString(R.string.apitid_small));
                                transactionType_dialog = getString(R.string.aadhaarpay);
                                balanceAmt_dialog = response.getString(getString(R.string.balance));
                                _statusMain = response.getString(getString(R.string.status_));
                                setDataToView(_statusMain, response.getString(getString(R.string.txid)),
                                        aadharpayResponse.getTransactionAmount(),
                                        response.getString(getString(R.string.apicomment)),
                                        aadharpayResponse.getBankName());
                            } catch (JSONException e) {
                                showAlert(getString(R.string.something_went_wrong_please_check_the_report_for_transaction_details_or_contact_to_support_iservu_in));
                                e.printStackTrace();
                            }
                        } else {
                            showAlert(getString(R.string.something_went_wrong_please_check_the_report_for_transaction_details_or_contact_to_support_iservu_in));
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        TransitionManager.beginDelayedTransition(rl_mainLayout);
                        rl_mainLayout.setVisibility(View.GONE);
                        pb_bigProgressBar.setVisibility(View.GONE);
                        ll_progressBar_container.setVisibility(View.VISIBLE);
                        btnUpdateStatus.setVisibility(View.VISIBLE);
                        ivPending.setVisibility(View.VISIBLE);
                        tvProgressViewMessage.setText(R.string.pending);
                        btnOk.setVisibility(View.VISIBLE);
                    }
                });
    }
    /**
     * this function will show one loader for 50 sec.
     * it will stop when the transaction response will receive via Firebase Notification.
     * if the notifiaction will not receive then it will show a FETCH button to fetch the transaction details.
     */
    private void showLoaderForMaxOneMinutes() {
        ll_progressBar_container.setVisibility(View.VISIBLE);
        rl_mainLayout.setVisibility(View.GONE);
        pb_bigProgressBar.setVisibility(View.VISIBLE);
        ivPending.setVisibility(View.GONE);
        btnUpdateStatus.setVisibility(View.GONE);
        countDownTimer = new CountDownTimer(50000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (Constants.NOTIFICATION_RECEIVED) { // this check will be true when the notification will receive
                    Gson gson = new Gson();
                    fcmNotificationData = gson.fromJson(Constants.THE_FIREBASE_DATA, FCMNotificationData.class);
                    if (fcmNotificationData.getTxId().length() != 0 && aadharpayResponse.getTxId().length() != 0) {
                        if (aadharpayResponse.getTxId().equals(fcmNotificationData.getTxId())) {// To check the txn ID from FCM and Previous Activity
                            rl_mainLayout.setVisibility(View.VISIBLE);
                            ll_progressBar_container.setVisibility(View.GONE);
                            btnUpdateStatus.setVisibility(View.GONE);
                            onFinish();
                            cancel();
                            if (!onceDataLoaded) { // To not over load/ Update in live
                                aadhaarNumber_dialog = fcmNotificationData.getAadhaarNumber();
                                rrNumber_dialog = fcmNotificationData.getRrn();
                                transactionType_dialog = getString(R.string.aadhaarpay);
                                balanceAmt_dialog = fcmNotificationData.getBalanceAmount();
                                _statusMain = fcmNotificationData.getStatus();
                                setDataToView(_statusMain, fcmNotificationData.getTxId(),
                                        aadharpayResponse.getTransactionAmount(), fcmNotificationData.getStatusDesc(),
                                        aadharpayResponse.getBankName());
                                onceDataLoaded = true;
                            }
                        }
                    }
                    Constants.NOTIFICATION_RECEIVED = false;
                }
            }
            public void onFinish() {
                if (!Constants.NOTIFICATION_RECEIVED || !fcmNotificationData.getTxId().equals(aadharpayResponse.getTxId())) {
                    tvProgressViewMessage.setText(getString(R.string.pending));
                    pb_bigProgressBar.setVisibility(View.GONE);
                    ivPending.setVisibility(View.VISIBLE);
                    btnUpdateStatus.setVisibility(View.VISIBLE);
                }
                cancel();
            }
        }.start();
    }
    public void showAlert(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(AadhaarpayReceiptActivity.this);
            builder.setTitle(getString(R.string.alert));
            builder.setMessage(msg);
            builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                dialog.dismiss();
                finish();
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @SuppressLint("SetTextI18n")
    public void setDataToView(String status, String txnID, String amount, String statusDesc, String bankName) {
        _statusMain = status;
        txnIdForPdf = txnID;
        if (status.equals(getString(R.string.success_caps))) {
            ivStatusIcon.setImageResource(R.drawable.hero_success);
            editTextMobile.setText(aadharpayResponse.getMobileNumber());
            if (SdkConstants.applicationType.equalsIgnoreCase(getString(R.string.core))) {
                mobileTextLayout.setVisibility(View.GONE);
            }
        } else if (status.equals(getString(R.string.failed))) {
            ivStatusIcon.setImageResource(R.drawable.hero_failure);
        } else {
            ivStatusIcon.setImageResource(R.drawable.pending);
        }
        if (amount.equals("") || amount.equals(null)) {
            tvTransactionAmount.setText(getString(R.string.txn_amount_rs) +getString(R.string.n_a));
        } else {
            tvTransactionAmount.setText(getString(R.string.txn_amount_rs) + amount);
        }
        if (statusDesc.equals("") || statusDesc == null) {
            tvTransactionStatusDecs.setText(getString(R.string.n_a));
        } else {
            tvTransactionStatusDecs.setText(statusDesc);
        }
        if (bankName.equals("") || bankName == null) {
            tvBankName.setText(getString(R.string.n_a));
        } else {
            tvBankName.setText(bankName);
        }
        Date date = Calendar.getInstance().getTime();
        // Display a date in day, month, year format
        DateFormat formatter = new SimpleDateFormat(getString(R.string.dd_mm_yyyy_hh_mm_ss));
        String currentDateandTime = formatter.format(date);
        tvDateTime.setText(currentDateandTime);
        if (txnID.equals("") || txnID.equals(null)) {
            tvTxnID.setText(getString(R.string.transaction_id) + getString(R.string.n_a));
        } else {
            tvTxnID.setText(getString(R.string.transaction_id) + txnID);
        }
    }
    /*sms feature methods*/
    public void mobileNumberSMS() {
        if (balanceAmt_dialog.equals("") || balanceAmt_dialog == null) {
            balanceAmt_dialog = getString(R.string.n_a);
        }
        String msgValue = getString(R.string.thanks_for_visiting) + SdkConstants.SHOP_NAME +
                getString(R.string.current_balance_for) + tvBankName.getText().toString().trim() +
                getString(R.string.account_seeded_with_aadhaar) + aadhaarNumber_dialog +
                getString(R.string.is_rs) + balanceAmt_dialog + getString(R.string.dated) +
                tvDateTime.getText().toString().trim() + getString(R.string.thanks_itpl);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(getString(R.string.user_name_), SdkConstants.userNameFromCoreApp);
            jsonObject.put(getString(R.string.mobilenumber_camelcase), editTextMobile.getText().toString());
            jsonObject.put(getString(R.string.smsfor), getString(R.string.transaction));
            jsonObject.put(getString(R.string.message), msgValue);

            AndroidNetworking.post("https://wallet-deduct-sms-vn3k2k7q7q-uc.a.run.app/")
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString(getString(R.string.status_));
                                String msg = obj.optString(getString(R.string.message));
                                if (status.equalsIgnoreCase("0")) {
                                    JSONObject results = obj.getJSONObject(getString(R.string.results));
                                    String statusMsg = results.getString(getString(R.string.status_));
                                    String message = results.getString(getString(R.string.message));
                                    hideLoader();
                                    Toast.makeText(AadhaarpayReceiptActivity.this,
                                            getString(R.string.message_sent_successfully), Toast.LENGTH_SHORT).show();
                                } else {
                                    hideLoader();
                                    Toast.makeText(AadhaarpayReceiptActivity.this, msg, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                hideLoader();
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorBody();
                            hideLoader();
                            Toast.makeText(AadhaarpayReceiptActivity.this,
                                    getString(R.string.wallet_balance_not_available), Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void showLoader() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(AadhaarpayReceiptActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.please_wait));
        }
        progressDialog.show();
    }
    public void hideLoader() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
    @Override
    public void onBackPressed() {
        try {
            if (_statusMain.equals(getString(R.string.failed))) {
                Intent intent = new Intent(this, AadharpayActivity.class);
                intent.putExtra(getString(R.string.transactionstatus_), getString(R.string.failed));
                startActivity(intent);
            }
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(R.string.select_printer_to_connect);
        for (int i = 0; i < printerList.size(); i++) {
            menu.add(0, v.getId(), 0, printerList.get(i));
        }
    }
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        super.onContextItemSelected(item);
        printerName= item.getTitle().toString();
        try {
            m_AemScrybeDevice.connectToPrinter(printerName);
            m_cardReader = m_AemScrybeDevice.getCardReader(this);
            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            GetPosConnectedPrinter.aemPrinter = m_AemPrinter;
            Toast.makeText(AadhaarpayReceiptActivity.this, getString(R.string.connected_with) +
                    printerName, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            if (e.getMessage().contains(getString(R.string.service_discovery_failed))) {
                Toast.makeText(AadhaarpayReceiptActivity.this, getString(R.string.not_connected) + printerName +
                        getString(R.string.is_unreachable_or_off_otherwise_it_is_connected_with_other_device),
                        Toast.LENGTH_SHORT).show();
            } else if (e.getMessage().contains(getString(R.string.device_or_resource_busy))) {
                Toast.makeText(AadhaarpayReceiptActivity.this, getString(R.string.the_device_is_already_connected),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(AadhaarpayReceiptActivity.this, getString(R.string.unable_to_connect),
                        Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }
    public void onScanMSR(final String buffer, CardReader.CARD_TRACK cardTrack) {
        cardTrackType = cardTrack;
        creditData = buffer;
        AadhaarpayReceiptActivity.this.runOnUiThread(new Runnable() {
            public void run() {
            }
        });
    }
    public void onScanDLCard(final String buffer) {
        CardReader.DLCardData dlCardData = m_cardReader.decodeDLData(buffer);
        String name = getString(R.string.name) + dlCardData.NAME + "\n";
        String SWD = getString(R.string.swd_of) + dlCardData.SWD_OF + "\n";
        String dob = getString(R.string.dob) + dlCardData.DOB + "\n";
        String dlNum = getString(R.string.dlnum) + dlCardData.DL_NUM + "\n";
        String issAuth = getString(R.string.iss_auth) + dlCardData.ISS_AUTH + "\n";
        String doi = getString(R.string.doi) + dlCardData.DOI + "\n";
        String tp = getString(R.string.valid_tp) + dlCardData.VALID_TP + "\n";
        String ntp = getString(R.string.valid_ntp) + dlCardData.VALID_NTP + "\n";
        final String data = name + SWD + dob + dlNum + issAuth + doi + tp + ntp;
        runOnUiThread(() -> {
        });
    }
    public void onScanRCCard(final String buffer) {
        CardReader.RCCardData rcCardData = m_cardReader.decodeRCData(buffer);
        String regNum = getString(R.string.reg_num) + rcCardData.REG_NUM + "\n";
        String regName = getString(R.string.reg_name) + rcCardData.REG_NAME + "\n";
        String regUpto = getString(R.string.reg_upto) + rcCardData.REG_UPTO + "\n";
        final String data = regNum + regName + regUpto;
        runOnUiThread(() -> {
        });
    }
    @Override
    public void onScanRFD(final String buffer) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(buffer);
        String temp = "";
        try {
            temp = stringBuffer.deleteCharAt(8).toString();
        } catch (Exception e) {
            // TODO: handle exception
        }
        final String data = temp;
        AadhaarpayReceiptActivity.this.runOnUiThread(() -> {
            try {
                m_AemPrinter.print(data);
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }
    public void onDiscoveryComplete(ArrayList<String> aemPrinterList) {
        printerList = aemPrinterList;
        for (int i = 0; i < aemPrinterList.size(); i++) {
            String Device_Name = aemPrinterList.get(i);
            String status = m_AemScrybeDevice.pairPrinter(Device_Name);
        }
    }
    @Override
    public void onScanPacket(String buffer) {
        if (buffer.equals(getString(R.string.printerok))) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(buffer);
            String temp = "";
            try {
                temp = stringBuffer.toString();
            } catch (Exception e) {
                // TODO: handle exception
            }
            tempdata = temp;
            final String strData = tempdata.replace("|", "&");
            final String[][] formattedData = {strData.split("&", 3)};
            responseString = formattedData[0][2];
            responseArray[0] = responseString.replace("^", "");
            AadhaarpayReceiptActivity.this.runOnUiThread(() -> {
                replacedData = tempdata.replace("|", "&");
                formattedData[0] = replacedData.split("&", 3);
                response = formattedData[0][2];
            });
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(buffer);
            String temp = "";
            try {
                temp = stringBuffer.toString();
            } catch (Exception e) {
                // TODO: handle exception
            }
            tempdata = temp;
            final String strData = tempdata.replace("|", "&");
            final String[][] formattedData = {strData.split("&", 3)};
            responseString = formattedData[0][2];
            responseArray[0] = responseString.replace("^", "");
            AadhaarpayReceiptActivity.this.runOnUiThread(() -> {
                replacedData = tempdata.replace("|", "&");
                formattedData[0] = replacedData.split("&", 3);
                response = formattedData[0][2];
            });
        }
    }
    public String printerStatus() throws IOException {
        String data = new String(printerStatus);
        m_AemPrinter.print(data);
        return data;
    }
    private void getPrintData(final String txnId,final String adharcard, final String date, final String bankname,
                              final String referenceNo, final String type) {
        new Thread(() -> {
            GetConnectToPrinter.getInstance().init();
            //Shop Name Set
            GetConnectToPrinter.getInstance().fontSet(EFontTypeAscii.FONT_24_48,
                    EFontTypeExtCode.FONT_24_48);
            GetConnectToPrinter.getInstance().leftIndents(Short.parseShort("60"));
            GetConnectToPrinter.getInstance().setInvert(false);
            GetConnectToPrinter.getInstance().printStr(SdkConstants.SHOP_NAME, null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("20"));
            //Transaction Details Message
            GetConnectToPrinter.getInstance().fontSet(EFontTypeAscii.FONT_16_32,
                    EFontTypeExtCode.FONT_16_32);
            GetConnectToPrinter.getInstance().printStr(getString(R.string.txn_report_txt),null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("20"));
            //status Message
            GetConnectToPrinter.getInstance().fontSet(EFontTypeAscii.FONT_16_32,
                    EFontTypeExtCode.FONT_16_32);
            GetConnectToPrinter.getInstance().leftIndents(Short.parseShort("110"));
            GetConnectToPrinter.getInstance().printStr(_statusMain,null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("30"));
            GetConnectToPrinter.getInstance().fontSet(EFontTypeAscii.FONT_12_24,
                    EFontTypeExtCode.FONT_16_32);
            GetConnectToPrinter.getInstance().spaceSet((byte) 0,(byte) 0);
            GetConnectToPrinter.getInstance().leftIndents(Short.parseShort("0"));
            GetConnectToPrinter.getInstance().printStr(getString(R.string.transaction_id_txt)+txnId,null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("10"));
            GetConnectToPrinter.getInstance().printStr(getString(R.string.date_time_txt)+date,null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("10"));
            GetConnectToPrinter.getInstance().printStr(getString(R.string.rrn_txt)+referenceNo,null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("10"));
            GetConnectToPrinter.getInstance().printStr(getString(R.string.bankname_)+bankname,null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("10"));
            GetConnectToPrinter.getInstance().printStr(getString(R.string.aadhar_number)+adharcard,null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("10"));
            GetConnectToPrinter.getInstance().printStr(getString(R.string.balance_amount_rs)+balanceAmt_dialog,null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("10"));
            GetConnectToPrinter.getInstance().printStr(getString(R.string.transactiontype_camelcase)+type,null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("10"));
            //Transaction Amount
            GetConnectToPrinter.getInstance().fontSet(EFontTypeAscii.FONT_12_24,
                    EFontTypeExtCode.FONT_16_32);
            GetConnectToPrinter.getInstance().spaceSet((byte) 0,(byte) 0);
            GetConnectToPrinter.getInstance().leftIndents(Short.parseShort("0"));
            GetConnectToPrinter.getInstance().printStr(getString(R.string.transaction_amount_rs)+
                    tvTransactionAmount.getText().toString(),null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("25"));
            //Thank You Message
            GetConnectToPrinter.getInstance().fontSet(EFontTypeAscii.FONT_16_32,
                    EFontTypeExtCode.FONT_16_32);
            String thankYouString = getString(R.string.thanks_txt);
            if (thankYouString != null && thankYouString.length() > 0)
                GetConnectToPrinter.getInstance().printStr(thankYouString,null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("15"));
            //Partner(Admin, MD etc.) Name Message
            GetConnectToPrinter.getInstance().fontSet(EFontTypeAscii.FONT_12_24,
                    EFontTypeExtCode.FONT_16_32);
            String brandName = SdkConstants.BRAND_NAME;
            if (brandName != null && brandName.length() > 0)
                GetConnectToPrinter.getInstance().printStr(brandName,null);
            GetConnectToPrinter.getInstance().step(Integer.parseInt("100"));
            GetConnectToPrinter.getInstance().start();
        }).start();
    }
    private void getConnection(View view){
        registerForContextMenu(btnPrint);
        if (bluetoothAdapter == null) {
            Toast.makeText(AadhaarpayReceiptActivity.this, getString(R.string.bluetooth_not_supported), Toast.LENGTH_SHORT).show();
        } else {
            if (bluetoothAdapter.isEnabled()) {
                if (GetPosConnectedPrinter.aemPrinter == null) {
                    printerList = m_AemScrybeDevice.getPairedPrinters();
                    if (printerList.size() > 0) {
                        openContextMenu(view);
                    } else {
                        showAlert(getString(R.string.no_paired_printers_found));
                    }
                } else {
                    m_AemPrinter = GetPosConnectedPrinter.aemPrinter;
                    callBluetoothFunction(tvTxnID.getText().toString(), tvDateTime.getText().toString(),
                            tvBankName.getText().toString(), rrNumber_dialog,
                            tvTransactionAmount.getText().toString(), balanceAmt_dialog,
                            transactionType_dialog, aadhaarNumber_dialog, _statusMain, view);
                }
            } else {
                GetPosConnectedPrinter.aemPrinter = null;
                Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivity(turnOn);
            }
        }
    }
    private class UnifiedPrintReceiptThread  extends Thread{
        @Override
        public void run() {
            mPrinter=new Printer(AadhaarpayReceiptActivity.this);
            try {
                mPrinter.setPrintType(0);//Printer type 0 means it's an internal printer
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            checkPrinterStatus();
        }
    }
    /**
     * first check the printer status i.e if paper is present or not,or check different condition
     *
     * if printer status is OK then start printing
     * */
    private void checkPrinterStatus() {
        try {
            int[] status = new int[1];
            mPrinter.getPrinterStatus(status);
            String msg="";
            switch (status[0]){
                case 0x00:
                    msg=getString(R.string.printer_status_ok);
                    startPrinting();
                    break;
                case 0x01:
                    msg=getString(R.string.parameter_error);
                    showLog(msg);
                    break;
                case 0x8A://----138 return
                    msg=getString(R.string.out_of_paper);
                    showLog(msg);
                    break;
                case 0x8B:
                    msg=getString(R.string.overheat);
                    showLog(msg);
                    break;
                default:
                    msg=getString(R.string.printer_error);
                    showLog(msg);
                    break;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    private void showLog(String msg) {
        runOnUiThread(() -> Toast.makeText(AadhaarpayReceiptActivity.this, msg, Toast.LENGTH_SHORT).show());
    }
    /**
     * First initialize the printer and clear if any catch data is present
     *
     * After initialization of printer then start printing
     * */
    private void startPrinting() {
        int result = -1;
        try {
            result = mPrinter.printInit();
            mPrinter.clearPrintDataCache();
            if (result==0){
                printReceipt();
            }else {
                Toast.makeText(this, getString(R.string.printer_initialization_failed), Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void printReceipt() {
        int result=-1;
        try {
            mPrinter.setGrayLevel(3);
            result = mPrinter.printStringExt(SdkConstants.SHOP_NAME, 0,0f,2.0f, Printer.Font.SANS_SERIF, 25, Printer.Align.CENTER,true,false,true);
            result = mPrinter.printString(getString(R.string.transactionreport),25, Printer.Align.CENTER,true,false);
            if (_statusMain.equalsIgnoreCase(getString(R.string.failed))) {
                result = mPrinter.printString(getString(R.string.failure), 25, Printer.Align.CENTER, true, false);
            }else {
                result = mPrinter.printString(getString(R.string.success), 25, Printer.Align.CENTER, true, false);
            }
            result = mPrinter.printString("------------------------------------------", 30, Printer.Align.CENTER, true, false);
            result = mPrinter.printString(getString(R.string.transaction_id)+txnIdForPdf,20, Printer.Align.LEFT,false,false);
            result = mPrinter.printString(getString(R.string.aadhaar_number)+aadhaarNumber_dialog,20, Printer.Align.LEFT,false,false);
            result = mPrinter.printString(getString(R.string.date_time)+ tvDateTime.getText().toString().trim(),20, Printer.Align.LEFT,false,false);
            result = mPrinter.printString(getString(R.string.bank_name)+ tvBankName.getText().toString(),20, Printer.Align.LEFT,false,false);

            result = mPrinter.printString(getString(R.string.rrn)+rrNumber_dialog,20, Printer.Align.LEFT,false,false);
            result = mPrinter.printString(getString(R.string.balance_amount)+balanceAmt_dialog,20, Printer.Align.LEFT,false,false);
            result = mPrinter.printString(getString(R.string.transaction_amount_rs)+SdkConstants.transactionAmount,20, Printer.Align.LEFT,false,false);
            result = mPrinter.printString(getString(R.string.transaction_type)+getString(R.string.aadhaarpay),20, Printer.Align.LEFT,false,false);
            result = mPrinter.printStringExt(getString(R.string.thanks_txt), 0,0f,2.0f, Printer.Font.SANS_SERIF, 20, Printer.Align.RIGHT,true,true,false);
            result = mPrinter.printStringExt(SdkConstants.BRAND_NAME, 0,0f,2.0f, Printer.Font.SANS_SERIF, 20, Printer.Align.RIGHT,true,true,false);
            result = mPrinter.printString(" ",25, Printer.Align.CENTER,false,false);
            result = mPrinter.printString(" ",25, Printer.Align.CENTER,false,false);
            result = mPrinter.printPaper(30);
            showPrinterStatus(result);
            result = mPrinter.printFinish();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /**
     *  in between printing if any error occur then this method will show the toast
     * */
    private void showPrinterStatus(int result) {
        String msg="";
        switch (result){
            case 0x00:
                msg=getString(R.string.print_finish);
                showLog(msg);
                break;
            case 0x01:
                msg=getString(R.string.parameter_error);
                showLog(msg);
                break;
            case 0x8A://----138 return
                msg=getString(R.string.out_of_paper);
                showLog(msg);
                break;
            case 0x8B:
                msg=getString(R.string.overheat);
                showLog(msg);
                break;
            default:
                msg=getString(R.string.printer_error);
                showLog(msg);
                break;
        }
    }
}