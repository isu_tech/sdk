package com.matm.matmsdk.aepsmodule.aadharpay.model;
/**
 * AadharpayRequest represents a request object used in Aadharpay transactions.
 * It contains various parameters and getters/setters for accessing and modifying them.
 *
 * @author Smrutilipsa Patel
 */
public class AadharpayRequest {
    private String aadharNo;
    private String iin;
    private String apiUser;
    private String latLong;
    private String ipAddress;
    private String amount;
    private String mobileNumber;
    private String retailer;
    private String paramA;
    private String paramB;
    private String paramC;
    private boolean isSL;
    private String bankName;
    private String pidData;
    private String shakey;
    /**
     * Constructs an AadharpayRequest object with the specified parameters.
     *
     * @param aadharNo     Aadhar number for the transaction.
     * @param iin          Issuer Identification Number (IIN) for the transaction.
     * @param apiUser      API user responsible for the transaction.
     * @param latLong      Latitude and Longitude information for the transaction location.
     * @param ipAddress    IP address from which the request is made.
     * @param amount       Amount involved in the transaction.
     * @param mobileNumber Mobile number associated with the transaction.
     * @param retailer     Retailer information for the transaction.
     * @param paramA       Additional parameter A for customization.
     * @param paramB       Additional parameter B for customization.
     * @param paramC       Additional parameter C for customization.
     * @param isSL         Flag indicating whether the transaction is in SL (Secure Mode).
     * @param bankName     Bank name involved in the transaction.
     * @param pidData      Personal Identification Data (PID) associated with the transaction.
     * @param shakey       Security key for the transaction.
     */
    public AadharpayRequest(String aadharNo, String iin, String apiUser, String latLong, String ipAddress,
                            String amount, String mobileNumber, String retailer, String paramA, String paramB,
                            String paramC, boolean isSL, String bankName, String pidData,String shakey) {
        this.aadharNo = aadharNo;
        this.iin = iin;
        this.apiUser = apiUser;
        this.latLong = latLong;
        this.ipAddress = ipAddress;
        this.amount = amount;
        this.mobileNumber = mobileNumber;
        this.retailer = retailer;
        this.paramA = paramA;
        this.paramB = paramB;
        this.paramC = paramC;
        this.isSL = isSL;
        this.bankName = bankName;
        this.pidData = pidData;
        this.shakey = shakey;
    }
    public String getAadharNo() {
        return aadharNo;
    }
    public void setAadharNo(String aadharNo) {
        this.aadharNo = aadharNo;
    }
    public String getIin() {
        return iin;
    }
    public void setIin(String iin) {
        this.iin = iin;
    }
    public String getLatLong() {
        return latLong;
    }
    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }
    public String getAmount() {
        return amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }
    public String getMobileNumber() {
        return mobileNumber;
    }
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    public String getRetailer() {
        return retailer;
    }
    public void setRetailer(String retailer) {
        this.retailer = retailer;
    }
    public String getParamA() {
        return paramA;
    }
    public void setParamA(String paramA) {
        this.paramA = paramA;
    }
    public String getParamB() {
        return paramB;
    }
    public void setParamB(String paramB) {
        this.paramB = paramB;
    }
    public String getParamC() {
        return paramC;
    }
    public void setParamC(String paramC) {
        this.paramC = paramC;
    }
    public boolean getIsSL() {
        return isSL;
    }
    public void setIsSL(boolean isSL) {
        this.isSL = isSL;
    }
    public String getBankName() {
        return bankName;
    }
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    public String getPidData() {
        return pidData;
    }
    public void setPidData(String pidData) {
        this.pidData = pidData;
    }
    public boolean isSL() {
        return isSL;
    }
}
