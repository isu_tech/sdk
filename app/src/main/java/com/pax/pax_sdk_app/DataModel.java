package com.pax.pax_sdk_app;
public class DataModel {
    String transactionAmount;

    String transactionType;

    String customTheme;

    String receiveDriverData;

    String shop_name;

    String brand_name;

    String user_mobile_number;

    String user_email;

    String token;

    String userName;

    String applicationType;

    String paramA;

    String paramB;

    String paramC;

    String internalFpName;

    String manufacture_flag;

    String driver_activity;

    String device_type;

    Boolean is_beta_user;

    String device_name;

    String encryptedData;
    private String clientRefID;
    private String clientID;
    private String clientSecret;

    public String getClientRefID() {
        return clientRefID;
    }

    public void setClientRefID(String clientRefID) {
        this.clientRefID = clientRefID;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    String loginID;
    String API_USER_NAME_VALUE;

    Boolean skipReceipt;

    public Boolean getSkipReceipt() {
        return skipReceipt;
    }

    public void setSkipReceipt(Boolean skipReceipt) {
        this.skipReceipt = skipReceipt;
    }

    public String getAPI_USER_NAME_VALUE() {
        return API_USER_NAME_VALUE;
    }

    public void setAPI_USER_NAME_VALUE(String API_USER_NAME_VALUE) {
        this.API_USER_NAME_VALUE = API_USER_NAME_VALUE;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getCustomTheme() {
        return customTheme;
    }

    public void setCustomTheme(String customTheme) {
        this.customTheme = customTheme;
    }

    public String getReceiveDriverData() {
        return receiveDriverData;
    }

    public void setReceiveDriverData(String receiveDriverData) {
        this.receiveDriverData = receiveDriverData;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getUser_mobile_number() {
        return user_mobile_number;
    }

    public void setUser_mobile_number(String user_mobile_number) {
        this.user_mobile_number = user_mobile_number;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    public String getParamA() {
        return paramA;
    }

    public void setParamA(String paramA) {
        this.paramA = paramA;
    }

    public String getParamB() {
        return paramB;
    }

    public void setParamB(String paramB) {
        this.paramB = paramB;
    }

    public String getParamC() {
        return paramC;
    }

    public void setParamC(String paramC) {
        this.paramC = paramC;
    }

    public String getInternalFpName() {
        return internalFpName;
    }

    public void setInternalFpName(String internalFpName) {
        this.internalFpName = internalFpName;
    }
    public String getManufacture_flag() {
        return manufacture_flag;
    }

    public void setManufacture_flag(String manufacture_flag) {
        this.manufacture_flag = manufacture_flag;
    }

    public String getDriver_activity() {
        return driver_activity;
    }

    public void setDriver_activity(String driver_activity) {
        this.driver_activity = driver_activity;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public Boolean getIs_beta_user() {
        return is_beta_user;
    }

    public void setIs_beta_user(Boolean is_beta_user) {
        this.is_beta_user = is_beta_user;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public String getEncryptedData() {
        return encryptedData;
    }

    public void setEncryptedData(String encryptedData) {
        this.encryptedData = encryptedData;
    }

    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String loginID) {
        this.loginID = loginID;
    }
}


